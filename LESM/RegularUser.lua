RegularUser = { }
--- If I want to do RegularUser(Client) in future
-- then i will need to redo this class
-- Client.rudclient = RegularUser(Client)
--[[
Keys:
id          = ID
ident       = Guid Identifier
ips         = Table of ips
lastname    = last used name (current name)
autolevel   = If autolevel enabled
bot         = isBot
skills      = Skills table
lastseen    = last seen date time ( number )
mutetime    = mute date time ( number )
mutestrict  = If is strictly muted
mutereason  = Mute reason
ignored     = table of ignored clients
soundignored= table of clients ignored sounds
sounds      = If lua sounds are enabled
swearfilter = If swear filter is admin forced
nocaps      = If nocaps is admin forced
nocolors    = If nocolors is admin forced
grammarcheck= If grammarcheck is admin forced
warnings    = Warnings table
aliases     = Table of aliases
guids       = Table of etkey guids
silentids   = Table of silent guids
etproguids  = Table of etpro guids
]]

local _getClient = function(rudclient)
    for k = 1 , tlen(Clients) do 
        local Client                = Clients[k]
        local eGuid,sGuid,etproGuid = Client:getGuids()
        local ip                    = Client:getIp()
        local ident                 = ""
        if sGuid ~= "" then
            ident = string.upper(sGuid)
        elseif etproGuid ~= "" then
            ident = string.upper(etproGuid)
        elseif eGuid ~= "" then
            ident = string.upper(eGuid)
        else
            ident = "IP" .. string.gsub(ip,'%.','')
        end
        if ident == rudclient.ident then return Client end
    end
    return nil
end

local _verify = function(r)
    -- r = rudclient
    if r.guid then r.guid = nil end
    if r.silent then r.silent = nil end
    if r.sounds == nil then r.sounds = true end

    r.id           = tonumber(r.id) or 0
    r.ident        = r.ident or ""
    r.lastname     = r.lastname or ""
    r.ips          = r.ips or { }
    r.aliases      = r.aliases or { }
    r.autolevel    = r.autolevel or false
    r.guids        = r.guids or { }
    r.etproguids   = r.etproguids or { }
    r.silentids    = r.silentids or { }
    r.bot          = r.bot or false
    r.lastseen     = tonumber(r.lastseen) or 0
    r.skills       = r.skills or { }
    r.mutetime     = tonumber(r.mutetime) or 0
    r.mutestrict   = r.mutestrict or false
    r.mutereason   = r.mutereason or ""
    r.ignored      = r.ignored or { }
    r.soundignored = r.soundignored or { }
    r.sounds       = r.sounds or false
    r.warnings     = r.warnings or { }
    r.swearfilter  = r.swearfilter or false
    r.nocaps       = r.nocaps or false
    r.nocolors     = r.nocolors or false
    r.grammarcheck = r.grammarcheck or false
end

function RegularUser:Init()
    RegularUserData = FileHandler:Load{ filePath = "LESM/save/RegularUserData.json" }
    local usercount,botcount = 0,0
    for k = 1 , tlen(RegularUserData) do 
        local r = RegularUserData[k]
        if r.bot then botcount = botcount + 1 else usercount = usercount + 1 end
        _verify(r)
    end
    Console:Debug("Loaded " .. usercount .. " rudusers and " .. botcount .. " rudbots from rud database","core")
end

function RegularUser:Deinit()
    local usercount,botcount = 0,0
    for k = 1 , tlen(RegularUserData) do 
        local r = RegularUserData[k]
        if r.bot then botcount = botcount + 1 else usercount = usercount + 1 end
        _verify(r)
    end
    FileHandler:Save{ filePath = "LESM/save/RegularUserData.json", fileData = RegularUserData }
    Console:Debug("Saved " .. usercount .. " rudusers and " .. botcount .. " rudbots","core")
end

function RegularUser:create(Client)
    if self:get(Client) then return self:get(Client) end

    local rudclient   = { }
    local eGuid,sGuid,etproGuid = Client:getGuids()
    local ip          = Client:getIp()
    local name        = Misc:trim( et.Q_CleanStr( Client:getName() ) )

    rudclient.id = Core:getNextId(RegularUserData) -- ID of rudclient
    
    -- Start identity key
    if sGuid ~= '' then
        rudclient.ident = string.upper(sGuid)
    elseif etproGuid ~= '' then
        rudclient.ident = string.upper(etproGuid)
    elseif eGuid ~= '' then
        rudclient.ident = string.upper(eGuid)
    else
        rudclient.ident = "IP" .. string.gsub(ip,"%.","")
    end
    -- end identity
    
    rudclient.lastname   = name

    rudclient.ips = { ip }
    if sGuid ~= "" then     rudclient.silentids  = { string.upper(sGuid) } else rudclient.silentids = { } end
    if eGuid ~= "" then     rudclient.guids      = { string.upper(eGuid) } else rudclient.guids = { } end
    if etproGuid ~= "" then rudclient.etproguids = { string.upper(etproGuid) } else rudclient.etproguids = { } end

    rudclient.aliases   = { name }
    rudclient.autolevel = true
    rudclient.bot       = Client:isBot()
    rudclient.lastseen  = os.time()
    
    --[[
    local skills      = Client:getSkillTable()
    if not Config.XpSave.Enabled or Config.XpSave.RequireLogin then skills = { 0,0,0,0,0,0,0,} end
    if Client:isBot() and not Config.XpSave.Bots then skills = { 0,0,0,0,0,0,0,} end
    rudclient.skills     = skills
    --]]
    rudclient.mutetime   = 0.0
    rudclient.mutestrict = false
    rudclient.mutereason = ""
    rudclient.ignored    = { }
    rudclient.soundignored = { }
    rudclient.sounds = true

    rudclient.warnings  = { }
    RegularUserData[tlen(RegularUserData)+1] = rudclient
    Client.rudclient    = rudclient
    return rudclient
end

function RegularUser:get(Client)
    local eGuid,sGuid,etproGuid = Client:getGuids()
    local ip                    = Client:getIp()
    local ident                 = ""
    if sGuid ~= "" then
        ident = string.upper(sGuid)
    elseif etproGuid ~= "" then
        ident = string.upper(etproGuid)
    elseif eGuid ~= "" then
        ident = string.upper(eGuid)
    else
        ident = "IP" .. string.gsub(ip,'%.','')
    end

    for k = 1 , tlen(RegularUserData) do 
        local rud = RegularUserData[k]
        if rud.ident == ident then
            self:update(rud,Client)
            return rud
        end
    end

    return nil
end

function RegularUser:update(rudclient,Client)
    local eGuid,sGuid,etproGuid = Client:getGuids()
    local ip          = Client:getIp()
    local name        = Misc:trim( et.Q_CleanStr( Client:getName() ) )

    if ip ~= "" then Misc:table_addNotExist(rudclient.ips,ip) end
    if sGuid ~= "" then Misc:table_addNotExist(rudclient.silentids,string.upper(sGuid)) end
    if eGuid ~= "" then Misc:table_addNotExist(rudclient.guids,string.upper(eGuid)) end
    if etproGuid ~= "" then Misc:table_addNotExist(rudclient.etproguids,string.upper(etproGuid)) end
    Misc:table_addNotExist(rudclient.aliases,name)
    rudclient.lastname  = name
    rudclient.bot       = Client:isBot()
    rudclient.lastseen  = os.time()
    --[[
    local skills      = Client:getSkillTable()
    if not Config.XpSave.Enabled or Config.XpSave.RequireLogin then skills = { 0,0,0,0,0,0,0,} end
    if Client:isBot() and not Config.XpSave.Bots then skills = { 0,0,0,0,0,0,0,} end
    rudclient.skills    = skills
    ]]--
    if not Client.rudclient then
        Client.rudclient = rudclient
    end
end

function RegularUser:getById(rudid)
    rudid = tonumber(rudid)
    if not rudid then return nil end
    for k = 1 , tlen(RegularUserData) do 
        if RegularUserData[k].id == rudid then return RegularUserData[k] end
    end
    return nil
end

function RegularUser:getByLastName(rudname)
    rudname = Misc:triml(rudname)
    if rudname == '' then return nil end
    
    for k=1 , tlen(RegularUserData) do 
        local rudclient = RegularUserData[k]
        if string.find(rudclient.lastname,rudname,1,true) then
            return rudclient
        end
    end
    return nil
end

function RegularUser:find( ruddata )
    if self:getById(ruddata) then
        return self:getById(ruddata)
    else
        return self:getByLastName( ruddata )
    end
end


-- Warning stuff: 
local _warnSort = function(a,b) return a.time < b.time end

function RegularUser:checkWarnExpired(warn)
    local expires = Misc:getTimeFormat(Config.Warn.Expires)
    if expires < 0 then
        return false
    end
    if ( warn.time + expires ) < os.time() then
        return true
    else
        return false
    end
end

function RegularUser:removeWarning(warnings,warnid)
    warnid = tonumber(warnid)
    if not warnid then return false end
    for k = 1 , tlen(warnings) do 
        if warnings[k].id == warnid then
            table.remove(warnings,k)
            return true
        end
    end
    return false
end

function RegularUser:removeLastWarning(warnings)
    table.sort(warnings,_warnSort)
    table.remove(warnings,1)
end

function RegularUser:getWarnById(warnings,warnid)
    warnid = tonumber(warnid)
    if not warnid then return nil end
    for k = 1 , tlen(warnings) do 
        if warnings[k].id == warnid then
            return warnings[k]
        end
    end
    return nil
end

function RegularUser:warn(rudclient,warn)
    if not rudclient then return end
    if not warn then warn = { } end
    local Client = _getClient(rudclient)
    warn.reason = warn.reason or ""
    if Config.Shrubbot.Warn then
        if not Client then return end
        et.trap_SendConsoleCommand( et.EXEC_APPEND , "!warn " .. Client.id .. " ".. warn.reason .. "\n" )
    else
        warn.id     = Core:getNextId(rudclient.warnings)
        warn.time   = os.time()
        warn.warner = warn.warner or "Console Warn"
        rudclient.warnings[tlen(rudclient.warnings)+1] = warn
        table.sort(rudclient.warnings,_warnSort)
        if Client then
            Client:Play(Config.Sound.Warn)
            Client:Chat("You have been warned with reason: " .. Color.Secondary .. warn.reason)
        end
        self:checkWarn(rudclient)
    end

    if Config.Warn.Mail then
        Mail:Compose("Warning issued to " .. rudclient.lastname .. Color.Primary .. " RUDCLIENT[" .. Color.Tertiary .. rudclient.id ..
            Color.Primary .. "] by " .. warn.warner .. Color.Primary .. " at " .. os.date("%c",warn.time) ,-1,-1,true)
    end
end

function RegularUser:checkWarn(rudclient)
    if Config.Shrubbot.Warn then return false end
    if not rudclient then return false end
    local warnings = rudclient.warnings
    local expired  = { }
    for k = 1 , tlen(warnings) do 
        if self:checkWarnExpired(warnings[k]) then expired[tlen(expired)+1] = warnings[k].id end
    end
    for k = 1 , tlen(expired) do self:removeWarning(warnings,expired[k]) end
    table.sort(rudclient.warnings,_warnSort)
    if Config.Warn.Max <= 0 then return false end -- Skip punishment system
    if tlen(warnings) >= Config.Warn.Max then
        local Client = _getClient(rudclient)
        if Client then
            local BLTime = Misc:getTimeFormat(Config.Warn.BlacklistTime)
            if BLTime < 0 then BLTime = 0 else BLTime = BLTime + os.time() end
            Client:blacklist{ title   = "(Auto) Max Warnings" ,
                desc    = { "Automatic warn blacklist", "Exceeded warning limit", },
                expires = BLTime,
                banner  = "LUA Automatic",
                reason  = "Exceeded max warnings", }
        end
        if Config.Warn.RemoveLast then self:removeLastWarning(rudclient.warnings) end
        return true
    else
        return false
    end
end

--- Mute system
function RegularUser:mute(rudclient,muteTime,reason)
    if not rudclient or not muteTime then return end
    muteTime = Misc:getTimeFormat(muteTime)
    if muteTime <= 0 then muteTime = -1 end -- Permanent 
    local muteStr = ""
    if not reason then reason = "No reason given" end
    rudclient.mutereason = reason
    if muteTime == -1 or muteTime == 0 then
        rudclient.mutetime = muteTime
        muteStr = "Permanently"
    else
        rudclient.mutetime = os_time() + muteTime
        muteStr = Misc:SecondsToClock(muteTime)
    end
    local Client = _getClient(rudclient)
    if Client then Client:Chat("You have been muted " .. Color.Secondary .. muteStr .. Color.Primary .. " " .. reason) end
end

function RegularUser:strictMute(rudclient,reason)
    if not rudclient then return end
    if not reason then reason = "No reason given" end
    rudclient.mutestrict = true
    local Client = _getClient(rudclient)
    if Client then
        Client:Chat("You have been muted " .. Color.Secondary .. muteStr .. Color.Primary .. " " .. reason)
    end
end

function RegularUser:unmute(rudclient)
    if not rudclient then return end
    rudclient.mutetime   = 0
    rudclient.mutestrict = false
    rudclient.mutereason = ""
    local Client = _getClient(rudclient)
    if Client then Client:Chat("You have been unmuted!") end
end

function RegularUser:checkMutes()
    for k = 1 , tlen(RegularUserData) do 
        local rudclient = RegularUserData[k]
        if not rudclient.mutestrict and rudclient.mutetime > 0 then
            if rudclient.mutetime < os_time() then self:unmute(rudclient) end
        end
    end
end


--[[

--- Adds [time] to Client's mute length
function ClientObject:addMuteTime(time)
    time = tonumber(time) or 0
    if time == 0 then return end
    local currentMuteTime = self:getMuteTime()
    currentMuteTime = currentMuteTime + time
    if currentMuteTime < 0 then currentMuteTime = 0 end
    if ( Game:Mod() == "silent" ) then
        self:entSet("sess.muted",currentMuteTime)
    else
        self.muteTime = currentMuteTime
    end
end

--- Sets Client's mute length to [time]
-- -1 would be infinite
function ClientObject:setMuteTime(time)
    time = tonumber(time) or 0
    
    if ( Game:Mod() == "silent" ) then
        if time < 0 then time = 0 end
        self:entSet("sess.muted",time)
    else
        if time < -1 then time = 0 end
        self.muteTime = time
    end
end

function ClientObject:setStrictMute(toggle)
    if not toggle then toggle = false end
    self.strictMute = toggle
end

--- Sets Client's mute length to [time]
function ClientObject:getMuteTime()
    local time = self.muteTime
    if ( Game:Mod() == "silent" ) then
        time = self:entGet("sess.muted")
    end
    if not time then time = 0 end
    return time
end
]]--
