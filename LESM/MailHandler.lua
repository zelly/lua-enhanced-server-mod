Mail = { }

local _sortInbox = function(a,b) return tonumber(a.id) < tonumber(b.id) end
-- Linked Commands:
-- inbox - views inbox   |
-- send  - Sends message | send <to> <message>
-- read  - Reads message | read <id>


--- Loads the all mail messages
function Mail:Init()
    Console:Info("Loading mail database...")
    self.Inbox = { }
    Console:Debug("Mail:Init() - Start","mail")
    self.Inbox = FileHandler:Load{ filePath = "LESM/save/Mail.json" }
    -- quick fix
    for k=1,tlen(self.Inbox) do
        if not self.Inbox[k].delete then self.Inbox[k].delete = { } end
    end
    table.sort(self.Inbox,_sortInbox)
    Console:Debug("Mail:Init() - End","mail")
end

--- Saves all mail messages
function Mail:DeInit()
    Console:Debug("Mail:DeInit() - Start","mail")
    if ( self.Inbox ~= nil and next(self.Inbox) ~= nil ) then
        table.sort(self.Inbox,_sortInbox)
        FileHandler:Save{ filePath = "LESM/save/Mail.json", fileData = self.Inbox }
    end
    Console:Info("Saving Mail Database")
    Console:Debug("Mail:DeInit() - End","mail")
end

--- Gets next available mail id
function Mail:getNextId()
    local mail_id = 0
    while(true) do
        mail_id = mail_id + 1
        if ( self:getMail(mail_id) == nil ) then return mail_id end
    end
end

--- Gets a mail by its id
function Mail:getMail(mail_id)
    mail_id = tonumber(mail_id)
    if not mail_id then return nil end
    for k=1,tlen(self.Inbox) do
        if self.Inbox[k].id == mail_id then  return self.Inbox[k] end
    end
    return nil
end

--- Checks if a mail is valid to a user
function Mail:validId(user_id,mail_id)
    user_id = tonumber(user_id)
    mail_id = tonumber(mail_id)
    if not mail_id or not user_id then return false end
    local User = UserHandler:getWithId(user_id)
    local mail = self:getMail(mail_id)
    if not mail or not User then return false end
    
    if mail.to == user_id then
        return true
    elseif mail.to == -1 then
        if mail.admin then
            if User:getKey('authorize') or User:getKey('level') >= Config.Level.Leader then
                return true
            else
                return false
            end
        else
            return true
        end
    else
        return false
    end
end

--- Composes New Mail Message
-- Requires only message
-- message - Mail Message
-- to - Id of User ( -1  or nil to be to everyone )
-- from - Id of User ( -1 or nil to be from The Server )
-- Mail:Compose("I am message",1,3) -- From user 3 to user 1
-- Mail:Compose("I am message",nil,2) -- From user 2 to everyone
-- Mail:Compose("I am message") -- From The Server to everyone
function Mail:Compose(message,to,from,admin)
    if not message then return        end
    if not to      then to    = -1    end
    if not from    then from  = -1    end
    if not admin   then admin = false end
    
    message      = Misc:trim(message)
    local date   = os.time()
    local read   = { }
    local delete = { }
    
    if ( to == -1 and from ~= -1 ) then read[tlen(read)+1] = from end
    local Email = {
        id      = self:getNextId() , -- Id of mail
        to      = to      , -- User Id of target ( -1 If to everyone )
        from    = from    , -- User Id of sender ( -1 If from console )
        date    = date    , -- Date sent
      --global  = global  , -- true if sent global
        delete  = delete  , -- Table of ids of those who have deleted
        message = message , -- Mail message
        read    = read    , -- Table of ids of those who read
        admin   = admin   , -- Level in which is required to read
    }
    self.Inbox[tlen(self.Inbox)+1] = Email
    
    self:Notify(Email)
end

--- Tells client if he has any new messages or not
function Mail:CheckTotal(Client)
    local totalMail   = 0
    local clientInbox = self:getInbox(Client.User:getKey('id'))
    if not clientInbox or not next(clientInbox) then
        Client:Print("You have no new mail")
        return
    end
    for k = 1 , tlen(clientInbox) do 
        local mail = clientInbox[k]
        local f = true
        for j=1,tlen(mail.read) do
            if ( mail.read[j] == Client.User:getKey('id') ) then f = false end
        end
        if ( f ) then
            totalMail = totalMail + 1
        end
    end
    if totalMail == 0 then
        Client:Print("You have no new mail")
    else
        Client:Chat("You have " .. Color.Secondary .. totalMail .. Color.Primary .. " new messages " .. Color.mail .. "/mail read" .. Color.Primary .. " to read")
        Client:Play(Config.Sound.NewMail)
    end
end

--- Notifies Clients of new message if online
function Mail:Notify(email)
    local name = "The Server"
    if ( email.from ~= -1 ) then
        local User = UserHandler:getWithId(email.from)
        if not User then return end
        name = User:getKey("username")
    end
    for k = 1 , tlen(Clients) do 
        if Clients[k]:isOnline() then
            local User_id = Clients[k].User:getKey("id")
            if ( email.to == -1 or User_id == email.to ) then
                local sendmsg = true
                for j=1,tlen(email.read) do
                    if ( email.read[j] == User_id ) then sendmsg = false end
                end
                for h=1,tlen(email.delete) do
                    if ( email.delete[h] == User_id ) then sendmsg = false end
                end
                if email.admin and ( Clients[k]:getLevel() < Config.Level.Leader and not Clients[k]:isAuth() ) then sendmsg = false end
                if sendmsg then
                    Clients[k]:Chat("You have received a mail from " .. name .. Color.mail .. " /mail read " .. email.id .. Color.Primary .. " to read")
                    Clients[k]:Play(Config.Sound.NewMail)
                end
            end
            
        end
    end
end

--- Get inbox of a UserObject
function Mail:getInbox(user_id) -- Takes user id as argument since thats all we need
    local NewInbox = { }
    local User     = UserHandler:getWithId(user_id)
    if not User then return { } end
    for k=1,tlen(self.Inbox) do
        local mail = self.Inbox[k]
        if not self:hasDeleted(user_id,mail.id) then
            if ( mail.to == -1 or user_id == mail.to ) then
                if mail.admin then
                    if User:getKey("authorize") or User:getKey("level") >= Config.Level.Leader then NewInbox[tlen(NewInbox)+1] = mail end
                else
                    NewInbox[tlen(NewInbox)+1] = mail
                end
            end
        end
    end
    return NewInbox
end

--- Gets user inbox, messages that are sent directly to user
function Mail:getUserInbox(user_id)
    user_id = tonumber(user_id)
    if not user_id then return end
    local inbox = self:getInbox(user_id)
    local user_inbox = { }
    for k = 1 , tlen(inbox) do 
        if inbox[k].to == user_id then user_inbox[tlen(user_inbox)+1] = inbox[k] end
    end
    return user_inbox
end

--- Gets user inbox, messages that are sent to groups like global or admin
function Mail:getGlobalInbox(user_id)
    user_id = tonumber(user_id)
    if not user_id then return end
    local inbox = self:getInbox(user_id)
    local global_inbox = { }
    for k = 1 , tlen(inbox) do 
        if inbox[k].to == -1 then global_inbox[tlen(global_inbox)+1] = inbox[k] end
    end
    return global_inbox
end

--- Checks if user has read this mail.
function Mail:hasRead(user_id,mail_id)
    user_id = tonumber(user_id)
    mail_id = tonumber(mail_id)
    if not user_id or not mail_id then return false end
    local mail = self:getMail(mail_id)
    if not self:validId(user_id,mail_id) then return false end -- Shouldn't be false
    
    for k=1,tlen(mail.read) do
        if mail.read[k] == user_id then return true end
    end
    for k=1,tlen(mail.delete) do
        if mail.delete[k] == user_id then return true end
    end
    return false
end

--- Checks if user has deleted this mail.
function Mail:hasDeleted(user_id,mail_id)
    user_id = tonumber(user_id)
    mail_id = tonumber(mail_id)
    if not user_id or not mail_id then return false end
    local mail = self:getMail(mail_id)
    if not self:validId(user_id,mail_id) then return false end -- Shouldn't be false
    
    for k=1,tlen(mail.delete) do
        if mail.delete[k] == user_id then return true end
    end
    return false
end

--- Sets a mail_id as read
function Mail:readMail(user_id,mail_id)
    if not self:hasRead(user_id,mail_id) then
        local mail = self:getMail(mail_id)
        mail.read[tlen(mail.read)+1] = user_id
    end
end

--- Sets a mail_id as read
function Mail:deleteMail(user_id,mail_id)
    if not self:hasDeleted(user_id,mail_id) then
        local mail = self:getMail(mail_id)
        mail.delete[tlen(mail.delete)+1] = user_id
        mail.read[tlen(mail.read)+1] = user_id
    end
end

--- Deletes all mail for user
function Mail:deleteAll(user_id)
    local inbox = self:getInbox(user_id)
    if not inbox then return end
    for k = 1 , tlen(inbox) do self:deleteMail(user_id,inbox[k].id) end
end