local BallCommand = Command("8Ball")

BallCommand.ADMIN          = true
BallCommand.CHAT           = true
BallCommand.LUA            = true
BallCommand.AUTH           = true
BallCommand.CONSOLE        = true
BallCommand.GLOBALOUTPUT   = true
BallCommand.DESCRIPTION    = { "Ask the 8ball any question woooo" }
BallCommand.SYNTAX         = {
    { "<question>", "Ask the super question"},
}

function BallCommand:Run(Client, Args)
    local roll    = math.random( 1 , 20 )
    local message = Client:getName() .. Color.Primary .. " asked 8ball "
    
    if Args[1] then
        message = message .. MessageHandler:filter(table.concat(Args," "),Client) .. " and it answered with " .. Color.Secondary
    else
        message = message .. "his fate and it answered with " .. Color.Secondary
    end
    if ( roll >= 1 and roll <= 10 ) then
        message = message .. "No!"
    else
        message = message .. "Yes!"
    end
    self:InfoAll(message)
    return self.SUCCESS
end

return BallCommand