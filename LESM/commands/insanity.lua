if Game:Shrubbot() then -- will popup error, but best ima do atm
local InsanityCommand = Command("Insanity")
local _maxValue = 256

InsanityCommand.ADMIN          = true
InsanityCommand.CHAT           = true
InsanityCommand.LUA            = true
InsanityCommand.AUTH           = true
InsanityCommand.REQUIRE_LEVEL  = Config.Level.Leader
InsanityCommand.DESCRIPTION    = { 
                       "Only supports nq and noquarter",
                       "Toggles insanity command stuff",
                       "charge,instantspawn,unlimitedammo,mortarcam,rocketcam",
                       "guidedrockets,homingrockets,venom,weapons,riflecam,killcam",
                       "dogtags,kickdyno",
                      }

InsanityCommand.ALIASES        = { "ins","insane","fun", }
InsanityCommand.REQUIRE_MOD    = { "nq" ,"noquarter"}

function InsanityCommand:Run(Client,Args)
    if not Args[1] then
        self:Error("Need something to toggle")
        return self:getHelp()
    end
    Args[1] = string.lower(Args[1])
    local jp_insanity = tonumber(et.trap_Cvar_Get("jp_insanity"))
    if ( Args[1] == "list" ) then
        local jp_insanity = tonumber(et.trap_Cvar_Get("jp_insanity"))
        local jp_insanity_bits = bit.tobits(jp_insanity)
        for k = 0 , tlen(Bits) do 
            local msg = "["..k.."] = " .. Bits[k]
            msg =  msg .. "^3"
            if ( Bits[k] == 1 ) then
                msg = msg .. " INSTANT SPAWN"
            elseif ( Bits[k] == 2 ) then
                msg = msg .. " UNLIMITED AMMO"
            elseif ( Bits[k] == 4 ) then
                msg = msg .. " MORTAR CAM"
            elseif ( Bits[k] == 8 ) then
                msg = msg .. " ROCKET CAM"
            elseif ( Bits[k] == 16 ) then
                msg = msg .. " UNLIMITED CHARGE"
            elseif ( Bits[k] == 32 ) then
                msg = msg .. " GUIDED ROCKETS"
            elseif ( Bits[k] == 64 ) then
                msg = msg .. " HOMING ROCKETS"
            elseif ( Bits[k] == 128 ) then
                msg = msg .. " UNLOCKED WEAPONS"
            elseif ( Bits[k] == 256 ) then
                msg = msg .. " VENOM"
            end
            if ( jp_insanity_bits[k+1] ~= nil ) then
                if ( jp_insanity_bits[k+1] == 1 ) then
                    msg = msg .. " ^2ENABLED"
                else
                    msg = msg .. " ^1DISABLED"
                end
            end
            if ( jp_insanity_bits[k] ~= nil ) then
                msg = msg ..  "^7 | ^v" .. jp_insanity_bits[k] .. "^7 | " .. type(jp_insanity_bits[k])
            end
            self:InfoClean(msg)
        end
        return self.SUCCESS
    end
    if ( Game:Mod() == "nq" ) then
        if ( Args[1] == "spawn" ) or ( Args[1] == "instantspawn" ) then
            Game:bitCvarToggle("jp_insanity",1)
        elseif ( Args[1] == "unlimitedammo" ) or ( Args[1] == "ammo" ) then
            Game:bitCvarToggle("jp_insanity",2)
        elseif ( Args[1] == "mortar" ) or ( Args[1] == "mortarcam" ) then
            Game:bitCvarToggle("jp_insanity",4)
        elseif ( Args[1] == "rocketcam" ) then
            Game:bitCvarToggle("jp_insanity",8)
        elseif ( Args[1] == "charge" ) then
            Game:bitCvarToggle("jp_insanity",16)
        elseif ( Args[1] == "guidedrockets" ) or ( Args[1] == "gr" ) then
            Game:bitCvarToggle("jp_insanity",32)
        elseif ( Args[1] == "homingrockets" ) or ( Args[1] == "hr" ) then
            Game:bitCvarToggle("jp_insanity",64)
        elseif ( Args[1] == "dogtags" ) then
            Game:bitCvarToggle("jp_insanity",128)
        elseif ( Args[1] == "venom" ) then
            Game:bitCvarToggle("jp_insanity",256)
        elseif ( Args[1] == "kickdyno" ) then
            Game:bitCvarToggle("jp_insanity",2048)
        elseif ( Args[1] == "killcam" ) then
            Game:bitCvarToggle("jp_insanity",4096)
        elseif ( Args[1] == "riflecam" ) then
            Game:bitCvarToggle("jp_insanity",8192)
        else
            self:Error("This mod doesn't support that sub-command")
            return self:getHelp()
        end
    elseif ( Game:Mod() == "noquarter" ) then
        if ( Args[1] == "spawn" ) or ( Args[1] == "instantspawn" ) then
            Game:bitCvarToggle("jp_insanity", 1 )
        elseif ( Args[1] == "unlimitedammo" ) or ( Args[1] == "ammo" ) then
            Game:bitCvarToggle("jp_insanity", 2 )
        elseif ( Args[1] == "mortar" ) or ( Args[1] == "mortarcam" ) then
            Game:bitCvarToggle("jp_insanity", 4 )
        elseif ( Args[1] == "rocketcam" ) then
            Game:bitCvarToggle("jp_insanity", 8 )
        elseif ( Args[1] == "charge" ) then
            Game:bitCvarToggle("jp_insanity", 16 )
        elseif ( Args[1] == "guidedrockets" ) or ( Args[1] == "gr" ) then
            Game:bitCvarToggle("jp_insanity", 32 )
        elseif ( Args[1] == "homingrockets" ) or ( Args[1] == "hr" ) then
            Game:bitCvarToggle("jp_insanity", 64 )
        elseif ( Args[1] == "weapons" ) then
            Game:bitCvarToggle("jp_insanity", 128 )
        elseif ( Args[1] == "venom" ) then
            Game:bitCvarToggle("jp_insanity", 256 )
        else
            self:Error("This mod doesn't support that sub-command")
            return self:getHelp()
        end
    else
        self:Error("Mod not supported")
        return self:getHelp()
    end
end

return InsanityCommand
end