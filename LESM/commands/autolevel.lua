local AutoLevel = Command("AutoLevel")

AutoLevel.ALIASES        = {"alevel", }
AutoLevel.ADMIN          = true
AutoLevel.CHAT           = true
AutoLevel.LUA            = true
AutoLevel.CONSOLE        = true
AutoLevel.REQUIRE_LEVEL  = Config.Level.Leader
AutoLevel.DESCRIPTION    = { "Toggles autolevel for certain clients", }
AutoLevel.SYNTAX         = {
    { "<client>", "Toggles autolevel for client", },
}


function AutoLevel:Run(Client,Args)
    if not Args[1] then return self:Error("Could not find client") end
    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Could not find client") end
    if not Target.rudclient then
        if Target.invalid then
            return self:Info( va("%s guid currently not validated" , Client:getName() ))
        end
        return self:Error("Could not find client")
    end
    if Target.rudclient.autolevel then
        Target.rudclient.autolevel = false
        self:Info(Color.Secondary .. Target:getName() .. Color.Tertiary .. "'s " .. Color.Primary .. " autolevel has been disabled")
    else
        Target.rudclient.autolevel = true
        self:Info(Color.Secondary .. Target:getName() .. Color.Tertiary .. "'s " .. Color.Primary .. " autolevel has been enabled")
    end
    return self.SUCCESS
end

return AutoLevel