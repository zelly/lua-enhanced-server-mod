local BlacklistCommand = Command("Blacklist")

local _Verify = function(BL_Item)
    BL_Item = tonumber(BL_Item)
    if ( BL_Item == nil ) or ( BL_Item <= 0 ) or ( BL_Item > tlen(Blacklist) ) or not Blacklist[BL_Item] then
        return false
    end
    return true
end

BlacklistCommand.ADMIN          = true
BlacklistCommand.CHAT           = true
BlacklistCommand.LUA            = true
BlacklistCommand.AUTH           = true
BlacklistCommand.CONSOLE        = true
BlacklistCommand.REQUIRE_LEVEL  = Config.Level.Leader
BlacklistCommand.DESCRIPTION    = { "Blacklists a user from the server" ,
                                    "Basically a client kick or ban" ,
                                    "Can also do partial matches for things such as subnet bans",
                                    "Editable keys: title,desc,reason,ip,guid,temp"
                                  }
BlacklistCommand.SYNTAX         = {
    { "<client>"             , "Blacklists the client" },
    { "ip <ip>"              , "Blacklists an ip manually" },
    { "guid <silentid/guid>" , "Blacklists a specific guid" },
    { "view <id>"            , "Views blacklist"},
    { "edit <id>"            , "Edits blacklist"},
    { "remove <id>"          , "Removes blacklist by id" },
    { "list <page>"          , "Shows list of current blacklisted players" },
}
BlacklistCommand.EXAMPLES        = {
    { "add Bob" , "Bob's ip,guid, and silent id if they exist are added to the blacklist" },
    { "add ip 127.0.0.1", "127.0.0.1 ip is added to the blacklist" },
}

function BlacklistCommand:Run(Client,Args)
    if not Args[1] or Misc:triml(Args[1]) == 'list' then
        if not next(Blacklist) then return self:Info("No users are blacklisted") end
        local Data   = { }
        for k = 1 , tlen(Blacklist) do 
            local blacklist = Blacklist[k]
            local bl_data = { }
            bl_data[1] = blacklist.id
            bl_data[2] = blacklist.title
            if blacklist.temp then
                bl_data[3] = "Temporary"
            else
                if blacklist.expires <= 0 then
                    bl_data[3] = "Permanent"
                else
                    if blacklist.expires - os.time() <= 0 then
                        bl_data[3] = "Expired"
                    else
                        bl_data[3] = Misc:SecondsToClock( blacklist.expires - os.time() )
                    end
                end
            end
            Data[tlen(Data)+1] = bl_data
        end
        self:Info("Showing " .. Color.Secondary .. tlen(Data) .. Color.Primary .. " Blacklists")
        local page = tonumber(Args[2]) or 1
        local PT = Misc:FormatTable(Data, { "ID", "TITLE" , "EXPIRES" },false, Client:getMaxChars() , Client:getMaxRows() , page )
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    end
    
    if Misc:triml(Args[1]) == "view" then
        local blacklist,_ = Core:BlacklistGet(Args[2])
        if not blacklist then return self:Error("Missing blacklist id") end
        local _formatLength = 11
        self:Info("Info on " .. Color.Secondary .. blacklist.title .. Color.Tertiary .. " (" .. Color.Secondary .. blacklist.id .. Color.Tertiary .. ")")
        if blacklist.desc and next(blacklist.desc) then
            self:InfoClean("Description" .. Color.Tertiary .. " : " .. Color.Secondary .. blacklist.desc[1] )
            if tlen(blacklist.desc) > 1 then
                for k=2,tlen(blacklist.desc) do self:InfoClean(Misc:GetStaticString(" ",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. blacklist.desc[k]) end
            end
        end
        if blacklist.reason ~= "" then self:InfoClean(Misc:GetStaticString("Reason"   ,_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. blacklist.reason) end
        if blacklist.ip     ~= "" then self:InfoClean(Misc:GetStaticString("Ip"       ,_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. blacklist.ip) end
        if blacklist.guid   ~= "" then self:InfoClean(Misc:GetStaticString("Guid"     ,_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. blacklist.guid) end
        if blacklist.guid2 ~= "" then self:InfoClean(Misc:GetStaticString("Secondary Guid",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. blacklist.guid2) end
        if blacklist.banner ~= "" then
            self:InfoClean(Misc:GetStaticString("Banner",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. blacklist.banner)
        else
            self:InfoClean(Misc:GetStaticString("Banner",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. "The Server")
        end
        if blacklist.temp then
            self:InfoClean(Misc:GetStaticString("Expires",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. "Temporary")
        else
            if blacklist.expires <= 0 then
                self:InfoClean(Misc:GetStaticString("Expires",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. "Permanent")
            else
                if blacklist.expires - os.time() <= 0 then
                    self:InfoClean(Misc:GetStaticString("Expires",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. "Expired")
                else
                    self:InfoClean(Misc:GetStaticString("Expires",_formatLength) .. Color.Tertiary .. " : " .. Color.Secondary .. Misc:SecondsToClock(blacklist.expires - os.time()))
                end
            end
        end
        return self.SUCCESS
    elseif ( Misc:triml(Args[1]) == "remove" or Misc:triml(Args[1]) == "delete" ) then
        if Core:BlacklistRemove(Args[2]) then
            self:Info("Removed blacklist " .. Color.Secondary .. Args[1])
            return self.SUCCESS
        else
            return self:Error("Could not find blacklist id")
        end
    elseif Misc:triml(Args[1]) == "edit" then
        local blacklist,bl_index = Core:BlacklistGet(Args[2])
        if not blacklist then return self:Error("Missing blacklist id") end
        local key = Misc:triml(Args[3])
        if key == "title" then
            blacklist.title = Misc:trim(table.concat(Args," " , 4))
            self:Info("Title updated")
            return self.SUCCESS
        elseif key == "reason" then
            blacklist.reason = Misc:trim(table.concat(Args," " , 4))
            self:Info("Reason updated")
            return self.SUCCESS
        elseif key == "desc" then
            local value = Misc:trim(table.concat(Args," " , 4))
            if value ~= "" then
                blacklist.desc[tlen(blacklist.desc)+1] = value
                self:Info("Description updated")
            else
                blacklist.desc[tlen(blacklist.desc)] = nil
                self:Info("Erased last line of description")
            end
            return self.SUCCESS
        elseif key == "expires" then
            local expires = Misc:getTimeFormat(Args[4])
            if expires <= 0 then
                blacklist.expires = -1
                self:Info("Expire set to permanent")
            else
                blacklist.expires = os.time() + expires
                self:Info("Expire set to " .. Color.Secondary .. Misc:SecondsToClock(expires))
            end
            return self.SUCCESS
        elseif key == "temp" then
            if blacklist.temp then
                blacklist.temp = false
                self:Info("Made blacklist non-temporary")
            else
                blacklist.temp = false
                self:Info("Made blacklist temporary. It will last only this map")
            end
            return self.SUCCESS
        elseif key == "ip" then
            local ip = Misc:triml(Args[4])
            blacklist.ip = ip
            self:Info("Ip updated to " .. Color.Secondary .. ip)
            return self.SUCCESS
        elseif key == "guid" then
            local guid = Misc:triml(Args[4])
            if string.len(guid) == 8 then
                for k = 1 , tlen(RegularUserData) do 
                    local rudclient = RegularUserData[k]
                    for j=1,tlen(rudclient.guids) do
                        if string.find(string.lower(rudclient.guids[j]),string.lower(guid).."$") then
                            guid = rudclient.guids[j]
                            break
                        end
                    end
                end
                if string.len(guid) ~= 32 and string.len(guid) ~= 0 then return self:Error("Need last 8 characters of an existing guid, or full 32 characters") end
            end
            blacklist.guid = guid
            self:Info("Guid updated to " .. Color.Secondary .. guid)
            return self.SUCCESS
        else
            return self:Error("Invalid blacklist key")
        end
        return self.SUCCESS
    end
    if Misc:triml(Args[1]) == "ip" then
        local ip = Args[2]
        if not ip then return self:Error("Missing ip") end
        local blacklist    = { }
        local expires = tonumber(Args[3]) or -1
        if expires > 0 then expires = os.time() + expires end
        blacklist.title    = ip
        blacklist.banner   = Misc:trim(et.Q_CleanStr(Client:getName()))
        blacklist.ip       = ip
        blacklist.reason   = Misc:trim(table.concat(Args," ",4))
        blacklist.expires  = expires
        Core:BlacklistAdd(blacklist)
        self:Info("Added new blacklist for " .. Color.Secondary .. ip)
        return self.SUCCESS
    end
    if Misc:triml(Args[1]) == "guid" then
        local guid = Args[2]
        if not guid then return self:Error("Missing guid") end
        guid = string.upper(guid)
        if string.len(guid) == 8 then
            --self:Info("Got 8 characters " .. guid)
            for k = 1 , tlen(RegularUserData) do 
                local rudclient = RegularUserData[k]
                if not rudclient.bot then
                    for j=1,tlen(rudclient.guids) do
                        local last8 = string.sub(rudclient.guids[j],-8,-1)
                        --self:InfoClean(last8)
                        if last8 == guid then
                            --self:Info("Found!")
                            guid = rudclient.guids[j]
                            break
                        end
                    end
                end
            end
        end
        --self:Info("Guid len = " .. string.len(guid))
        if string.len(guid) ~= 32 then return self:Error("Need last 8 characters of an existing guid, or full 32 characters") end
        local blacklist    = { }
        local expires = tonumber(Args[3]) or -1
        if expires > 0 then expires = os.time() + expires end
        blacklist.title    = guid
        blacklist.banner   = Misc:trim(et.Q_CleanStr(Client:getName()))
        blacklist.guid     = guid
        blacklist.reason   = Misc:trim(table.concat(Args," ",4))
        blacklist.expires  = expires
        Core:BlacklistAdd(blacklist)
        self:Info("Added new blacklist for " .. Color.Secondary .. guid)
        return self.SUCCESS
    end
    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Could not find client") end
    local eGuid,sGuid,etproGuid= Target:getGuids()
    local blacklist    = { }
    local expires = tonumber(Args[2]) or -1
    if expires > 0 then expires = os.time() + expires end
    blacklist.title    = Misc:trimlname(Target:getName())
    blacklist.banner   = Misc:trim(et.Q_CleanStr(Client:getName()))
    blacklist.ip       = Target:getIp()
    blacklist.guid     = eGuid
    if sGuid ~= "" then
        blacklist.guid2 = sGuid
    elseif etproGuid ~= "" then
        blacklist.guid2 = etproGuid
    end
    blacklist.reason   = Misc:trim(table.concat(Args," ",3))
    blacklist.expires  = expires
    Target:blacklist(blacklist)
    self:Info("Added new blacklist")
    return self.SUCCESS
end

return BlacklistCommand