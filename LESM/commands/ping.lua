local PingCommand = Command("Ping")

PingCommand.ADMIN          = true
PingCommand.CHAT           = true
PingCommand.LUA            = true
PingCommand.AUTH           = true
PingCommand.CONSOLE        = true
PingCommand.REQUIRE_LOGIN  = true
PingCommand.REQUIRE_AUTH   = true
PingCommand.DESCRIPTION    = { "Falsifys targets ping" , "Does not actually affect performance, just for show." }
PingCommand.SYNTAX         = {
    { "auto" , "Toggles automatic fakeping for you."},
    { "<pingMin> <pingMax>", "Sets your fake ping range"},
    { "<clientName> <pingMin> <pingMax>", "Sets fake ping range for client"},
}


function PingCommand:Run(Client,Args)
    if not Args[1] then
        self:Error("Need ping min and max")
        return self:getHelp()
    end
    if ( string.lower(Args[1]) == "auto" ) then
        if not Client.User:getKey("fakeping") then
            Client.User:update("fakeping",true)
            self:Info("Enabled automatic fake ping")
            return self.SUCCESS
        else
            Client.User:update("fakeping",false)
            self:Info("Disabled automatic fake ping")
            return self.SUCCESS
        end
    end
    local pingMin = tonumber(Args[2])
    local pingMax = tonumber(Args[3])
    if not pingMax then
        pingMin = tonumber(Args[1])
        pingMax = tonumber(Args[2])
        if not pingMin or not pingMax then
            self:Error("Need ping min and max") 
            return self:getHelp()
        end
        Client.pingMin = pingMin
        Client.pingMax = pingMax
        Client:setPing(math.random(pingMin,pingMax))
        if ( Client:isOnline() ) and ( Client:isAuth() ) then
            Client.User:update("pingmin",pingMin)
            Client.User:update("pingmax",pingMax)
        end
        self:Info("Set your ping range to " .. pingMin .. " - " .. pingMax)
        return self.SUCCESS
    end
    if not pingMin or not pingMax then
        self:Error("Need ping min and max") 
        return self:getHelp()
    end
    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not find client")
        return self:getHelp()
    end
    Target.pingMin = pingMin
    Target.pingMax = pingMax
    Target:setPing(math.random(Target.pingMin,Target.pingMax))
    if ( Target:isOnline() ) and ( Target:isAuth() ) then
        Target.User:update("pingmin",pingMin)
        Target.User:update("pingmax",pingMax)
    end
    self:Info("Set " .. Target:getName() .. Color.Primary .. "'s ping range to " .. pingMin .. " - " .. pingMax)
    return self.SUCCESS
end

return PingCommand