local RulesCommand = Command("Rules")

RulesCommand.CORE           = true
RulesCommand.ADMIN          = true
RulesCommand.CHAT           = true
RulesCommand.LUA            = true
RulesCommand.GLOBALOUTPUT   = true
RulesCommand.DESCRIPTION    = { "Displays server rules" , "Can also print individual rule to everyone" }
RulesCommand.ALIASES        = { "rule" }
RulesCommand.SYNTAX         = {
    {"","Displays all rules",},
    { "<rulenumber>" , "Displays specific rule number to everyone",},
    { "add <message>" , "Adds a rule" },
    { "edit <rulenumber> <message>" , "Edit rule message" },
    { "delete <rulenumber>" , "Removes rule" },
    { "refresh" , "Refreshes rule routine" },
}

function RulesCommand:Run(Client,Args)
    if Client:isConsole() or Client:isAuth() then
        if Misc:triml(Args[1]) == "add" then
            local ruleMessage = Misc:trim(table.concat(Args," ", 2))
            if ruleMessage == "" then return self:Error("Need message to add") end
            
            Rules[tlen(Rules)+1] = ruleMessage
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Rules, fileData = Rules }
            self:Info("Added rule!")
            return self.SUCCESS
        elseif Misc:trim(Args[1]) == "edit" then
            local rulen  = tonumber(Args[2])
            if not rulen then return self:Error("Need rule number to edit") end
            if not Rules[rulen] then return self:Error("No rule with that number") end
            local ruleMessage = Misc:trim(table.concat(Args," ",3))
            if ruleMessage == "" then return self:Error("Need message to edit") end
            
            Rules[rulen] = ruleMessage
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Rules, fileData = Rules }
            self:Info("Edited rule")
            return self.SUCCESS
        elseif Misc:trim(Args[1]) == "delete" then
            local rulen  = tonumber(Args[2])
            if not rulen then return self:Error("Need rule number to delete") end
            if not Rules[rulen] then return self:Error("No rule with that number") end
            table.remove(Rules,rulen)
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Rules, fileData = Rules }
            self:Info("Removed rule!")
            return self.SUCCESS
        end
    end
    if tonumber(Args[1]) then
        local RuleNumber = tonumber(Args[1])
        if Rules[RuleNumber] == nil or Misc:trim(Rules[RuleNumber]) == '' then
            return self:Error("No Rule by that number")
        end
            
        self:InfoAll(Color.Tertiary .. "#" .. Color.Secondary .. RuleNumber .. Color.Primary .." " .. MessageHandler:replace( Rules[RuleNumber] , true , Client ) )
        return self.SUCCESS
    else
        self:Info("Displaying " .. Color.Secondary .. tlen(Rules) .. Color.Primary .. " rules")
        for k = 1 , tlen(Rules) do 
            self:InfoClean(Color.Tertiary .. "#" .. Color.Secondary .. k .. Color.Primary .." " .. MessageHandler:replace( Rules[k] , true , Client ) )
        end
        return self.SUCCESS
    end
end

return RulesCommand