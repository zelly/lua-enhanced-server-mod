local Report = Command("Report")

Report.ALIASES        = { "issue", "bugreport", }
Report.CORE           = true
Report.ADMIN          = true
Report.CHAT           = true
Report.LUA            = true
Report.AUTH           = true
Report.CONSOLE        = true
Report.DESCRIPTION    = {
                         "Reports an issue to the server admins.",
                         "Issue could be anything from an issue with a player, the server, or the lua.",
                         "The report will automatically include your name and the time you reported.",
                         "Keep your report short simple and to the point.",
                         "Note you can only input a certain amount of characters into chat( about 125) and console( about 200 )",
                         "You are limited to " .. Color.Tertiary .. Config.MaxReports .. Color.Secondary .. " reports per round",
                        }
Report.SYNTAX         = { { "(message)" , "Reports an issue with the message being (message)" }, }
Report.EXAMPLES       = { { "John is being a big ol meanie!", "" }, }

function Report:Run(Client,Args)
    local name     = "Console"
    local reporton = os.date("%c")
    local message  = Misc:trim(et.Q_CleanStr(table.concat(Args," ")))
    if not Client:isConsole() then
        if ( Client.reports >= Config.MaxReports and not Client:isAuth() ) then return self:Error("You have exceeded the allowed reports.") end
        name = et.Q_CleanStr(Client:getName())
    end
    
    if not message or ( string.len(message) <= 5 ) then return self:Error("Message must be at least 5 characters") end
    if Config.ReportToMail then
        Mail:Compose(name .. " has reported an issue at " .. reporton .. " with message: " .. message ,-1,-1,true)
    else
        local Report = {
            ReportedBy = name,
            ReportedOn = reporton,
            Message = message,
        }
        FileHandler:Save{ filePath = "LESM/save/br_" .. os.time()..".json",fileData = Report }
    end
    if not Client:isConsole() then Client.reports = ( Client.reports + 1 ) end
    self:Info("Your report has been submitted.")
    return self.SUCCESS
end

return Report