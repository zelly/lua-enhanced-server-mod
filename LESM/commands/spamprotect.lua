local SpamProtectCommand = Command("SpamProtect")

SpamProtectCommand.ADMIN          = true
SpamProtectCommand.CHAT           = true
SpamProtectCommand.LUA            = true
SpamProtectCommand.AUTH           = true
SpamProtectCommand.CONSOLE        = true
SpamProtectCommand.REQUIRE_LEVEL  = Config.Level.Leader
SpamProtectCommand.DESCRIPTION    = { "Sets a multiplier to Clients spam protection." }
SpamProtectCommand.ALIASES        = { "spam" }
SpamProtectCommand.SYNTAX         = {
    { "<clientname|clientid> <spammultiplier>", "Sets targets spam multiplier",},
}

function SpamProtectCommand:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not find client")
        return self:getHelp()
    end
    
    if ( Client:getLevel() <= Target:getLevel() ) and not Client:isAuth() then
        self:Error("Targets level is too high")
        return self:getHelp()
    end
   local Multiplier = tonumber(Args[2]) or 1.0
   Target.spamMultiplier = Multiplier
   if Target:isOnline() then
        Target.User:update("spam",Multiplier)
   end
   self:Info(Target:getName() .. Color.Primary .."'s spam Multiplier has been set to " .. Color.Secondary .. Multiplier)
   return self.SUCCESS
end

return SpamProtectCommand