local ShowDamageCommand = Command("ShowDamage")

ShowDamageCommand.CORE           = true
ShowDamageCommand.ADMIN          = true
ShowDamageCommand.CHAT           = true
ShowDamageCommand.LUA            = true
ShowDamageCommand.CONSOLE        = true
ShowDamageCommand.DESCRIPTION    = { "Show damages and kills", "If value is -1 it means mod isn't supported" }
ShowDamageCommand.ALIASES        = { "showdmg","damages","damage","dmg", }

function ShowDamageCommand:Run(Client,Args)
    local Data = { }
    for k = 1 , tlen(Clients) do 
        if ( Clients[k]:getTeam() == 1 ) then
            local ClientData = { }
            ClientData[1] = Clients[k].id
            ClientData[2] = Clients[k]:getName()
            if not Client:isConsole() and ( Client:getTeam() ~= 3 ) and ( Client:getTeam() ~= Clients[k]:getTeam() ) then
                ClientData[3] = "-----"
            else
                ClientData[3] = Clients[k]:getClassTable().name
            end
            ClientData[4] =  EntityHandler:safeEntGet(Clients[k].id,"sess.kills") or -1
            ClientData[5] =  EntityHandler:safeEntGet(Clients[k].id,"sess.deaths") or -1
            ClientData[6] =  EntityHandler:safeEntGet(Clients[k].id,"sess.damage_given") or -1
            ClientData[7] =  EntityHandler:safeEntGet(Clients[k].id,"sess.damage_received") or -1
            ClientData[8] =  EntityHandler:safeEntGet(Clients[k].id,"sess.team_kills") or -1
            ClientData[9] =  EntityHandler:safeEntGet(Clients[k].id,"sess.team_damage_given") or -1
            ClientData[10] = EntityHandler:safeEntGet(Clients[k].id,"sess.team_damage_received") or -1
            Data[tlen(Data)+1] = ClientData
        end
    end
    Data[tlen(Data)+1] = { "---","---","---","---","---","---","---","---","---","---",}
    for k = 1 , tlen(Clients) do 
        if ( Clients[k]:getTeam() == 2 ) then
            local ClientData = { }
            ClientData[1] = Clients[k].id
            ClientData[2] = Clients[k]:getName()
            if not Client:isConsole() and ( Client:getTeam() ~= 3 ) and ( Client:getTeam() ~= Clients[k]:getTeam() ) then
                ClientData[3] = "-----"
            else
                ClientData[3] = Clients[k]:getClassTable().name
            end
            ClientData[4] = EntityHandler:safeEntGet(Clients[k].id,"sess.kills") or -1
            ClientData[5] = EntityHandler:safeEntGet(Clients[k].id,"sess.deaths") or -1
            ClientData[6] = EntityHandler:safeEntGet(Clients[k].id,"sess.damage_given") or -1
            ClientData[7] = EntityHandler:safeEntGet(Clients[k].id,"sess.damage_received") or -1
            ClientData[8] = EntityHandler:safeEntGet(Clients[k].id,"sess.team_kills") or -1
            ClientData[9] = EntityHandler:safeEntGet(Clients[k].id,"sess.team_damage_given") or -1
            ClientData[10] = EntityHandler:safeEntGet(Clients[k].id,"sess.team_damage_received") or -1
            Data[tlen(Data)+1] = ClientData
        end
    end
    if tlen(Data) == 1 then return self:Info("No damage info yet") end
    local DataPrint = Misc:FormatTable(Data,{"ID","NAME","CLASS","KILLS","DEATHS","DMG GVN", "DMG RCV","TK","TDMG GVN","TDMG RCV"},false,Client:getMaxChars())
    if not next(DataPrint) then return self:Error("Could not get info") end
    for p = 1 , tlen(DataPrint) do self:InfoClean(DataPrint[p]) end
    return self.SUCCESS
end

return ShowDamageCommand