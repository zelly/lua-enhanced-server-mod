local ViewFollowersCommand = Command("ViewFollowers")

ViewFollowersCommand.CORE           = true
ViewFollowersCommand.ADMIN          = true
ViewFollowersCommand.CHAT           = true
ViewFollowersCommand.LUA            = true
ViewFollowersCommand.AUTH           = true
ViewFollowersCommand.CONSOLE        = true
ViewFollowersCommand.REQUIRE_LOGIN  = true
ViewFollowersCommand.REQUIRE_AUTH   = true
ViewFollowersCommand.DESCRIPTION    = { "Views who is spectating who" }
ViewFollowersCommand.ALIASES        = { "followers","vf", }

function ViewFollowersCommand:Run(Client,Args)
    local Data = { }
    for k = 1 , tlen(Clients) do 
        local spec_state  = tonumber(Clients[k]:entGet("sess.spectatorState")) or -1
        local spec_client = tonumber(Clients[k]:entGet("sess.spectatorClient"))
        if ( Clients[k]:getTeam() == et.TEAM_SPEC and spec_state == 2 ) then
            local Target = ClientHandler:getClient(spec_client)
            if Target then Data[tlen(Data)+1] = { Clients[k]:getName(),Target:getName(), } end
        end
    end
    if not next(Data) then return self:Error("No clients are spectating") end
    local PrintTable = Misc:FormatTable(Data,{"Spectator", "Player"},false,Client:getMaxChars())
    for p = 1 , tlen(PrintTable) do 
        self:InfoClean(PrintTable[p])
    end
    return self.SUCCESS
end

return ViewFollowersCommand