local PrivateMessageCommand = Command("PrivateMessage")

PrivateMessageCommand.CORE           = true
PrivateMessageCommand.LUA            = true
PrivateMessageCommand.AUTH           = true
--PrivateMessageCommand.NOSHRUBBOT     = true
PrivateMessageCommand.DESCRIPTION    = { "Sends a private message to a client", "See also pmlevel" }
PrivateMessageCommand.ALIASES        = { "pm","m"}
PrivateMessageCommand.SYNTAX         = {
    { "<client> <message>", "Sends message to client"},
    { "<client>,<client>,<client>... <message>", "Sends message to client"},
    { "admin <message>", "Sends message to client"},
    { "leader <message>", "Sends message to client"},
}
PrivateMessageCommand.EXAMPLES       = {
    { "bob Hello bob!" , "Sends pm to bob"},
    { "bob,steve,joe,4 Hey guys hows it going!?" , "Sends a pm to bob , steve , joe , and whoever is in client slot #4"},
}

function PrivateMessageCommand:Run(Client,Args)
    local to      = Misc:trimlname(Args[1])
    local message = MessageHandler:filter(table.concat(Args, " " , 2 ),Client)
    
    if string.len(to) <= 0 then return self:Error("No one to send message to!") end
    if message == "" then return self:Error("No message to send!") end
    
    local to_table = { }
    local sendto   = { }
    if string.find(to,",") then to_table = Misc:split(to,",") end
    
    for k = 1 , tlen(Clients) do 
        local Target = Clients[k]
        if not Target:isIgnored(Client) and Client.id ~= Target.id then
            if to == "admin" then
                if Target:getLevel() >= Config.Level.Admin then sendto[tlen(sendto)+1] = Target end
            elseif to == "leader" then
                if Target:getLevel() >= Config.Level.Leader or Target:isAuth() then sendto[tlen(sendto)+1] = Target end
            elseif next(to_table) then
                for j = 1 , tlen(to_table) do 
                    local tname = Misc:trimlname(Target:getName())
                    if tonumber(to_table[j]) then
                        if tonumber(to_table[j]) == Target.id then sendto[tlen(sendto)+1] = Target end
                    else
                        if string.find(tname,to_table[j],1,true) then sendto[tlen(sendto)+1] = Target end
                    end
                end
            else
                if tonumber(to) then
                    if tonumber(to) == Target.id then sendto[tlen(sendto)+1] = Target end
                else
                    local tname = Misc:trimlname(Target:getName())
                    if string.find(tname,to,1,true) then sendto[tlen(sendto)+1] = Target end
                end
            end
        end
    end
    if not next(sendto) then return self:Error("Could not find any clients to send to") end
    message = "PM for " .. Color.Tertiary .. "(" .. Color.Secondary .. to .. Color.Tertiary .. ") " .. Color.Primary .. " from " .. Client:getName() .. Color.Tertiary ..": " .. Color.Primary .. message
    for k = 1 , tlen(sendto) do 
        sendto[k]:ChatClean(message)
        Sound:play(sendto[k],Config.Sound.PrivateMessage,Client)
    end
    if tlen(sendto) == 1 then
        self:Info("Sent PM to " .. Color.Secondary .. sendto[tlen(sendto)]:getName() )
    else
        self:Info("Sent PM to " .. Color.Tertiary .. "(" .. Color.Secondary .. to .. Color.Tertiary .. ") " .. Color.Secondary .. tlen(sendto) .. Color.Primary .. " recipients")
    end
    Logger:Log("privatemessage",Client.id .. " " .. to .. " " .. message)
    return self.SUCCESS
end

return PrivateMessageCommand