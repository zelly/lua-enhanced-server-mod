local ViewCommand = Command("View")

local _View = function(User)
    local t = { }
    local c1 = Color.Primary
    local c2 = Color.Secondary
    local c3 = Color.Tertiary
    -- ID(0) UserName LVL(0)
    t[tlen(t)+1] = "ID" .. c3 .."(" .. c2 .. User:getKey('id') .. c3 .. ") " .. c2 .. User:getKey('username') .. c1 .. " LVL" .. c3 .. "(" .. c2 .. User:getKey('level') .. c3 ..")"
    if User:getKey('afk') then
        -- Away blah for x seconds
        t[tlen(t)+1] = "Away " .. c2 .. User:getKey('afkreason') .. c1 .. " for " .. c2 .. Misc:SecondsToClock( os.time() - User:getKey('afktime') )
    end
    -- Join Date : 01/02/15 6:43 PM 
    -- Last Seen : 01/02/15 2:23 AM
    t[tlen(t)+1] = "Join Date  " .. c3 .. ": " .. c2 .. os.date("%c", User:getKey('joindate') )
    t[tlen(t)+1] = "Last Seen  " .. c3 .. ": " .. c2 .. os.date("%c", User:getKey('lastseen') )
    -- Play Time : x seconds
    t[tlen(t)+1] = "Play Time  " .. c3 .. ": " .. c2 .. Misc:SecondsToClock( User:getKey('timeplayed') )
    if User:getKey('xfire') ~= "" then
        -- Xfire : xfirename
        t[tlen(t)+1] = "Xfire      " .. c3 .. ": " .. c2 .. User:getKey('xfire')
    end
    if User:getKey('forumsname') ~= "" then
        -- Xfire : xfirename
        t[tlen(t)+1] = "ForumsName " .. c3 .. ": " .. c2 .. User:getKey('forumsname')
    end
    if User:getKey('realname') ~= "" then
        -- Real Name : Your Name AGE()
        local age = User:getKey('age')
        if age ~= 0 then age = c1 .. " AGE" .. c3 .. "(" .. c2 .. age .. c3 .. ")" end
        t[tlen(t)+1] = "Real Name  " .. c3 .. ": " .. c2 .. User:getKey('realname') .. age
    end
    if User:getKey('greeting') ~= "" then
        -- Greeting : greeting SOUND(greetingsound)
        local sound = User:getKey('greetingsound')
        if sound ~= "" then sound = c1 .. " SOUND" .. c3 .. "(" .. c2 .. sound .. c3 .. ")" end
        t[tlen(t)+1] = "Greeting   " .. c3 .. ": " .. c2 .. User:getKey('greeting') .. sound
    end
    if User:getKey('farewell') ~= "" then
        -- Farewell : farewell SOUND(farewell)
        local sound = User:getKey('farewellsound')
        if sound ~= "" then sound = c1 .. " SOUND" .. c3 .. "(" .. c2 .. sound .. c3 .. ")" end
        t[tlen(t)+1] = "Farewell   " .. c3 .. ": " .. c2 .. User:getKey('farewell') .. sound
    end
    local _colorkarma = function(karma)
        karma = tonumber(karma) or 0
        if not karma or karma == 0 then
            return "^70"
        elseif karma > 0 then
            return "^2" .. karma
        else
            return "^1" .. karma
        end
    end
    local karmagoodin = _colorkarma(User:getKey('karmagoodin'))
    local karmagoodout = _colorkarma(User:getKey('karmagoodout'))
    local karmabadout = _colorkarma(User:getKey('karmabadout'))
    local karmabadin = _colorkarma(User:getKey('karmabadin'))
    -- Karma : 2/0
    t[tlen(t)+1] =     "Karma      " .. c3 .. ": " .. karmagoodin .. c3 .. "/" .. karmagoodout .. " " .. karmabadin .. c3 .. '/' .. karmagoodout
    
    -- Kills : kills/botkills
    t[tlen(t)+1] = "Kills      " .. c3 .. ": " .. c2 .. User:getKey('kills') .. c3 .. "/" .. c2 .. User:getKey('botkills')
    -- Deaths : deaths/botdeaths
    t[tlen(t)+1] = "Deaths     " .. c3 .. ": " .. c2 .. User:getKey('deaths') .. c3 .. "/" .. c2 .. User:getKey('botdeaths')
    -- TeamKills : teamkills/botteamkills
    t[tlen(t)+1] = "TeamKills  " .. c3 .. ": " .. c2 .. User:getKey('teamkills') .. c3 .. "/" .. c2 .. User:getKey('botteamkills')
    -- TeamDeaths : teamdeaths/botteamdeaths
    t[tlen(t)+1] = "TeamDeaths " .. c3 .. ": " .. c2 .. User:getKey('teamdeaths') .. c3 .. "/" .. c2 .. User:getKey('botteamdeaths')
    -- Self/World : selfkills/worlddeaths
    t[tlen(t)+1] = "Self/World " .. c3 .. ": " .. c2 .. User:getKey('selfkills') .. c3 .. "/" .. c2 .. User:getKey('worlddeaths')
    
    return t
end

local _ViewAuth = function(User)
    local usertable = { }
    local c1 = Color.Primary
    local c2 = Color.Secondary
    local c3 = Color.Tertiary
    local titlelength = Misc:table_longestString(Keys,"title")
    for k = 1 , tlen(Keys) do 
        if Misc:trim(User:getKey(Keys[k].name)) ~= "" then
            usertable[tlen(usertable)+1] = Misc:GetStaticString(Keys[k].title,titlelength) .. c3 .. " : " .. c2 .. tostring(User:getKey(Keys[k].name))
        end
    end
    return usertable
end

--    Username    --    Join Date    --    Last Seen    --
local _ViewAll = function(maxchars,maxrows,page)
    local Data = { }
    for k = 1 , tlen(Users) do 
        local User     = Users[k]
        local UserData = { }
        UserData[1]    = User:getKey("username")
        UserData[2]    = os.date("%c",User:getKey("joindate"))
        UserData[3]    = os.date("%c",User:getKey("lastseen"))
        UserData[4]    = Misc:SecondsToClock( User:getKey("timeplayed") )
        Data[tlen(Data)+1]  = UserData
    end
    if not next(Data) then return nil end
    return Misc:FormatTable(Data,{"Username","Join Date","Last Seen","Time Played"},false,maxchars,maxrows,page)
end

--    Username    --          Away For           --          AFK Reason         --
local _ViewOnline = function(maxchars,maxrows,page)
    local Data = { }
    for k = 1 , tlen(Clients) do 
        local Client = Clients[k]
        if Client:isOnline() then
            local OnlineUser = { }
            OnlineUser[tlen(OnlineUser)+1] = Client.User:getKey('username')
            OnlineUser[tlen(OnlineUser)+1] = Client:getName()
            
            local AwayTime = Client.User:getKey("afktime")
            if AwayTime == 0 then
                AwayTime = "Not away"
            else
                AwayTime = Misc:SecondsToClock( os.time() - Client.User:getKey("afktime") )
            end
            local AwayReason = Client.User:getKey("afkreason")
            OnlineUser[tlen(OnlineUser)+1] = AwayTime
            OnlineUser[tlen(OnlineUser)+1] = AwayReason
            
            Data[tlen(Data)+1] = OnlineUser
        end
    end
    if not next(Data) then return nil end -- No Users
    return Misc:FormatTable(Data, { "Username","Name","Away Time","AFK Reason" } ,false,maxchars,maxrows,page)
end


ViewCommand.CORE           = true
ViewCommand.ADMIN          = true
ViewCommand.CHAT           = true
ViewCommand.LUA            = true
ViewCommand.AUTH           = true
ViewCommand.CONSOLE        = true
ViewCommand.REQUIRE_LOGIN  = true
ViewCommand.DESCRIPTION    = { "View your or other users user profile." }
ViewCommand.ALIASES        = { "v","show","display", "profile", }
ViewCommand.SYNTAX         = {
    { ""           , "Views a list of all online users"},
    { "online"     , "Views a list of all online users"},
    { "all"        , "Views a list of all users"},
    { "<username>" , "View <username>'s profile"},
    { "<username> <key>" , "View <username>'s profile key"},
}

function ViewCommand:Run(Client,Args)
    local PrintTable = { }
    if not Args[1] or Misc:triml(Args[1]) == "online" then
        PrintTable = _ViewOnline( Client:getMaxChars() , Client:getMaxRows() , tonumber(Args[2]) )
        if not PrintTable or not next(PrintTable) then return self:Error("No online users") end
    elseif Misc:triml(Args[1]) == "all" then
        PrintTable = _ViewAll( Client:getMaxChars() , Client:getMaxRows() , tonumber(Args[2]) )
        if not PrintTable or not next(PrintTable) then return self:Error("No users found") end
    else
        local Target = UserHandler:getUser(Args[1])
        if not Target then return self:Error("Could not find target user") end
        
        if Args[2] then -- Viewing keys
            local key = Misc:triml(Args[2])
            for k = 1 , tlen(Keys) do 
                if Keys[k].name == key then key = Keys[k] end
            end
            if type(key) ~= "table" then return self:Error("Could not find that key") end
            if not Client:isConsole() and not Client:isAuth() then
                if ( key.auth or key.name == "password" ) then return self:Error("Do not have access to this key") end
            end
            local value = tostring(Target:getKey(key.name))
            self:Info(Color.Tertiary .. "[" .. Color.Secondary .. string.upper(key.type) .. Color.Tertiary .. "]" .. Color.Primary .. key.name .. Color.Tertiary .. ": " .. Color.Secondary .. value)
            self:InfoClean("EDITABLE" .. Color.Tertiary .. ":" .. Color.Secondary .. tostring(key.editable) .. Color.Primary .. " AUTH" .. Color.Tertiary .. ":" .. Color.Secondary .. tostring(key.auth))
            if key.type == "number" or key.type == "string" then
                self:InfoClean("RANGE" .. Color.Tertiary .. ": " .. Color.Secondary .. key.min .. Color.Tertiary .. " - " .. Color.Secondary .. key.max)
                -- Print pattern if string dunno how
            end
            return self.SUCCESS
        end
        
        if Client:isConsole() or Client:isAuth() then
            PrintTable = _ViewAuth(Target)
        else
            PrintTable = _View(Target)
        end
        if not PrintTable or not next(PrintTable) then return self:Error("Error getting info on user") end
    end
    for k = 1 , tlen(PrintTable) do
        --et.G_LogPrint(PrintTable[k] .. '\n')
        if not string.find(PrintTable[k],"%",1,true) then -- TODO FIX
            self:InfoClean(PrintTable[k])
        end
    end
    return self.SUCCESS
end

return ViewCommand