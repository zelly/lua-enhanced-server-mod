local PlaySoundCommand = Command("PlaySound")

PlaySoundCommand.CORE           = true
PlaySoundCommand.ADMIN          = true
PlaySoundCommand.CHAT           = true
PlaySoundCommand.LUA            = true
PlaySoundCommand.AUTH           = true
PlaySoundCommand.GLOBALOUTPUT   = true
PlaySoundCommand.DESCRIPTION    = {
                                   "Plays a sound from sound list. ",
                                   "You need to have the correct level to use the sound",
                                   "Authorized users can edit title,message,tags,level,and alias"
                                  }
PlaySoundCommand.ALIASES        = { "play","p","ps",}
PlaySoundCommand.SYNTAX         = {
    { "","Shows list of all sounds you have access to", },
    { "<soundname>", "Plays <soundname> if available", },
    { "<partialsoundname>","Plays <partialsoundname> if available, if there are multiple matches will display all of them", },
    { "tags/tag <tagname>", "Shows list of available sounds with <tagname>", },
    { "savesounds","Saves sounds(Auth)", },
    { "<soundname> alias <alias>", "Adds alias (Needs saving)(Auth)", },
    { "<soundname> title <title>", "Edits title(Needs saving)(Auth)", },
    { "<soundname> level <level>", "Edits level (Needs saving)(Auth)", },
    { "<soundname> message <desc>", "Edits description(Needs saving)(Auth)", },
    { "<soundname> tag <tag>", "Adds tag(Needs saving)(Auth)", },
}

function PlaySoundCommand:Run(Client,Args)
    if not Config.Sound.Enabled then
        return self:Info("Server has sounds disabled.")
    elseif Client:isOnline() and not Client.User:getKey("sounds") then
        return self:Info("You have sounds disabled")
    end
    local maxChars = Client:getMaxChars()
    local maxRows  = Client:getMaxRows()
    if Args[1] then Args[1] = string.lower(Args[1]) end
    if not Args[1] or ( Args[1] == "all" or Args[1] == "list" or Args[1] == "l" or Args[1] == 'page' ) then -- List all available sounds.
        local clientSounds = Sound:canUse(Client,Sound.Sounds)
        local Data         = { }
        
        for k = 1 , tlen(clientSounds) do Data[tlen(Data)+1] = clientSounds[k].title end
        
        if not next(Data) then return self:Error("There are no available sounds for you") end
        
        local PrintTable = Misc:FormatList(Data , false , maxChars , maxRows , tonumber(Args[2]) )
        self:Info("Displaying " .. Color.Secondary .. tlen(Data) .. Color.Primary .. " sounds")
        for p = 1 , tlen(PrintTable) do self:InfoClean(PrintTable[p]) end
        return self.SUCCESS
    elseif Args[1] == "tags" or Args[1] == "tag" then
        if not Args[2] then -- List all tags
            local tags = Sound:getTagList()
            if not next(tags) then return self:Error("There are no tags set") end
            self:Info("Displaying " .. Color.Secondary .. tlen(tags) .. Color.Primary .. " tags")
            local TagPrintTable = Misc:FormatList(tags,false,maxChars,maxRows,tonumber(Args[3]) )
            for k = 1 , tlen(TagPrintTable) do self:InfoClean(TagPrintTable[k]) end
            return self.SUCCESS
        end
        
        table.remove(Args,1) -- No longer need "tags"
        
        local soundList = Sound:canUse( Client , Sound:findWithTag(Args) ) -- each Arg is now a tag
        if not next(soundList) then self:Error("Could not find any sounds.") end
        
        self:Info("List of matching sounds with the tags: " .. Color.Secondary .. table.concat(Args,Color.Tertiary .. "," .. Color.Secondary))
        local PrintTable = Misc:FormatList(FoundSounds,false,maxChars,maxRows,tonumber(Args[tlen(Args)]) )
        for j = 1 , tlen(PrintTable) do self:InfoClean(PrintTable[j]) end
        return self.SUCCESS
    elseif Args[1] == "savesounds" and Client:isAuth() then
        Sound:Save()
        self:Info("Saved sounds")
        return self.SUCCESS
    else
        local soundname = Args[1]
        if string.len(soundname) <= 1 then return self:Error("Sound name too short") end -- require at least 2 characters
        local soundList  = Sound:canUse( Client , Sound:find(soundname) )
        if not next(soundList) then return self:Error("Could not find any sounds with that name") end

        if tlen(soundList) > 1 then
            local d = { }
            for k = 1 , tlen(soundList) do d[tlen(d)+1] = soundList[k].title end
            self:Info(Color.Secondary .. tlen(soundList) .. Color.Primary .. " Sounds were found!")
            local PT = Misc:FormatList(d,false,maxChars,maxRows,tonumber(Args[2]))
            for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
            return self.SUCCESS
        end

        local FoundSound = soundList[1] -- Only one left in list
        if ( Client:isAuth() and Args[2] ) then
            Args[2] = Misc:triml(Args[2])
            if Args[2] == "title" then
                local title = Misc:triml(Args[3])
                if string.len(title) < 1 then return self:Error("Title too short") end
                local ot = FoundSound.title
                FoundSound.title   = title
                FoundSound.aliases[tlen(FoundSound.aliases)+1] = ot
                self:Info("Changed title from " .. ot .. " to " .. title)
                return self.SUCCESS
            elseif Args[2] == "alias" then
                local alias = Misc:triml(Args[3])
                if string.len(alias) < 1 then return self:Error("Alias too short") end
                if Misc:table_find(FoundSound.aliases,alias) then return self:Error("Alias already exists") end
                FoundSound.aliases[tlen(FoundSound.aliases)+1] = alias
                self:Info("Added " .. alias .." alias to " .. FoundSound.title)
                return self.SUCCESS
            elseif ( Args[2] == "tag" or Args[2] == "tags") then
                local tag = Misc:triml(Args[3])
                if string.len(tag) < 1 then return self:Error("Tag too short") end
                if Misc:table_find(FoundSound.tags,tag) then return self:Error("Tag already exists") end
                FoundSound.tags[tlen(FoundSound.tags)+1] = tag
                self:Info("Added " .. tag .." tag to " .. FoundSound.title)
                return self.SUCCESS
            elseif Args[2] == "level" then
                local level = tonumber(Args[3])
                if not level then return self:Error("Level must be a number") end
                FoundSound.level = level
                self:Info("Level set to " .. tostring(level) .." in " .. FoundSound.title)
                return self.SUCCESS
            elseif Args[2] == "message" then
                local message = Misc:trim( table.concat( Args , " " , 3 ) )
                FoundSound.message = message
                self:Info("Edited the message of " .. FoundSound.title)
                return self.SUCCESS
            end
        end
        Sound:playAll(FoundSound,Client)
        return self.SUCCESS
    end
end

return PlaySoundCommand