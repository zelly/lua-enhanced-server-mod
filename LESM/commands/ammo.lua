local AmmoCommand = Command("Ammo")

local _GiveAmmo = function(self,Target,ammo_amount)
    Target:setAmmo(Target:getWeapon(),Target:getAmmo(Target:getWeapon()) + ammo_amount)
    self:Info("Gave " .. Target:getName() .. Color.Secondary .. " " .. ammo_amount .. Color.Primary .. " bullets!")
end

AmmoCommand.ADMIN          = true
AmmoCommand.CHAT           = true
AmmoCommand.LUA            = true
AmmoCommand.AUTH           = true
AmmoCommand.CONSOLE        = true
AmmoCommand.REQUIRE_LOGIN  = true
AmmoCommand.REQUIRE_AUTH   = true
AmmoCommand.DESCRIPTION    = { "Gives specified ammo amount.", }
AmmoCommand.SYNTAX         = {
    { "","Give yourself 30 bullets" },
    { "<clientname|clientid>", "Give target 30 bullets" },
    { "<clientname|clientid> <bullets>", "Give target <bullets> bullets" },
}

function AmmoCommand:Run(Client,Args)
    if not Args[1] then
        _GiveAmmo(self,Client,30)
        return self.SUCCESS
    end
    if not Args[2] then
        if tonumber(Args[1]) then
            _GiveAmmo(self,Client,tonumber(Args[1]))
            return self.SUCCESS
        end
        local Target = ClientHandler:getClient(Args[1])
        if not Target then
            self:Error("Could not find client")
            return self:getHelp()
        end
        _GiveAmmo(self,Target,30)
        return self.SUCCESS
    end
    local Target = ClientHandler:getClient(Args[1])
    Args[2] = tonumber(Args[2])
    if not Args[2] then Args[2] = 30 end
    if not Target then
        self:Error("Could not find client")
        return self:getHelp()
    end
    _GiveAmmo(self,Target,Args[2])
    return self.SUCCESS
end

return AmmoCommand