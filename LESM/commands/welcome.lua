local WelcomeCommand = Command("Welcome")

WelcomeCommand.ADMIN          = true
WelcomeCommand.CHAT           = true
WelcomeCommand.LUA            = true
WelcomeCommand.AUTH           = true
WelcomeCommand.GLOBALOUTPUT   = true
WelcomeCommand.DESCRIPTION    = { "Welcomes players", }
WelcomeCommand.SYNTAX         = {
    { "" , "Say welcome!" },
    { "<client>", "Say welcome to specified person" },
    { "@last", "Say welcome to last person that connected" },
}

function WelcomeCommand:Run(Client,Args)
    Sound:playAll(Config.Sound.Welcome,Client)
    if Args[1] then
        self:InfoAll(Client:getName() .. Color.Primary .. " welcomes " .. MessageHandler:filter(table.concat(Args," "),Client,nil,nil,true) .. Color.Primary .. " to the server")
    else
        self:InfoAll(Client:getName() .. Color.Primary .. " welcomes you to the server")
    end
    return self.SUCCESS
end

return WelcomeCommand