local NoColorsCommand = Command("NoColors")

NoColorsCommand.ADMIN          = true
NoColorsCommand.CHAT           = true
NoColorsCommand.LUA            = true
NoColorsCommand.AUTH           = true
NoColorsCommand.CONSOLE        = true
NoColorsCommand.REQUIRE_LEVEL  = Config.Level.Leader
NoColorsCommand.DESCRIPTION    = { "Toggle nocolors for client" , }
NoColorsCommand.ALIASES        = { "decolor",}
NoColorsCommand.SYNTAX         = {
    { "<target>", "Enables nocolors for target",},
}


function NoColorsCommand:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Could not find target") end
    if ( Target.id == Client.id ) and not Client:isAuth() then return self:Error("Cannot use this on yourself") end
    if ( Target:getLevel() > Client:getLevel() ) and not Client:isAuth() then return self:Error(Target:getName() .. Color.Primary .. " has higher level than you!") end
    if not Target.rudclient then return self:Error("Target does not have rudclient") end
    if Target.rudclient.nocolors then
        Target.rudclient.nocolors = false
        self:Info("Disabled nocolors for " .. Target:getName())
        return self.SUCCESS
    else
        Target.rudclient.nocolors = true
        self:Info("Enabled nocolors for " .. Target:getName())
        return self.SUCCESS
    end
end

return NoColorsCommand