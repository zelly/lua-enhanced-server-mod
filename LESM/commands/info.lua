local Info = Command("Info")

Info.ALIASES        = { "lesm" , "luaesmod" , "zelly" , }
Info.CORE           = true
Info.ADMIN          = true
Info.CHAT           = true
Info.LUA            = true
Info.AUTH           = true
Info.CONSOLE        = true
Info.DESCRIPTION    = { "Displays info about this lua.", }

function Info:Run(Client,Args)
    if Args[1] and Misc:triml(Args[1]) == "license" or Misc:triml(Args[1]) == "copyright" or Misc:triml(Args[1]) == "copy" then
        self:Info("^zSee console for details")
        if Client:isConsole() then
            self:InfoClean(Core.LICENSE)
        else
            Client:Print(Misc:trim(Core.LICENSE))
        end
        return self.SUCCESS
    end
    self:Info(Core.LONGNAME .. Color.Tertiary .. "(" .. Color.Secondary .. Core.VERSION .. Core.SUB_VERSION .. Color.Tertiary .. ") " .. Color.Primary .. "^zSee console")
    self:InfoClean(Core.DESCRIPTION .. Color.Primary .. " Created by " .. Color.Secondary .. Core.AUTHOR)
    self:InfoClean("Website: " .. Core.URL .. Color.Primary .. " Contact: " .. Core.CONTACT)
    self:InfoClean("I am looking for people to assist in development, whether it be ideas or actual lua coding.")
    return self.SUCCESS
end

return Info