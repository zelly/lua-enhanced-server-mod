local ConfigCommand = Command("Config")

ConfigCommand.ADMIN          = true
ConfigCommand.CHAT           = true
ConfigCommand.LUA            = true
ConfigCommand.AUTH           = true
ConfigCommand.CONSOLE        = true
ConfigCommand.REQUIRE_LEVEL  = Config.Level.Leader
ConfigCommand.DESCRIPTION    = { "Able to view the servers lua config",
                                 "Can also temporarily change the values for the map",
                                 "Note this is also case sensitive",
                               }
ConfigCommand.ALIASES        = { "conf","cfg",}
ConfigCommand.SYNTAX         = {
    { ""              , "Gets list of all config keys and what they are" },
    { "save"              , "Saves config" },
    { "<key>"         , "Gets <key>'s value" },
    { "<key> <value>" , "Sets <key>'s value to <value>" },
}
ConfigCommand.EXAMPLES       = {
    { "MapEnts", "Would return true or false based on Config.MapEnts"},
    { "MapEnts true", "Would set Config.MapEnts to true"},
}

function ConfigCommand:Run(Client,Args)
    
    -- List config options
    if not Args[1] or Misc:triml(Args[1]) == 'list' or Misc:triml(Args[1]) == "page" then -- LIST ALL
        local page     = tonumber(Args[1])
        local Data     = { }
        local collen_1 = { }
        local collen_2 = { }
        if not Args[1] == "list" or Args[1] == "page" then page = tonumber(Args[2]) end
        for k = 1 , tlen(DefaultOptions) do 
            collen_1[tlen(collen_1)+1] = DefaultOptions[k].name
            collen_2[tlen(collen_2)+1] = tostring(Config[DefaultOptions[k].name])
            local value = ""
            if type(Config[DefaultOptions[k].name]) == "table" then
                value = Misc:GetStaticString(tlen(DefaultOptions[k].default),2) .. Color.Primary .. " keys"
            else
                value = Config[DefaultOptions[k].name]
            end
            Data[tlen(Data)+1] = { 
                DefaultOptions[k].name,
                Color.Secondary .. tostring(value),
            }
        end
        
        local PrintTable = Misc:FormatTable(Data,{"Key","Value"}, false, Client:getMaxChars(), Client:getMaxRows(), page )
        self:Info("Showing " .. Color.Secondary .. tlen(Data) .. Color.Primary .. " config keys")
        for k = 1 , tlen(PrintTable) do 
            self:InfoClean(PrintTable[k])
        end
        return self.SUCCESS
    elseif string.lower(Args[1]) == "save" then
        ConfigHandler:Save()
        self:Info("Configuration saved!")
        return self.SUCCESS
    else
        local configkey = ConfigHandler:getOption(Args[1])
        if configkey == nil then
            self:Error("No such key " .. Args[1])
            return self.SUCCESS
        end
        if Args[2] then
            ConfigHandler:setOption(Args[1],table.concat(Args," ",2))
            self:Info( Color.Secondary .. Args[1] .. Color.Tertiary .. " = " .. Color.Secondary .. tostring(table.concat(Args," ",2)) )
            return self.SUCCESS
        else
            if type(configkey) == "table" then
                local Data = { }
                for i,v in pairs(configkey) do
                    Data[tlen(Data)+1] = { i , tostring(v) }
                end
                local PrintTable = Misc:FormatTable(Data,{"Key","Value"},false,Client:getMaxChars())
                self:Info("Showing " .. Color.Secondary .. tlen(Data) .. Color.Primary .. " config keys in " .. Color.Secondary .. Args[1])
                for k = 1 , tlen(PrintTable) do 
                    self:InfoClean(PrintTable[k])
                end
                return self.SUCCESS
            end
            self:Info( Color.Secondary .. Args[1] .. Color.Tertiary .. " = " .. Color.Secondary .. tostring(configkey) )
        end
    end
end

return ConfigCommand