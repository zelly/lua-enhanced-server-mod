local RoutinesCommand = Command("Routines")

RoutinesCommand.ADMIN          = true
RoutinesCommand.CHAT           = true
RoutinesCommand.LUA            = true
RoutinesCommand.AUTH           = true
RoutinesCommand.CONSOLE        = true
RoutinesCommand.REQUIRE_LOGIN  = true
RoutinesCommand.REQUIRE_AUTH   = true
RoutinesCommand.DESCRIPTION    = { "Displays runframe routines", }
RoutinesCommand.ALIASES        = { "runframe", }
RoutinesCommand.SYNTAX         = {
    { "","Shows all runframe routines" }, 
}

-- TODO Should I keep these @ milliseconds?
function RoutinesCommand:Run(Client,Args)
    if not next(RoutineHandler.Routines) then return self:Info("No routines defined") end
    local RoutineData = { }
    
    for k=1, tlen(RoutineHandler.Routines) do
        local routine = RoutineHandler.Routines[k]
        local loop;
        local run;
        local last;
        
        if routine.loop == 0 then
            loop = "No"
        else
            loop = "Every " .. math.floor(routine.loop/1000) .. " seconds"
        end
        
        if ( routine.run == 0 and routine.last ~= 0 ) or ( routine.run ~= 0 and routine.last ~= 0 ) then
            run = "In " .. math.floor( ((routine.last + routine.loop) - Game:getLevelTime())/1000 ) .. " seconds"
        elseif routine.run == 0 and routine.last == 0 then
            run = "Now" -- Is this right?
        elseif routine.run ~= 0 and routine.last == 0 then
            run = "In " .. math.floor( (routine.run - Game:getLevelTime())/1000 ) .. " seconds"
        end
        
        if routine.last == 0 then
            last = "Never"
        else
            last = math.floor( (Game:getLevelTime() - routine.last) / 1000 ) .. " seconds ago"
        end
        
        RoutineData[tlen(RoutineData)+1] = {
            routine.id,
            loop,
            run,
            last,
            routine.desc,
            }
    end
    if not next(RoutineData) then return self:Error("No data gathered from routines") end
    local PT = Misc:FormatTable( RoutineData , {"ID","LOOP","NEXT RUN","LAST RUN","DESCRIPTION"} , false , Client:getMaxChars() , Client:getMaxRows() , tonumber(Args[1]))
    if not next(PT) then return self:Error("Could not form print table") end
    for k=1,tlen(PT) do
        self:InfoClean(PT[k])
    end
    return self.SUCCESS
end

return RoutinesCommand