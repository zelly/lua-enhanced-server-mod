local MergeCommand = Command("Merge")
local _Merge = function(TargetUser1,TargetUser2)
    local TargetUserClient = TargetUser1:getOnlineClient()
    if ( TargetUserClient ~= nil ) then TargetUserClient:Logout(true) end
    local backup = { }
    for k = 1 , tlen(Keys) do 
        backup[Keys[k].name] = Misc:ConvertType(Keys[k].type,TargetUser1[Keys[k].name])
    end
    FileHandler:Save{ filePath = "LESM/save/rm_" .. TargetUser1:getKey("username") .."_" .. os.time() .. ".json",fileData = backup }
    if ( TargetUser2:getKey("age") == 0 ) then TargetUser2:update("age",TargetUser1:getKey("age")) end
    if ( TargetUser2:getKey("autologinguid") == "" ) then TargetUser2:update("autologinguid",TargetUser1:getKey("autologinguid")) end
    if ( TargetUser2:getKey("autologinsid") == "" ) then TargetUser2:update("autologinsid",TargetUser1:getKey("autologinsid")) end
    if ( TargetUser2:getKey("autologinip") == "" ) then TargetUser2:update("autologinip",TargetUser1:getKey("autologinip")) end
    if ( TargetUser2:getKey("forumsname") == "" ) then TargetUser2:update("forumsname",TargetUser1:getKey("forumsname")) end
    if ( TargetUser2:getKey("xfire") == "" ) then TargetUser2:update("xfire",TargetUser1:getKey("xfire")) end
    if ( TargetUser2:getKey("realname") == "" ) then TargetUser2:update("realname",TargetUser1:getKey("realname")) end
    if ( TargetUser2:getKey("lastseen") < TargetUser1:getKey("lastseen") ) then TargetUser2:update("lastseen",TargetUser1:getKey("lastseen")) end
    if ( TargetUser2:getKey("timeout") < TargetUser1:getKey("timeout") ) then TargetUser2:update("timeout",TargetUser1:getKey("timeout")) end
    if ( TargetUser2:getKey("joindate") > TargetUser1:getKey("joindate") ) then TargetUser2:update("joindate",TargetUser1:getKey("joindate")) end
    TargetUser2:update("kills",( TargetUser1:getKey("kills") + TargetUser2:getKey("kills") ))
    TargetUser2:update("deaths",( TargetUser1:getKey("deaths") + TargetUser2:getKey("deaths") ))
    TargetUser2:update("botkills",( TargetUser1:getKey("botkills") + TargetUser2:getKey("botkills") ))
    TargetUser2:update("botdeaths",( TargetUser1:getKey("botdeaths") + TargetUser2:getKey("botdeaths") ))
    TargetUser2:update("selfkills",( TargetUser1:getKey("selfkills") + TargetUser2:getKey("selfkills") ))
    TargetUser2:update("teamkills",( TargetUser1:getKey("teamkills") + TargetUser2:getKey("teamkills") ))
    TargetUser2:update("teamdeaths",( TargetUser1:getKey("teamdeaths") + TargetUser2:getKey("teamdeaths") ))
    TargetUser2:update("timeplayed",( TargetUser1:getKey("timeplayed") + TargetUser2:getKey("timeplayed") ))
    TargetUser2:update("karmagoodin",( TargetUser1:getKey("karmagoodin") + TargetUser2:getKey("karmagoodin") ))
    TargetUser2:update("karmagoodout",( TargetUser1:getKey("karmagoodout") + TargetUser2:getKey("karmagoodout") ))
    TargetUser2:update("karmabadin",( TargetUser1:getKey("karmabadin") + TargetUser2:getKey("karmabadin") ))
    TargetUser2:update("karmabadout",( TargetUser1:getKey("karmabadout") + TargetUser2:getKey("karmabadout") ))
    for k=1,tlen(TargetUser1.silent) do TargetUser2:updateNotFound("silent",TargetUser1.silent[k]) end
    for k=1,tlen(TargetUser1.ip) do TargetUser2:updateNotFound("ip",TargetUser1.ip[k]) end
    for k=1,tlen(TargetUser1.mac) do TargetUser2:updateNotFound("mac",TargetUser1.mac[k]) end
    for k=1,tlen(TargetUser1.guid) do TargetUser2:updateNotFound("guid",TargetUser1.guid[k]) end
    for k=1,tlen(TargetUser1.alias) do TargetUser2:updateNotFound("alias",TargetUser1.alias[k]) end
    UserHandler:delete(TargetUser1.id)
end

MergeCommand.AUTH           = true
MergeCommand.CONSOLE        = true
MergeCommand.REQUIRE_LOGIN  = true
MergeCommand.REQUIRE_AUTH   = true
MergeCommand.DESCRIPTION    = { "Merges two users." , "BE CAREFUL DATA WILL BE LOST" , "A backup of the merged player is made in save directory" }
MergeCommand.SYNTAX         = {
    { "<username1> <username2>", "Merges <username1> INTO <username2> (Second username takes priority)"},
}

function MergeCommand:Run(Client,Args)
    local TargetUser1 = UserHandler:getUser(Args[1])
    local TargetUser2 = UserHandler:getUser(Args[2])
    if not TargetUser1 then
        self:Error("Could not find first user")
        return self:getHelp()
    end
    if not TargetUser2 then
        self:Error("Could not find second user")
        return self:getHelp()
    end
    self:Info("Merging " .. TargetUser1:getKey("username") .. " into " .. TargetUser2:getKey("username"))
    _Merge(TargetUser1,TargetUser2)
    return self.SUCCESS
end

return MergeCommand