local Back = Command("Back")

Back.CORE           = true
Back.ADMIN          = true
Back.CHAT           = true
Back.LUA            = true
Back.DESCRIPTION    = { 
                       "Notify server that you are back from being away.",
                       "If you join a team, this command is automatically executed.",
                       }
Back.SYNTAX         = { 
    {"","Notify server that you have returned", },
}


function Back:Run(Client,Args)
    if not Client.isAfk then return self:Error("You have not gone afk") end
    Sound:playAll(Config.Sound.Return,Client)
    local afktime   = 0
    local afkreason = Client.afkReason
    if Client:isOnline() then
        afktime   = Client.User:getKey("afktime")
        afkreason = Client.User:getKey("afkreason")
        Client.User:update( "afk"       , false)
        Client.User:update( "afkreason" , ""   )
        Client.User:update( "afktime"   , 0    )
    end
    local message = Client:getName() .. Color.Primary .. " has returned from " .. Color.Secondary
    if afkreason ~= "" then
        message = message .. afkreason
    else
        message = message .. "being away."
    end
    if afktime ~= 0 then message = message .. Color.Primary .. " After being away for " .. Color.Secondary .. Misc:SecondsToClock(os.time() - afktime) end
    Console:IteratePrint(message , "chat" , "afklocation" , Client )
    Client.afkReason = ""
    Client.isAfk     = false
    if Client:isOnline() then Mail:CheckTotal(Client) end
    return self.SUCCESS
end

return Back