local DoubleJumpCommand = Command("DoubleJump")

DoubleJumpCommand.ADMIN          = true
DoubleJumpCommand.CHAT           = true
DoubleJumpCommand.LUA            = true
DoubleJumpCommand.AUTH           = true
DoubleJumpCommand.CONSOLE        = true
DoubleJumpCommand.REQUIRE_LEVEL  = Config.Level.Leader
DoubleJumpCommand.DESCRIPTION    = { "Toggles double jump", }
DoubleJumpCommand.ALIASES        = { "dj","toggledj","toggledoublejump", }
DoubleJumpCommand.REQUIRE_MOD    = { "silent", "nq" ,"noquarter"}

function DoubleJumpCommand:Run(Client,Args)
    if ( Game:Mod() == "silent" ) then
        if ( Game:bitCvarToggle("g_misc",1) ) then
            self:InfoAll("Enabled double jump!")
        else
            self:InfoAll("Disabled double jump!")
        end
        return self.SUCCESS
    elseif ( Game:Mod() == "nq" ) or ( Game:Mod() == "noquarter" ) then
        local g_doublejump = tonumber(et.trap_Cvar_Get("g_doublejump"))
        if ( g_doublejump == 1 ) or ( g_doublejump == 2 ) then
            et.trap_Cvar_Set("g_doublejump",0)
            self:InfoAll("Disabled Double Jump!")
        else
            et.trap_Cvar_Set("g_doublejump",1)
            self:InfoAll("Enabled Double Jump!")
        end
        return self.SUCCESS
    else
        self:Error("Mod not supported")
        return self:getHelp()
    end
end

return DoubleJumpCommand