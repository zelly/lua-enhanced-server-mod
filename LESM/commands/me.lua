local MeCommand = Command("Me")

local _Color1 = "^z"
local _Color2 = "^g"

MeCommand.CORE           = true
MeCommand.ADMIN          = true
MeCommand.CHAT           = true
MeCommand.LUA            = true
MeCommand.AUTH           = true
MeCommand.REQUIRE_LOGIN  = false
MeCommand.GLOBALOUTPUT   = true
MeCommand.DESCRIPTION    = { "Roleplay-like command" ,
                        "For making your own type of commands",
                        "You can also use @clientname to target specfic people",
                        "You can also use #killer , #victim , #revive , #health , and #ammo",
                        "Which is client that did that event to you. e.g. last person that revived you for #revive.",
                        "With this you could make your own binds too.",
                      }
MeCommand.SYNTAX         = {
    { "<message...>", "Displays message"},
}
MeCommand.EXAMPLES       = {
    { "waves hello!" , "Displays: * Steve waves hello! *"},
    { "waves at @bob", "Displays: * Steve waves at SuperBob123 *"},
}

function MeCommand:Run(Client,Args)
    local message = table.concat(Args," ")
    if Misc:trim(et.Q_CleanStr(message)) == "" then return self:Error("Requires message") end
    message = MessageHandler:filter(message,Client)
    Console:IteratePrint( _Color1 .. "* ^7" .. Client:getName() .. " "  .. _Color2 .. message .. " " .. _Color1 .. "*","chatclean",nil,Client)
    return self.SUCCESS
end

return MeCommand