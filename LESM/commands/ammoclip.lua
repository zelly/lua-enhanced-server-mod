local AmmoClipCommand = Command("AmmoClip")

local _GiveAmmo = function(self,Target,ammo_amount)
    Target:setAmmoClip(Target:getWeapon(),Target:getAmmoClip(Target:getWeapon()) + ammo_amount)
    self:Info("Gave " .. Target:getName() .. Color.Secondary .. " " .. ammo_amount .. Color.Primary .. " bullets!")
end

AmmoClipCommand.ADMIN          = true
AmmoClipCommand.CHAT           = true
AmmoClipCommand.LUA            = true
AmmoClipCommand.AUTH           = true
AmmoClipCommand.CONSOLE        = true
AmmoClipCommand.REQUIRE_LOGIN  = true
AmmoClipCommand.REQUIRE_AUTH   = true
AmmoClipCommand.DESCRIPTION    = { "Gives specified ammo amount directly to the current clip", }
AmmoClipCommand.SYNTAX         = {
    { "","Give yourself 30 bullets in the clip" },
    { "<clientname|clientid>", "Give target 30 bullets" },
    { "<clientname|clientid> <bullets>", "Give target <bullets> bullets" },
}

function AmmoClipCommand:Run(Client,Args)
    if not Args[1] then
        _GiveAmmo(self,Client,30)
        return self.SUCCESS
    end
    if not Args[2] then
        if tonumber(Args[1]) then
            _GiveAmmo(self,Client,tonumber(Args[1]))
            return self.SUCCESS
        end
        local Target = ClientHandler:getClient(Args[1])
        if not Target then
            self:Error("Could not find client")
            return self:getHelp()
        end
        _GiveAmmo(self,Target,30)
        return self.SUCCESS
    end
    local Target = ClientHandler:getClient(Args[1])
    Args[2] = tonumber(Args[2])
    if not Args[2] then Args[2] = 30 end
    if not Target then
        self:Error("Could not find client")
        return self:getHelp()
    end
    _GiveAmmo(self,Target,Args[2])
    return self.SUCCESS
end

return AmmoClipCommand