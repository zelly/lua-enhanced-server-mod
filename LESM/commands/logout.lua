local Logout = Command("LogoutCommand")

Logout.NAME           = "logout"
Logout.CORE           = true
Logout.ADMIN          = true
Logout.CHAT           = true
Logout.LUA            = true
Logout.AUTH           = true
Logout.REQUIRE_LOGIN  = true
Logout.DESCRIPTION    = { "Logs out of your user profile." }
Logout.ALIASES        = { "signout", }

function Logout:Run(Client,Args)
    if ( tlen(Client.pings) >= 10 ) then
        local average = 0
        for k=1,tlen(Client.pings) do
            average = ( average + Client.pings[k] )
        end
        average = ( average / tlen(Client.pings) )
        while ( tlen(Client.User.pings) >= 100 ) do
            table.remove(Client.User.pings,1)
        end
        Client.User.pings[tlen(Client.User.pings)+1] = average
    end
    Client.User:updateFields(Client)
    Client:saveXp()
    Client.User:update("afk",false)
    Client.User:update("afkreason","")
    Client.User:update("afktime",0)
    if ( Client.User:getKey("autologin") ) then
        self:Info("You still have autologin enabled, you will be automatically logged in next connect")
    else
        Client.User:update("timeout",0) -- No autologin
    end
    Client.User = nil
    Client:Play(Config.Sound.Logout)
    self:Info("Logged out!")
end

return Logout