local Help = Command("Help")

local _FindCommands = function(availableCommands,cmdtype)
    local MatchedCommands = { }
    for k = 1 , tlen(availableCommands) do 
        local cmd = availableCommands[k]
        local thiscmdtype = cmd:getCommandType()
        local skip = false
        if cmdtype ~= '' and thiscmdtype ~= cmdtype then skip = true end
        -- Maybe allow these two? And make them admin color
        --if cmd.NAME == 'help' then skip = true end
        --if cmd.OVERWRITE and Game:Shrubbot() then skip = true end
        if not skip then MatchedCommands[tlen(MatchedCommands)+1] = Color[thiscmdtype] .. cmd.NAME end
        
    end
    return MatchedCommands
end

Help.ALIASES        = { "luahelp","lhelp" }
Help.CORE           = true
Help.ADMIN          = true
Help.CHAT           = true
Help.LUA            = true
Help.AUTH           = true
Help.MAIL           = true
Help.CONSOLE        = true
Help.OVERWRITE      = true
Help.DESCRIPTION    = { "Displays commands available to you or help on a specific command.", }
Help.SYNTAX         = {
    { ""          , "Displays full list of available commands" },
    { "<command>" , "Displays specific details on that command" },
    { "auth"      , "Displays available authorized commands" },
    { "lua"       , "Displays available lua commands" },
    { "admin"     , "Displays available admin commands" },
    { "mail"      , "Displays available mail commands" },
    { "default"   , "Displays available default commands" },
}
Help.EXAMPLES       = {
    { "autologin", "Displays help for autologin",},
    { "help", "Displays help for help",},
}

function Help:Run(Client,Args)
    ---------------------
    -- LIST COMMANDS ----
    ---------------------
    local maxChars = Client:getMaxChars()
    local maxRows  = Client:getMaxRows()
    local availableCommands = CommandHandler:getAvailableCommands(Client)
    local commandType;
    local commandList;
    local returnCode;
    -- List all available commands and shrubbot commands
    if not Args[1] or tonumber(Args[1]) then
        commandType = ""
        returnCode = self.PASS
    end
    
    -- Specify cmd type return only those types of commands
    if not Client:isConsole() then
        local cmdtype_temp = Misc:triml( Args[1] )
        if ( cmdtype_temp == 'auth' and Client:isAuth() ) then
            commandType = 'auth'
            returnCode  = self.SUCCESS
        elseif ( cmdtype_temp == 'lua' and Client:isOnline() ) then
            commandType = 'lua'
            returnCode  = self.SUCCESS
        elseif ( cmdtype_temp == 'mail' and Client:isOnline() ) then
            commandType = 'mail'
            returnCode  = self.SUCCESS
        elseif cmdtype_temp == 'admin' then
            commandType = 'admin'
            returnCode  = self.SUCCESS
        elseif cmdtype_temp == 'default' then
            commandType = 'default'
            returnCode  = self.SUCCESS
        end
    end
    
    -- If command type or no args then print command list
    if commandType and returnCode then
        commandList = _FindCommands(availableCommands,commandType)
        if not next(commandList) then return self:Error("No commands found") end
        local PT = Misc:FormatList( commandList , false , maxChars , maxRows , tonumber(Args[2]) or tonumber(Args[1]) )
        for k=1,tlen(PT) do
            self:InfoClean(PT[k])
        end
        return returnCode
    end
    ---------------------
    -- LIST COMMANDS ----
    ---------------------
    
    ----------------------
    -- HELP FOR COMMAND --
    ----------------------
    local COMMAND = CommandHandler:getCommand(Args[1])
    if not COMMAND then
        if not Game:Shrubbot() then return self:Error("Couldn't find command by that name") end
        -- Small bug here, if we have unknown command in-putted in help they don't get the error in shrubbot mode
        self:Info("Can't find lua command")
        return self.PASS
    end
    if not COMMAND:canUseCommand(Client) then
        if not Game:Shrubbot() then
            return self:Error("You don't have access to that command")
        else
            self:Info("You don't have access to that command")
            return self.PASS
        end
    end
    local c1 = Color.Primary
    local c2 = Color.Secondary
    local c3 = Color.Tertiary
    self:Info("Help info for " .. COMMAND:getColor() .. COMMAND.NAME .. Color.Tertiary .. " See console for details")
    
    -- DESCRIPTION --
    if next(COMMAND.DESCRIPTION) ~= nil then
        self:InfoClean("Description " .. c3 .. ": " .. c2 .. COMMAND.DESCRIPTION[1])
        if tlen(COMMAND.DESCRIPTION) > 1 then
            for k=2, tlen(COMMAND.DESCRIPTION) do
                self:InfoClean("            " .. c3 .. ": " .. c2 .. COMMAND.DESCRIPTION[k])
            end
        end
    end
    
    -- ALIASES --
    if next(COMMAND.ALIASES) ~= nil then
        self:InfoClean("Aliases     " .. c3 .. ": " .. c2 .. table.concat(COMMAND.ALIASES, c3 .. " , " .. c2))
    end
    
    -- SYNTAX --
    if next(COMMAND.SYNTAX) ~= nil then
        local SyntaxTable = { }
        for k=1,tlen(COMMAND.SYNTAX) do
            local cc = COMMAND:getColor()
            local synt = COMMAND.SYNTAX[k][1]
            synt = string.gsub(synt,"<",c3 .. "<" .. c2)
            synt = string.gsub(synt,">",c3 .. ">" .. cc)
            synt = string.gsub(synt,"%[",c3 .. "[" .. c2)
            synt = string.gsub(synt,"%]",c3 .. "]" .. cc)
            synt = string.gsub(synt,"|",c3 .. "|" .. c2)
            SyntaxTable[tlen(SyntaxTable)+1] = cc ..  COMMAND:getCommandArg0() .. COMMAND.NAME .. " " .. synt
        end
        local longest = Misc:table_longestString(SyntaxTable)
        local desc    = ""
        if Misc:trim(COMMAND.SYNTAX[1][2]) ~= "" then desc = c3 .. "(^z" .. COMMAND.SYNTAX[1][2] .. c3 .. ")" end
        self:InfoClean("Syntax      " .. c3 .. ": " .. Misc:GetStaticString(SyntaxTable[1],longest) .. desc)
        if ( tlen(COMMAND.SYNTAX) > 1 ) then
            for k=2, tlen(COMMAND.SYNTAX) do
                desc = ""
                if ( Misc:trim(COMMAND.SYNTAX[k][2]) ~= "" ) then
                    desc = c3 .. "(^z" .. COMMAND.SYNTAX[k][2] .. c3 .. ")"
                end
                self:InfoClean("            " .. c3 .. ": " .. Misc:GetStaticString(SyntaxTable[k],longest) .. desc)
            end
        end
    end
    
    -- EXAMPLES --
    if next(COMMAND.EXAMPLES) ~= nil then
        local ExampleTable = { }
        for k=1,tlen(COMMAND.EXAMPLES) do
            ExampleTable[tlen(ExampleTable)+1] = COMMAND:getColor() ..  COMMAND:getCommandArg0() .. COMMAND.NAME .. " " .. COMMAND.EXAMPLES[k][1]
        end
        local longest = Misc:table_longestString(ExampleTable)
        local desc    = ""
        if ( Misc:trim(COMMAND.EXAMPLES[1][2]) ~= "" ) then
            desc = c3 .. "(^z" .. COMMAND.EXAMPLES[1][2] .. c3 .. ")"
        end
        self:InfoClean("Example     " .. c3 .. ": " .. Misc:GetStaticString(ExampleTable[1],longest) .. desc)
        if ( tlen(COMMAND.EXAMPLES) > 1 ) then
            for k=2, tlen(COMMAND.EXAMPLES) do
                desc = ""
                if ( Misc:trim(COMMAND.EXAMPLES[k][2]) ~= "" ) then
                    desc = c3 .. "(^z" .. COMMAND.EXAMPLES[k][2] .. c3 .. ")"
                end
                self:InfoClean("            " .. c3 .. ": " .. Misc:GetStaticString(ExampleTable[k],longest) .. desc)
            end
        end
    end
    
    if COMMAND.OVERWRITE then
        return self.ERROR -- Return 0 because it will have shrubbot help as well.
    else
        return self.SUCCESS
    end
end

return Help

