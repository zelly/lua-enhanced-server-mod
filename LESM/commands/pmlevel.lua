local PMLevelCommand = Command("PMLevel")

PMLevelCommand.CORE           = true
PMLevelCommand.ADMIN          = true
PMLevelCommand.CHAT           = true
PMLevelCommand.LUA            = true
PMLevelCommand.CONSOLE        = true
PMLevelCommand.DESCRIPTION    = { "Private messages a level." , "Can also private message a range of levels." }
PMLevelCommand.ALIASES        = { "pml"}
PMLevelCommand.SYNTAX         = {
    { "<level> <message>", "Private messages all clients at <level>"},
    { "<level>+ <message>", "Private messages all clients at <level> or above"},
    { "<level>- <message>", "Private messages all clients at <level> or below"},
}

function PMLevelCommand:Run(Client,Args)
    if not Args[1] then return self:Error("Need a level to private message") end
    local pmMessage = MessageHandler:filter(table.concat(Args," ",2),Client)
    if pmMessage == "" then return self:Error("Your message is empty") end
    
    local level = tonumber(Args[1])
    if level then
        pmMessage = Color.Primary .. "Private message for level " .. Color.Secondary .. level .. Color.Primary .. " from " .. Client:getName() .. Color.Tertiary .. ": " ..  Color.Primary .. pmMessage
        for k = 1 , tlen(Clients) do 
            local Target = Clients[k]
            if ( Target:getLevel() == level and Target.id ~= Client.id ) and not Target:isIgnored(Client) then
                Target:ChatClean(pmMessage)
                Sound:play(Target,Config.Sound.PrivateMessage,Client)
            end
        end
        Client:ChatClean(pmMessage)
        return self.SUCCESS
    end
    local mLevel,mOperator = Misc:match(Args[1],"(%d+)(.)")
    level = tonumber(mLevel)
    if not level then return self:Error("Could not get level") end
    if mOperator == "+" then
        pmMessage = Color.Primary .. "Private message for level " .. Color.Secondary .. level .. " and above" .. Color.Primary .. " from " .. Client:getName() .. Color.Tertiary .. ": " ..  Color.Primary .. pmMessage
        for k = 1 , tlen(Clients) do 
            local Target = Clients[k]
            if ( Target:getLevel() >= level and Target.id ~= Client.id ) and not Target:isIgnored(Client)  then
                Target:ChatClean(pmMessage)
                Sound:play(Target,Config.Sound.PrivateMessage,Client)
            end
        end
        Client:ChatClean(pmMessage)
        return self.SUCCESS
    elseif mOperator == "-" then
        pmMessage = Color.Primary .. "Private message for level " .. Color.Secondary .. level .. " and below" .. Color.Primary .. " from " .. Client:getName() .. Color.Tertiary .. ": " ..  Color.Primary .. pmMessage
        for k = 1 , tlen(Clients) do 
            local Target = Clients[k]
            if ( Target:getLevel() <= level and Target.id ~= Client.id ) and not Target:isIgnored(Client) then
                Target:ChatClean(pmMessage)
                Sound:play(Target,Config.Sound.PrivateMessage,Client)
            end
        end
        Client:ChatClean(pmMessage)
        return self.SUCCESS
    else
        return self:Error("Wrong operator")
    end
end

return PMLevelCommand