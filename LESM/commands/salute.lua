local SaluteCommand = Command("Salute")
SaluteCommand.ADMIN          = true
SaluteCommand.CHAT           = true
SaluteCommand.LUA            = true
SaluteCommand.AUTH           = true
SaluteCommand.GLOBALOUTPUT   = true
SaluteCommand.DESCRIPTION    = { "Salutes players" , }
SaluteCommand.SYNTAX         = {
    { "","Salutes everyone" },
    { "<clientname|clientid>", "Salutes specifc person" },
    { "@last", "Salutes last connected person" },
}

function SaluteCommand:Run(Client,Args)
    Sound:playAll(Config.Sound.Salute,Client)
    if Args[1] then
        self:InfoAll(Client:getName() .. Color.Primary .. " salutes " .. MessageHandler:filter(table.concat(Args," "),Client,nil,nil,true) .. Color.Tertiary .. "!")
    else
        self:InfoAll(Client:getName() .. Color.Primary .. " salutes" .. Color.Tertiary .. "!")
    end
    return self.SUCCESS
end

return SaluteCommand