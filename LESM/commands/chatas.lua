local ChatAsCommand = Command("ChatAs")

ChatAsCommand.ADMIN          = true
ChatAsCommand.CHAT           = true
ChatAsCommand.LUA            = true
ChatAsCommand.AUTH           = true
ChatAsCommand.CONSOLE        = true
ChatAsCommand.REQUIRE_LOGIN  = true
ChatAsCommand.REQUIRE_AUTH   = true
ChatAsCommand.DESCRIPTION    = { "Chats as another player.",
}
ChatAsCommand.ALIASES        = { "chatas", }
ChatAsCommand.SYNTAX         = {
    { "<clientname|clientid> (message)","Sends global chat (message) from <clientname>", },
    { "<clientname|clientid> -team (message)", "Sends team chat (message) from <clientname>", },
    { "<clientname|clientid> -fireteam (message)", "Sends fireteam chat (message) from <clientname>", },
}

function ChatAsCommand:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not find target")
        return self:getHelp()
    end
    local message = table.concat(Args," ",2)
    local WhereToChat = et.SAY_ALL
    if ( Args[2] == "-team" ) then
        message = table.concat(Args," ",3)
        WhereToChat = et.SAY_TEAM
    elseif ( Args[2] == "-teamnl" ) then
        message = table.concat(Args," ",3)
        WhereToChat = et.SAY_TEAMNL
    elseif ( Args[2] == "-buddy" ) or ( Args[2] == "-fireteam" ) then
        message = table.concat(Args," ",3)
        WhereToChat = et.SAY_BUDDY
    end
    message = MessageHandler:filter(message,Target)
    if message == "" then
        self:Error("Missing chat message")
        return self:getHelp()
    end
    et.G_Say( Target.id, WhereToChat,message)
    return self.SUCCESS
end

return ChatAsCommand