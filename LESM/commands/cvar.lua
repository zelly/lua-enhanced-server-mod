local CvarCommand = Command("Cvar")


CvarCommand.CONSOLE        = true
CvarCommand.ADMIN          = true
CvarCommand.CHAT           = true
CvarCommand.LUA            = true
CvarCommand.AUTH           = true
CvarCommand.REQUIRE_LEVEL  = Config.Level.Leader
CvarCommand.DESCRIPTION    = { "WIP Toggles cvars" }
CvarCommand.SYNTAX         = {
    { "<page>","Go to page number",},
    { "<cvar>","Get cvar info",},
    { "<cvar> <value>","Set cvar value",},
    { "<cvar> <add> <value>","Adds to cvar",},
    { "<cvar> <sub> <value>","Subtracts from cvar",},
}


local _isBlacklisted = function(cvar)
    for k=1,tlen(Cvars.BLACKLIST) do
        if Cvars.BLACKLIST[k].find then
            if string.find(cvar,Misc:triml(Cvars.BLACKLIST[k].name)) then return true end
        elseif Misc:triml(Cvars.BLACKLIST[k].name) == cvar then
            return true
        end
    end
    return false
end

local _isWhitelisted = function(cvar)
    local _FOUND = false
    for k=1,tlen(Cvars.WHITELIST) do
        if Cvars.WHITELIST[k].find then
            --if string.find(Misc:triml(Cvars.WHITELIST[k].name),cvar) then
            if string.find(cvar,Misc:triml(Cvars.WHITELIST[k].name)) then
                _FOUND = true
                break
            end
        elseif Misc:triml(Cvars.WHITELIST[k].name) == cvar then
            _FOUND = true
            break
        end
    end
    return _FOUND
end

local _getCvarInfo = function(cvar)
    for k=1,tlen(Cvars.CVARS) do
        if Misc:triml(Cvars.CVARS[k].name) == cvar then
            return Cvars.CVARS[k]
        end
    end
    return nil
end

function CvarCommand:Run(Client,Args)
    if Game:Mod() ~= "silent" then
        self:Error("WARNING: " .. Color.Primary .. " the default values are only for silent mod")
    end
    local cvar = Misc:trim(Args[1],true)
    if cvar == '' or tonumber(Args[1]) then
        local cvarstable = { }
        for k=1,tlen(Cvars.BLACKLIST) do
            local exact;
            if Cvars.BLACKLIST[k].find then
                exact = "Will search for name in cvar"
            else
                exact = "Will search for exact name"
            end
            local cvardata = {
                Cvars.BLACKLIST[k].name,
                "BlackListed",
                "",
                exact,
            }
            cvarstable[tlen(cvarstable)+1] = cvardata
        end
        for k=1,tlen(Cvars.WHITELIST) do
            local exact;
            if Cvars.WHITELIST[k].find then
                exact = "Will search for name in cvar"
            else
                exact = "Will search for exact name"
            end
            local cvardata = {
                Cvars.WHITELIST[k].name,
                "WhiteListed",
                "",
                exact,
            }
            cvarstable[tlen(cvarstable)+1] = cvardata
        end
        for k=1,tlen(Cvars.CVARS) do
            local skip = false
            if Cvars.USE_BLACKLIST and _isBlacklisted(Cvars.CVARS[k].name) then skip=true end
            if Cvars.USE_WHITELIST and not _isWhitelisted(Cvars.CVARS[k].name) then skip=true end
            if not skip then
                local cvardata = { }
                cvardata[1] = Cvars.CVARS[k].name
                cvardata[2] = Cvars.CVARS[k].type
                cvardata[3] = et.trap_Cvar_Get(Cvars.CVARS[k].name) -- MAYBE NOT WANT TO DO THIS lol
                cvardata[4] = Cvars.CVARS[k].desc
                cvarstable[tlen(cvarstable)+1] = cvardata
            end
        end
        if not next(cvarstable) then return self:Info("No cvars defined in cvar table") end
        local PT = Misc:FormatTable( cvarstable , { "NAME","TYPE","VALUE","DESCRIPTION"} , false , Client:getMaxChars() , Client:getMaxRows() , tonumber(Args[1]) )
        for k=1, tlen(PT) do
            self:InfoClean(PT[k])
        end
        --self:InfoClean( string.len( et.Q_CleanStr(PT[1]) ) )
        return self.SUCCESS
    end
    if Cvars.USE_BLACKLIST and _isBlacklisted(cvar) then return self:Error("This cvar is blacklisted") end
    if Cvars.USE_WHITELIST and not _isWhitelisted(cvar) then return self:Error("This cvar is not on the whitelist") end
    local value = et.trap_Cvar_Get(cvar)
    if not value or value == "" then return self:Error(cvar .. " returned no info") end
    if not Args[2] then
        self:Info(Color.Secondary .. cvar .. Color.Tertiary .. " = " .. Color.Primary .. value)
        local CVARDATA = _getCvarInfo(cvar)
        if not CVARDATA then return self.SUCCESS end
        self:InfoClean( "Type    " .. Color.Tertiary .. ": " .. Color.Secondary .. CVARDATA.type)
        self:InfoClean( "Default " .. Color.Tertiary .. ": " .. Color.Secondary .. CVARDATA.default)
        
        if CVARDATA.type == "bitmask" then
            local total = 0
            for i,v in pairs(CVARDATA.value) do total = total+i end
            local total_bit = bit.tobits( total ) 
            value = bit.tobits( tonumber(value) )
            for k = 1 , tlen(total_bit) do 
                local info = CVARDATA.value[Core.Bits[k]] or ""
                if value[k] and value[k] == 1 then
                    self:InfoClean("^2" .. Misc:GetStaticString(Core.Bits[k],7) .. Color.Tertiary .. " - " .. Color.Primary .. info)
                else
                    self:InfoClean("^7" .. Misc:GetStaticString(Core.Bits[k],7) .. Color.Tertiary .. " - " .. Color.Primary .. info)
                end
            end
        end
        if Client:isConsole() then
            Client:InfoClean(CVARDATA.desc)
        else
            Client:PrintClean(CVARDATA.desc)
        end
        return self.SUCCESS
    elseif Misc:triml(Args[2]) == "add" then
        value = tonumber(value)
        if not value then return self:Error("Cvar isn't a number") end
        local addvalue = tonumber(Args[3])
        if not addvalue then return self:Error("Need value to add") end
        value = value + addvalue
        et.trap_Cvar_Set(cvar,value)
        self:Info(Color.Secondary .. cvar .. Color.Tertiary .. " = " .. Color.Primary .. value)
        return self.SUCCESS
    elseif Misc:triml(Args[2]) == "sub" then
        value = tonumber(value)
        if not value then return self:Error("Cvar isn't a number") end
        local subvalue = tonumber(Args[3])
        if not subvalue then return self:Error("Need value to sub") end
        value = value - subvalue
        et.trap_Cvar_Set(cvar,value)
        self:Info(Color.Secondary .. cvar .. Color.Tertiary .. " = " .. Color.Primary .. value)
        return self.SUCCESS
    else
        value = table.concat(Args," ",2)
        et.trap_Cvar_Set(cvar,value)
        self:Info(Color.Secondary .. cvar .. Color.Tertiary .. " = " .. Color.Primary .. value)
        return self.SUCCESS
    end
end

return CvarCommand