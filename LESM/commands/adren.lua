local Adren = Command("Adren")

Adren.ALIASES        = { "adrenaline", }
Adren.ADMIN          = true
Adren.CHAT           = true
Adren.LUA            = true
Adren.AUTH           = true
Adren.CONSOLE        = true
Adren.REQUIRE_LEVEL  = Config.Level.Leader
Adren.REQUIRE_AUTH   = true
Adren.DESCRIPTION    = { "Gives adrenaline to a client.", }
Adren.SYNTAX         = {
    { ""                      , "Give yourself adrenaline for 10 seconds" },
    { "<seconds>"             , "Give yourself adrenaline for <seconds> seconds" },
    { "<clientname|clientid>" , "Give target adrenaline for 10 seconds" },
    { "<clientname|clientid> <seconds>", "Give target adrenaline for <seconds> seconds" },
}
Adren.EXAMPLES       = {
    { "60", "Gives yourself 60 seconds of adrenaline" },
    { "Bob", "Gives Bob 10 seconds of adrenaline" },
    { "Bob 60", "Gives Bob 60 seconds of adrenaline" },
}

function Adren:Run(Client,Args)
    if ( Args[1] == nil ) then
        if ( Client:isConsole() ) then
            self:Error("Could not find target")
            return CommandHandler:getHelp(Console,self.NAME)
        end
        Client:setAdren(10*1000)
        self:Info("Gave yourself " .. Color.Secondary .. "10" .. Color.Primary .. " seconds of adrenaline")
        return self.SUCCESS
    end
    if ( Args[2] == nil ) then
        local adrenTime = tonumber(Args[1])
        if adrenTime then
            if ( Client:isConsole() ) then
                self:Error("Could not find target")
                return CommandHandler:getHelp(Console,self.NAME)
            end
            Client:setAdren(adrenTime*1000)
            self:Info("Gave yourself " .. Color.Secondary .. tostring(adrenTime) .. Color.Primary .. " seconds of adrenaline")
            return self.SUCCESS
        else
            local Target = ClientHandler:getClient(Args[1])
            if Target then
                Target:setAdren(10*1000)
                self:Info("Gave " .. Target:getName() .. Color.Secondary .. " 10 " .. Color.Primary .. "seconds of adrenaline")
                return self.SUCCESS
            end
        end
        self:Error("Could not find target")
        return CommandHandler:getHelp(Console,self.NAME)
    end
    local Target = ClientHandler:getClient(Args[1])
    local adren_time = tonumber(Args[2])
    if not Target or not adren_time then
        Client:Error("Could not find target")
        return CommandHandler:getHelp(Client,self.NAME)
    end
    Target:setAdren(adren_time*1000)
    self:Info("Gave " .. Target:getName() .. Color.Secondary .. " " .. tostring(adren_time) .." " .. Color.Primary .. "seconds of adrenaline")
    return self.SUCCESS
end
return Adren