local SwearFilterCommand = Command("SwearFilter")

SwearFilterCommand.ADMIN          = true
SwearFilterCommand.CHAT           = true
SwearFilterCommand.LUA            = true
SwearFilterCommand.AUTH           = true
SwearFilterCommand.CONSOLE        = true
SwearFilterCommand.REQUIRE_LEVEL  = Config.Level.Leader
SwearFilterCommand.DESCRIPTION    = { "Toggle swearfilter for client" , }
SwearFilterCommand.ALIASES        = { "sf","swear","censor"}
SwearFilterCommand.SYNTAX         = {
    { "<client>", "Enables Swearfilter for target",},
}

function SwearFilterCommand:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Could not find target") end
    if ( Target.id == Client.id ) and not Client:isAuth() then return self:Error("Cannot use this on yourself") end
    if ( Target:getLevel() > Client:getLevel() ) and not Client:isAuth() then return self:Error(Target:getName() .. Color.Primary .. " has higher level than you!") end
    if not Target.rudclient then return self:Error("Target does not have rudclient") end
    if Target.rudclient.swearfilter then
        Target.rudclient.swearfilter = false
        self:Info("Disabled swearfilter for " .. Target:getName())
        return self.SUCCESS
    else
        Target.rudclient.swearfilter = true
        self:Info("Enabled swearfilter for " .. Target:getName())
        return self.SUCCESS
    end
end

return SwearFilterCommand