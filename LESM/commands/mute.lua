local Mute = Command("Mute")

Mute.ADMIN          = true
Mute.CHAT           = true
Mute.CONSOLE        = true
if not Game:Shrubbot() then
    Mute.CORE           = true
    Mute.LUA            = true
    Mute.AUTH           = true
    Mute.REQUIRE_LEVEL  = Config.Level.Admin
    Mute.REQUIRE_LOGIN  = true
    Mute.OVERWRITE      = false
    Mute.ALIASES        = { "unmute" }
else
    Mute.OVERWRITE      = true
    Mute.CORE           = false
    Mute.LUA            = false
    Mute.AUTH           = false
    Mute.REQUIRE_LEVEL  = 0
    Mute.REQUIRE_LOGIN  = false
    Mute.REQUIRE_FLAG   = 'm'
end
Mute.DESCRIPTION    = { "Mute and unmutes a specified user" , }
Mute.SYNTAX         = {
    { "<client>", "Mute <client> permanently"},
    { "<client> <muteTime>", "Mute <client> for <muteTime> seconds"},
    { "<client> strict", "Mute <client>'s commands permanently"},
}
-- TODO Offline mutes
local _getPrintTableOfRUDMutes = function()
    local Data = { }
    for k = 1 , tlen(Clients) do 
        local Target = Clients[k]
        if Target.rudclient and Target:isMuted() then
            local clientMute = { }
            clientMute[1] = Target:getName()
            local muteTime = Target.rudclient.mutetime
            if Target:isStrictMuted() then
                muteTime = "Strict Muted"
            elseif muteTime <= -1 then
                muteTime = "Permanent"
            else
                muteTime = Misc:SecondsToClock( muteTime - os.time() )
            end
            clientMute[2] = muteTime
            clientMute[3] = Target.rudclient.mutereason
            Data[tlen(Data)+1] = clientMute
        end
    end
    return Data
end

local _getPrintTableOfMutes = function()
    local Data = { }
    for k = 1 , tlen(Clients) do 
        local Target = Clients[k]
        if Target:isMuted() then
            local clientMute = { }
            local muteTime = self:entGet("sess.muted")
            local reason = ""
            if Target.rudclient then
                reason = Target.rudclient.mutereason
            end
            clientMute[1] = Target:getName()
            if Target:isStrictMuted() then
                muteTime = "Strict Muted"
            else
                muteTime = tostring(muteTime) -- not 100% sure how sess.muted works on here
                -- Muted:      Silent(Above 0,seconds from year 2000 + time muted I think...
            end
            clientMute[2] = muteTime
            clientMute[3] = reason -- not able to get mute reason in shrubbot, so we see if its saved in rudclient
            Data[tlen(Data)+1] = clientMute
        end
    end
    return Data
end

function Mute:Run(Client,Args)
    if not Args[1] or Misc:triml(Args[1]) == 'list' then --- START NO ARGS
        local Data;
        if Config.Shrubbot.Mute then
            Data = _getPrintTableOfMutes()
        else
            Data = _getPrintTableOfRUDMutes()
        end
        
        if not next(Data) then return self:Info("No clients are currently muted") end
        
        self:Info(Color.Secondary .. tlen(Data) .. Color.Primary .. " Clients are currently muted")
        local PT = Misc:FormatTable(Data,{ "Name", "Mute Time", "Mute Reason"},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[2]) or tonumber(Args[1]))
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    end --- END NO ARGS
    
    local Target    = ClientHandler:getClient(Args[1]) -- ARG1 IS TARGET CLIENT
    local muteTime  = tonumber(Args[2]) or -1
    local message   = Misc:trim(table.concat(Args, " " , 3))
    
    if not Target or not Target.rudclient then
        if self.OVERWRITE and Config.Shrubbot.Mute then return self.PASS end -- Let shrubbot handle
        self:Error("Could not find target")
        return self.INVALID
    end
    
    if Misc:trim(Args[2], true) == "strict" then -- This will work in shrubbot no matter what
        if Target:strictMuted() then
            RegularUser:unmute(Target.rudclient)
            self:Info("Strict unmuted " .. Target:getName())
        else
            RegularUser:strictMute(Target.rudclient,message)
            self:Info("Strict mute " .. Target:getName())
        end
        return self.SUCCESS
    end
    
    if self.OVERWRITE and Config.Shrubbot.Mute then -- Let the mod handle the rest
        if muteTime > 0 then Target.rudclient.mutereason = message end -- An attempt to get mute message for display
        return self.PASS
    end
    
    if Target:isMuted() and not Args[2] then
        RegularUser:unmute(Target.rudclient)
        self:Info("Unmuted " .. Target:getName())
        return self.SUCCESS
    else
        RegularUser:unmute(Target.rudclient,muteTime,message)
        self:Info("Muted " .. Target:getName())
        return self.SUCCESS
    end
end

return Mute