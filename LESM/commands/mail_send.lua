local MailSend = Command("Send")

MailSend.CORE           = true
MailSend.MAIL           = true
MailSend.CONSOLE        = true
MailSend.REQUIRE_LOGIN  = true
MailSend.DESCRIPTION    = { "Composes a mail to another user." }
MailSend.ALIASES        = { "email", }
MailSend.SYNTAX         = {
    { "<username> <message...>", "Sends a message to username." },
    { "all <message...>", "Sends a message to everyone.(Auth only)" },
}


function MailSend:Run(Client,Args)
    if not Args[1] then
        self:Error("Missing target and message")
        return CommandHandler:getHelp(Client,self.NAME)
    end
    
    if not Args[2] then
        self:Error("Missing message")
        return CommandHandler:getHelp(Client,self.NAME)
    end
    local user_id = -1
    if not Client:isConsole() then user_id = Client.User:getKey('id') end
    if ( Args[1] == "all" ) then
        if not Client:isConsole() and not Client:isAuth() then
            self:Error("You are not authorized")
            return self.SUCCESS
        end
        Mail:Compose(table.concat(Args," ",2),-1,user_id)
        self:Info("Sent to message")
        return self.SUCCESS
    end
    
    local TargetUser = UserHandler:getUser(Args[1])
    if not TargetUser then
        self:Error("Could not find username")
        return CommandHandler:getHelp(Client,self.NAME)
    end
    
    Mail:Compose(table.concat(Args," ",2),TargetUser:getKey('id'),user_id)
    self:Info("Sent mail to " .. TargetUser:getKey('username'))
end

return MailSend