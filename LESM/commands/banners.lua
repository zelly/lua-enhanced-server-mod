local BannersCommand = Command("Banners")

BannersCommand.CORE           = true
BannersCommand.ADMIN          = true
BannersCommand.CHAT           = true
BannersCommand.LUA            = true
BannersCommand.GLOBALOUTPUT   = true
BannersCommand.DESCRIPTION    = { "Displays server banners" , "Can also print individual banner to everyone" }
BannersCommand.ALIASES        = { "banner" }
BannersCommand.SYNTAX         = {
    {"","Displays all benners",},
    { "<bannernumber>" , "Displays specific banner number to everyone",},
    { "add <message>" , "Adds a banner" },
    { "edit <bannernumber> <message>" , "Edit banner message" },
    { "delete <bannernumber>" , "Removes banner" },
    { "refresh" , "Refreshes banner routine" },
}

function BannersCommand:Run(Client,Args)
    if Client:isConsole() or Client:isAuth() then
        if Misc:triml(Args[1]) == "add" then
            local bannerMessage = Misc:trim(table.concat(Args," ", 2))
            if bannerMessage == '' then return self:Error("Need message to add") end
            Banners[tlen(Banners)+1] = bannerMessage
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Banners, fileData = Banners }
            self:Info("Added banner!")
            return self.SUCCESS
        elseif Misc:trim(Args[1]) == "edit" then
            local bannern  = tonumber(Args[2])
            if not bannern then return self:Error("Need banner number to edit") end
            if not Banners[bannern] then return self:Error("No banner with that number") end
            local bannerMessage = Misc:trim(table.concat(Args," ",3))
            if bannerMessage == '' then return self:Error("Need message to edit") end
            Banners[bannern] = bannerMessage
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Banners, fileData = Banners }
            self:Info("Edited banner")
            return self.SUCCESS
        elseif Misc:trim(Args[1]) == "delete" then
            local bannern  = tonumber(Args[2])
            if not bannern then return self:Error("Need banner number to delete") end
            if not Banners[bannern] then return self:Error("No banner with that number") end
            table.remove(Banners,bannern)
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Banners, fileData = Banners }
            self:Info("Removed banner!")
            return self.SUCCESS
        elseif Misc:trim(Args[1]) == "refresh" then
            RoutineHandler:refreshBanners()
            self:Info("Refreshed banners")
            return self.SUCCESS
        end
    end
    if tonumber(Args[1]) then
        local bannern = tonumber(Args[1])
        if ( Banners[bannern] == nil or Misc:trim(Banners[bannern]) == "" ) then return self:Error("No banner by that number") end
        Console:IteratePrint(MessageHandler:filter(Banners[bannern]),Config.Banner.Location,"bannerlocation")
        return self.SUCCESS
    else
        self:Info("Displaying " .. Color.Secondary .. tlen(Banners) .. Color.Primary .. " banners")
        for k = 1 , tlen(Banners) do 
            self:InfoClean(Color.Tertiary .. "#" .. Color.Secondary .. k .. Color.Primary .." " .. MessageHandler:filter(Banners[k]))
        end
        return self.SUCCESS
    end
end

return BannersCommand