local UpdateCommand = Command("Update")

UpdateCommand.ADMIN          = true
UpdateCommand.CHAT           = true
UpdateCommand.LUA            = true
UpdateCommand.AUTH           = true
UpdateCommand.CONSOLE        = true
UpdateCommand.REQUIRE_LOGIN  = true
UpdateCommand.REQUIRE_AUTH   = true
UpdateCommand.DESCRIPTION    = { "Updates user's key",
                                 "There is no error checking in this",
                               }
UpdateCommand.SYNTAX         = {
    { "<username> <key> <value>", "Updates <username> <key> to <value>", },
    { "all <key> <value>", "Updates everyone's <key> to <value>", },
}
UpdateCommand.EXAMPLES       = {
    { "Steve42 username steve_42", "Updates Steve42's username to steve_42", },
}

function UpdateCommand:Run(Client,Args)
    if not Args[2] then
        self:Error("Missing key")
        return self:getHelp()
    end
    if not Args[3] then
        self:Error("Missing value")
        return self:getHelp()
    end
    local key = string.lower(Args[2])
    local value = Misc:trim(table.concat(Args," ",3))
    if ( key == "password" ) then value = Core:encrypt(value) end
    if Args[1] == "all" then
        for k = 1 , tlen(Users) do 
            Users[k]:update(key,value)
        end
        self:Info("Set " .. Color.Secondary .. tlen(Users) .. Color.Primary .. " " .. key .. Color.Tertiary .. "'s " .. Color.Primary .. "to " .. Color.Secondary .. value)
        return self.SUCCESS
    end
    local Target = UserHandler:getUser(Args[1])
    if not Target then
        self:Error("Missing username")
        return self:getHelp()
    end
    Target:update(key,value)
    self:Info("Set " .. Color.Secondary .. Target.username .. Color.Primary .. "'s " .. key  .. Color.Primary .. " to " .. Color.Secondary .. value)
    return self.SUCCESS
end

return UpdateCommand