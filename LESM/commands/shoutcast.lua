local ShoutCastCommand = Command("ShoutCast")
ShoutCastCommand.ADMIN          = true
ShoutCastCommand.CHAT           = true
ShoutCastCommand.LUA            = true
ShoutCastCommand.AUTH           = true
ShoutCastCommand.REQUIRE_LEVEL  = Config.Level.Leader
ShoutCastCommand.DESCRIPTION    = { "Sets yourself shoutcaster", }
ShoutCastCommand.ALIASES        = { "ms","sc","makeshoutcaster","makeshoutcast","shoutcaster",}

ShoutCastCommand.REQUIRE_MOD = { "broken" }
-- This command has a high chance of crashing server. Gotta figure out how to prevent it before enabling

function ShoutCastCommand:Run(Client,Args)
    local shoutcaster = tonumber(EntityHandler:safeEntGet(Client.id,"sess.shoutcaster"))
    if not shoutcaster then
        self:Error("Not supported in this mod")
        return self.SUCCESS
    end
    if Client:getTeam() ~= 3 then
        Client:setTeam(3)
        self:Info("Moved to spectator")
    end
    if shoutcaster == 1 then
        Client:entSet("sess.shoutcaster",0)
        self:Info("Shoutcaster disabled")
    else
        Client:entSet("sess.shoutcaster",1)
        self:Info("Shoutcaster enabled")
    end
    return self.SUCCESS
end

return ShoutCastCommand