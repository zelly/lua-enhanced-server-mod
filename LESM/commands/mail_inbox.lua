local MailInbox = Command("Inbox")

MailInbox.ALIASES        = { "read", }
MailInbox.CORE           = true
MailInbox.MAIL           = true
MailInbox.REQUIRE_LOGIN  = true
MailInbox.DESCRIPTION    = { "Views your inbox." , "Also you can read and delete your messages", "You may also empty your inbox" }
MailInbox.SYNTAX         = {
    { "","Views your current inbox, with a small summary of each message" },
    { "page <pageid>","Views your current inbox, with a small summary of each message" },
    { "<mail_id>", "Displays entire <mail_id> message" },
    { "delete <mail_id>", "Deletes <mail_id> from your inbox" },
    { "empty", "Emptys all messages from your inbox" },
}
MailInbox.EXAMPLES = {
    { "3" , "Will view and mark as read mail id 3",},
    { "delete 4" , "Will delete mail id 4 and you will no longer see it in your inbox",},
}

function MailInbox:Run(Client,Args)
    if Misc:triml(Args[1]) == "empty" then
        if Client.confirm.mailEmpty then
            Client.confirm.mailEmpty = false
            Mail:deleteAll(Client.User:getKey('id'))
            self:Info("Deleted all messages!")
            return self.SUCCESS
        else
            Client.confirm.mailEmpty = true
            self:Info("Are you sure you want to delete all of your messages?")
            self:Info("Repeat command if you agree")
            return self.SUCCESS
        end
    end
    
    local mail_id = tonumber(Args[1])
    local delete  = false
    if Misc:triml(Args[2]) == "delete" then delete = true end
    if Misc:triml(Args[1]) == "delete" then
        mail_id = tonumber(Args[2])
        delete  = true
    end
    
    if mail_id and Misc:triml(Args[1]) ~= 'page' then
        if not Mail:validId(Client.User:getKey('id') , mail_id) then return self:Error("Invalid mail id") end
        -- Pretend we don't know what they are talking about
        if Mail:hasDeleted(Client.User:getKey('id') , mail_id) then return self:Error("Invalid mail id") end
        if delete then
            if Client.confirm.mailDelete then
                Client.confirm.mailDelete = false
                Mail:deleteMail(Client.User:getKey('id'),mail_id)
                self:Info("Deleted mail " .. Color.Tertiary .."#" .. Color.Secondary .. mail_id)
                return self.SUCCESS
            else
                Client.confirm.mailDelete = true
                self:Info("Are you sure you want to delete this mail? " .. Color.Secondary .. "Repeat command if you agree.")
                return self.SUCCESS
            end
        else
            local mail = Mail:getMail(mail_id)
            if not mail then return self:Error("No mail found") end
            local name = "The Server"
            if mail.from ~= -1 then
                local User = UserHandler:getWithId(mail.from)
                if User then name = User:getKey('username') end
            end
            Mail:readMail(Client.User:getKey('id'),mail_id)
            self:InfoClean("Sent by " .. Color.Secondary .. name .. Color.Primary .. " on " .. Color.Secondary .. os.date("%c",mail.date) )
            Client:Print(mail.message,Client:getMaxChars())
            return self.SUCCESS
        end
    else
        local user_id = Client.User:getKey('id')
        local inbox   = Mail:getInbox(user_id)
        if not user_id then return self:Error("Could not get your user id") end
        if not inbox then return self:Error("Error getting your inbox") end
        if not next(inbox) then return self:Info("No messages in your inbox") end
        local Data = { }
        for k = 1 , tlen(inbox) do 
            local mail = inbox[k]
            if not Mail:hasDeleted() then
                local MAIL_DATA = { "","","","" }
                if Mail:hasRead(user_id,mail.id) then MAIL_DATA = { "^9","^9","^9","^9" } end
                MAIL_DATA[1] = MAIL_DATA[1]     .. mail.id
                if string.len(et.Q_CleanStr(mail.message)) > 32 then
                    MAIL_DATA[2] = MAIL_DATA[2] .. string.sub(et.Q_CleanStr(mail.message),1,32)
                else
                    MAIL_DATA[2] = MAIL_DATA[2] .. et.Q_CleanStr(mail.message)
                end
                if ( mail.from == -1 ) then
                    MAIL_DATA[3] = MAIL_DATA[3] .. "The Server"
                else
                    local User = UserHandler:getWithId(mail.from)
                    if not User then
                        MAIL_DATA[3] = MAIL_DATA[3] .. "Unknown"
                    else
                        MAIL_DATA[3] = MAIL_DATA[3] .. User:getKey('username')
                    end
                end
                MAIL_DATA[4] = MAIL_DATA[4]     .. os.date("%c",mail.date)
                Data[tlen(Data)+1] = MAIL_DATA
            end
        end
        if not next(Data) then return self:Info("No messages in your inbox") end
        self:Info(Color.Secondary .. tlen(Data) .. Color.Primary .. " Messages found")
        local PrintTable = Misc:FormatTable(Data,{"id","Subject","Username","Message",},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[2]),tonumber(Args[1]))
        for k = 1 , tlen(PrintTable) do self:InfoClean(PrintTable[k]) end
        return self.SUCCESS
    end
end


return MailInbox