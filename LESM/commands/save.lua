local SaveCommand = Command("Save")

SaveCommand.CORE           = true
SaveCommand.ADMIN          = true
SaveCommand.CHAT           = true
SaveCommand.LUA            = true
SaveCommand.AUTH           = true
SaveCommand.REQUIRE_LOGIN  = true
SaveCommand.REQUIRE_AUTH   = true
SaveCommand.DESCRIPTION    = { "Saves your current location" , "See also load command" }
SaveCommand.SYNTAX         = {
    { "" , "Saves current location to temporary cache"},
    { "<name>","Saves current location <name> for current map"},
}


function SaveCommand:Run(Client,Args)
    if not Args[1] then
        Client.savePoint = et.gentity_get(Client.id,"ps.origin")
        self:Info("Saved Location")
        return self.SUCCESS
    end

    local save = { }
    save.origin = et.gentity_get(Client.id,"ps.origin")
    save.name   = string.lower(Misc:trim(Args[1]))
    save.map    = string.lower(Misc:trim(Game:Map()))
    Client.User.savepoint[tlen(Client.User.savepoint)+1] = save
    self:Info("Saved Location to " .. Color.Secondary .. save.name)
end

return SaveCommand