local AveragePingCommand = Command("AveragePingCommand")

local _GetAveragePing = function(self,Target)
    local message = Target:getName() .. Color.Primary .. " average, Map: " .. Color.Secondary .. math.floor(Misc:table_average(Target.pings))
    if ( Target:isOnline() ) then
        message = message .. Color.Primary .. " User: " .. Color.Secondary .. math.floor(Misc:table_average(Target.User.pings))
    end
    self:Info(message)
end

AveragePingCommand.NAME           = "avgping"
AveragePingCommand.ADMIN          = true
AveragePingCommand.CHAT           = true
AveragePingCommand.LUA            = true
AveragePingCommand.AUTH           = true
AveragePingCommand.CONSOLE        = true
AveragePingCommand.DESCRIPTION    = { "Tells you your average ping over the course of a game.",
                                      "(Must be actively on a team)",
                                      "This will also save your ping over games, if you are logged in.",
                                    }
AveragePingCommand.ALIASES        = { "getping","averageping",}
AveragePingCommand.SYNTAX         = {
    { "","Gets your average ping", },
    { "<clientname|clientid>","Gets <clientname>'s average ping", },
}

function AveragePingCommand:Run(Client,Args)
    if not Args[1] then
        _GetAveragePing(self,Client)
        return self.SUCCESS
    end
    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not find target")
        return self:getHelp()
    end
    _GetAveragePing(self,Target)
    return self.SUCCESS
end


return AveragePingCommand