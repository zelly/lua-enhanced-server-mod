local RunAsCommand = Command("RunAs")

RunAsCommand.ADMIN          = true
RunAsCommand.CHAT           = true
RunAsCommand.LUA            = true
RunAsCommand.AUTH           = true
RunAsCommand.CONSOLE        = true
RunAsCommand.REQUIRE_LOGIN  = true
RunAsCommand.REQUIRE_AUTH   = true
RunAsCommand.DESCRIPTION    = { "Runs a command as another player",}
RunAsCommand.SYNTAX         = {
    { "<client> <command> <args>","Runs a command as another client", },
}

function RunAsCommand:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    local command = Misc:triml(Args[2])
    if not Target then return self:Error("Could not find target") end
    if command == "" then return self:Error("Missing command") end
    table.remove(Args,1)
    table.remove(Args,1)
    CommandHandler:Command(Target,"lua",command,Args)
    self:Info("Running " .. Color.Secondary .. command .. Color.Primary .. " as " .. Target:getName() )
    return self.SUCCESS
end

return RunAsCommand