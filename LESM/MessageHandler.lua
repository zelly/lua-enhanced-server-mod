MessageHandler      = { }
MessageHandler.MOTD = ""

local _shortenGuid = function( guid )
    if not guid or guid == '' or string.len( guid ) < 32 then return '' end
    return string.sub( guid , -8 , -1 )
end

local _getNameEvent = function(Client,fn)
	local Target = ClientHandler:getWithId(Client:entGet(fn))
	if not Target then return "" end
	return Target:getName()
end
--- Replace a message with data
-- Needs to be more modular
-- Look at event mention pattern matching
function MessageHandler:replace( text , replaceTable , Client )
	text = Misc:trim( text )
    if text == '' then return text end
    if replaceTable == true then
        replaceTable = {
            map=true,mod=true,teams=true,leveltime=true,colors=true,servercvar=true,
            clientnum=true,clientname=true,clientcountry=true,clientcvar=true,clientguid=true,clientip=true,
			clientclass=true,clientteam=true,clientlevel=true,clientetversion=true,clientprotocol=true,clientping=true,
            userkey=true,cliententity=true,
        }
    end
	if replaceTable.client then
		replaceTable = {
			clientnum=true,clientname=true,clientcountry=true,clientcvar=true,clientguid=true,clientip=true,
			clientclass=true,clientteam=true,clientlevel=true,clientetversion=true,clientprotocol=true,clientping=true,
            userkey=true,cliententity=true,
		}
	end
	
    if not replaceTable or type(replaceTable) ~= "table" then return text end
	
    local r = function( replaceText , replaceWith )
        if not replaceWith then replaceWith = "" end
        if type(replaceText) == "table" then
            for k=1,tlen(replaceText) do
                text = string.gsub( text , replaceText[k] , replaceWith )
            end
        else
            text = string.gsub( text , replaceText , replaceWith )
        end
    end
    -- map mod leveltime teams colors servecvar
    -- clientnum clientname clientcountry clientcvar clientguid clientip clientclass clientteam clientlevel clientetversion clientprotocol clientping
    -- userkey
    if replaceTable.map then r( "<mapname>" , Game:Map() ) end
    if replaceTable.mod then r( "<mod>" , Game:Mod() ) end
    if replaceTable.leveltime then r( "<leveltime>" , Game:getLevelTime() ) end
    if replaceTable.teams then
		Game:getTeams()
        r( "<players>"        , Game:getTotalPlayers() )
        r( "<axisplayers>"    , Game:getAxisPlayers() )
        r( "<alliesplayers>"  , Game:getAlliedPlayers() )
        r( "<specplayers>"    , Game:getTotalPlayers() - ( Game:getAxisPlayers() + Game:getAlliedPlayers() ) )
        r( "<playingplayers>" , Game:getTotalPlayingPlayers() )
        
        r( "<bots>"        , Game:getTotalBots() )
        r( "<axisbots>"    , Game:getAxisBots() )
        r( "<alliesbots>"  , Game:getAlliedBots() )
        r( "<specbots>"    , Game:getTotalBots() - ( Game:getAxisBots() + Game:getAlliedBots() ) )
        r( "<playingbots>" , Game:getTotalPlayingBots() )
        
        r( "<clients>"        , Game:getTotalClients( true ) )
        r( "<axisclients>"    , Game:getAxisClients( true ) )
        r( "<alliesclients>"  , Game:getAlliedClients( true ) )
        r( "<specclients>"    , Game:getTotalClients( true ) - ( Game:getAlliedClients( true ) + Game:getAxisClients( true ) ) )
        r( "<playingclients>" , Game:getTotalPlayingClients( true ) )
    end
    if replaceTable.colors then
        r( { "<color1>" , "<c1>" } , Color.Primary )
        r( { "<color2>" , "<c2>" } , Color.Secondary )
        r( { "<color3>" , "<c3>" } , Color.Tertiary )
    end
    if replaceTable.servercvar then
        while string.find( text , "<cvar%(([%w_]+)%)>" ) do
            local cvarname = Misc:match( text , "<cvar%(([%w_]+)%)>" )
            if cvarname then
                local value = et.trap_Cvar_Get( cvarname )
                if not value then value = "" end
                r( "<cvar%(" .. cvarname .. "%)>" , value )
            end
        end
    end
    if Client then
		if replaceTable.clientevents and Config.Message.EventMentions then
			r( {"<clientlastkiller>", "<d>"} , _getNameEvent(Client, "pers.lastkiller_client") )
			r( {"<clientlastvictim>", "<k>"} , _getNameEvent(Client, "pers.lastkilled_client") )
			r( {"<clientlasthealth>", "<h>"} , _getNameEvent(Client, "pers.lasthealth_client") )
			r( {"<clientlastammo>", "<a>"} , _getNameEvent(Client, "pers.lastammo_client") )
			r( {"<clientlastrevive>", "<r>"} , _getNameEvent(Client, "pers.lastrevive_client") )
		end
        if replaceTable.clientname then
            r( "<clientname>" , Client:getName() )
            r( "<clientcleanname>" , Client:getCleanName() )
        end
        if replaceTable.clientcountry then
            local country = Client:getCountry()
            if not country then country = "" end
            r( "<clientcountry>" , country )
        end
        if replaceTable.clientguid  then
            local eGuid,sGuid,etproGuid = Client:getGuids()
            r( "<etkeyguid>" , eGuid )
            r( "<shortetkeyguid>" , _shortenGuid(eGuid) )
            r( "<silentguid>" , sGuid )
            r( "<shortsilentguid>" , _shortenGuid(sGuid) )
            r( "<etproguid>" , etproGuid)
            r( "<shortetproguid>" , _shortenGuid(etproGuid) )
        end
        if replaceTable.clientip then
            local ip = Client:getIp()
            local ip1,ip2,ip3,ip4 = Misc:match( ip ,  "(%d+)%.(%d+)%.(%d+)%.(%d+)" )
            r( "<clientip>" , ip )
            r( "<clientip1>", ip1 )
            r( "<clientip2>", ip2 )
            r( "<clientip3>", ip3 )
            r( "<clientip4>", ip4 )
        end
        if replaceTable.clientclass then
            local class = Client:getClass()
            local classname = et.CLASS[class]
            if not classname then classname = "" end
            r( "<clientclass>" , classname )
            r( "<clientclassnum>"   , tostring(class) )
        end
        if replaceTable.clientteam then
            local team     = Client:getTeam()
            local teamname = et.TEAM[team]
            if not teamname then teamname = "" end
            r( "<clientteam>" , teamname )
            r( "<clientteamnum>"   , tostring(team) )
        end
        if replaceTable.clientnum then r("<clientnum>",Client.id ) end
        if replaceTable.clientetversion then r("<clientetversion>" , Client:getVersion() ) end
        if replaceTable.clientlevel then r( "<clientlevel>" , Client:getLevel() ) end
        if replaceTable.clientprotocol then r( "<clientprotocol>" , Client:getProtocol() ) end
        if replaceTable.clientping then r( "<clientping>" , Client:getPing() ) end
        if replaceTable.cliententity then
            while string.find( text , "<cliententity%(([%w_%.]+)%)>" ) do
                local fieldname = Misc:match( text , "<cliententity%(([%w_%.]+)%)>" )
                if fieldname then
                    local value = Client:entGet( fieldname )
                    if not value then value = "" end
                    r( "<cliententity%(" .. fieldname .. "%)>" , value )
                end
            end
        end
        if Client:isOnline() then
            if replaceTable.userkey then
                while string.find( text , "<clientuserkey%(([%w_]+)%)>" ) do
                    local keyname = Misc:match(  text , "<clientuserkey%(([%w_]+)%)>" )
                    if keyname then
                        local value;
                        if Client.User[keyname] ~= nil then value = Client.User:getKey( keyname ) end
                        if not value then value = "" end
                        r( "<clientuserkey%(" .. keyname .. "%)>" , value )
                    end
                end
            end
        end
    end
    --[[
    TODO
    if replaceTable.clientcvar and Client then
        local cvarname = Misc:match( "<clientcvar%(([%w_]+)%)>" )
        if cvarname then
            local value = et.trap_Cvar_Get( cvarname )
            if value then
                r( "<cvar%(" .. cvarname .. "%)>" , value )
            end
        end
    end]]--
    return text
end

--- Get the Message of the Day for specific client
function MessageHandler:motd( Client )
    if Misc:trim(self.MOTD) == '' then return nil end
    return self:replace( self.MOTD , true , Client )
end

--- Get client greeting
function MessageHandler:greeting( message , Client )
    if Misc:trim(Config.Profile.GreetingFormat) == '' then return message end
    return string.gsub( self:replace( Config.Profile.GreetingFormat , true , Client ) , "<message>" , message )
end

--- Get client farewell
function MessageHandler:farewell( message , Client )
    if Misc:trim(Config.Profile.FarewellFormat) == '' then return message end
    return string.gsub( self:replace( Config.Profile.FarewellFormat , true , Client ) , "<message>" , message )
end

--- Get distance message
-- Not sure if I like how this function works will see
function MessageHandler:distance( units , distance )
    units    = tonumber(units)
    distance = tonumber(Misc:int(distance))
    
	if not units or not distance then return "" end -- error
	
    if units == et.MEASUREMENT_UNITS then
        return self:replace( string.gsub( Config.Distance.Unit , "<units>" , distance ) , true )
    elseif units == et.MEASUREMENT_IMPERIAL then
        local feet,inches = EntityHandler:DistanceToFeet(distance)
        if feet == 0 then
            return self:replace( string.gsub( Config.Distance.ImperialInches , "<inches>" , inches ) , true )
        elseif inches == 0 then
            return self:replace( string.gsub( Config.Distance.ImperialFeet , "<feet>" , feet ) , true )
        else
            local message = string.gsub( Config.Distance.ImperialFeet , "<feet>" , feet ) .. " " .. string.gsub( Config.Distance.ImperialInches , "<inches>" , inches )
            return self:replace( message , true )
        end
    elseif units == et.MEASUREMENT_METRIC then
        return MessageHandler:replace( string.gsub( Config.Distance.Metric , "<metres>" , EntityHandler:DistanceToMeters(distance) ) , true )
    else
        return ""
    end
end

--- Filter chat and vsay messages
-- Needs a rewrite
function MessageHandler:filter(message,Client,beginChar,endChar,includeLevel,vsay)
	message = Misc:trim(message)
    if message == "" then return "" end
    if not beginChar then beginChar = Color.Secondary end
    if not endChar   then endChar   = Color.Primary end
	
	message = self:replace( message , { colors=Config.Message.ColorReplace } )
	
    if Client then
        if not ( vsay and Client:isOnline() and Client.User:getKey('vsaybypass') ) then
            if Client:isNoColored() then      message = self:nocolors(message) end
            if Client:isGrammarChecked() then message = self:grammar(message) end
            if Client:isSwearFiltered() then  message = self:censor(message) end
            if Client:isNoCapped() then       message = self:nocaps(message) end
        end
    end
	
    if not vsay then
        if Config.Message.NameMentions then
			message = self:nameMentions(message,beginChar,endChar,includeLevel)
		end
		message = self:replace( message , { clientevents=Config.Message.EventMentions}, Client )
    end
	
    return message
end

--- Get names on mention (using @ symbol)
function MessageHandler:nameMentions(message,beginChar,endChar,includeLevel)
	message = Misc:trim( message )
	if message == "" then return "" end
	
	-- Workaround because nocaps adds sentence ending
    local lastchar = string.sub(message,-1,-1)
    if lastchar == "." or lastchar == "?" or lastchar == "!" then
        message = string.sub(message,1,-2)
    else
        lastchar = ""
    end
    
    local LastTarget = Game:getLastConnect()
    if LastTarget then
		message = string.gsub(message,"@last",beginChar .. LastTarget:getName() .. endChar)
	end
	
    local _nameMatch = "(%@%w+)"
    while string.find(message,_nameMatch) do
        local str = Misc:match(message,_nameMatch)
        local Target = ClientHandler:getClient( string.gsub(str,"@","",1))
        if not Target then
            message = string.gsub(message,str,string.gsub(str,"@","__TEMP__",1))
        else
            if includeLevel then
                message = string.gsub(message,str,beginChar .. Target:getNameWithRank() .. endChar,1)
            else
                message = string.gsub(message,str,beginChar .. Target:getName() .. endChar,1)
            end
        end
    end
	
    message = string.gsub(message,"__TEMP__","@")
    if lastchar ~= "" then message = message .. lastchar end
	
    return message
end

--- Checks message for swear words and replaces them with a replacement
function MessageHandler:censor(message)
	message = Misc:trim(message)
    if message == "" then return "" end
    if not next(SwearReplace) then return message end
	
    local words = Misc:split( message , " " )
    if not words or not next(words) then return message end
	
    for censorword , censor_replace in pairs(SwearReplace) do
		for k=1,tlen(words) do
			local word = string.lower( words[k] )
			if string.find( word , censorword , 1 , true ) then
				words[k] = string.gsub(word,censorword,censor_replace)
            end
        end
    end
	
    message = table.concat(words," ")
	
    return message
end

--- Checks message for a common spelling or grammar error and replaces it with correct version
-- Sometimes it is also an acronym as well
-- Also adds a sentence ending punctuation
-- Find new name for this function
function MessageHandler:grammar(message)
	message = Misc:trim(message)
    if message == "" then return "" end
    if not next(Grammar) then return message end
	
    local words = Misc:split( message , " " )
    if not words or not next(words) then return message end
	
	for grammarword , grammar_replace in pairs(Grammar) do
		for k=1, tlen(words) do
			local word = string.lower( words[k] )
			if word == grammarword then
				words[k] = grammar_replace
			end
        end
    end
	
    message = table.concat(words," ")
	
    if tlen(words) >= 2 then
		local lastchar = string.sub( message , -1 )
		if lastchar ~= '?' and lastchar ~= '!' and lastchar ~= '.' then
            message = message .. "."
        end
    end
	
    return message
end

--- Replaces message with a Title message
-- Ex: HELLO THERE BOB to Hello there bob
function MessageHandler:nocaps(message)
	return string.gsub(string.lower(message), "^%l", string.upper)
end

--- Removes colors from a message
function MessageHandler:nocolors(message)
	return et.Q_CleanStr(message)
end

--- Formats a message based on Config.Message.Format
function MessageHandler:sformat( message , Client )
	message = Misc:trim( message )
    local new_message = self:replace( Config.Message.Format , true , Client )
    return string.gsub(new_message,"<message>",message)
end

--- Adminwatch dispatcher
function MessageHandler:adminwatch(message,team,clientNum,awprint)
    if not team      then team      = 0     end
    if not clientNum then clientNum = -1    end
    if not awprint   then awprint   = false end
	
    if Config.AdminWatchConsole then Console:InfoClean("[AdminWatch] " .. message) end
	
    for k = 1, tlen(Clients) do
        if Clients[k]:isOnline() and Clients[k].User:getKey("adminwatch") then
            if ( Clients[k]:getTeam() ~= team and Clients[k].id ~= clientNum ) then
                if Clients[k].User:getKey("adminwatchsound") then
                    Clients[k]:Play(Config.Sound.Adminwatch)
                end
                if aw_print then
                    Clients[k]:AWPrint(message)
                else
                    Clients[k]:AWChat(message)
                end
            end
        end
    end
end
--[[
function Core:NewChat(Client,message,where)
    local TITLES = {
        [0]="Level 0",
        "Level 1",
        "Level 2",
        "Level 3",
        "Level 4",
        "Level 5",
        "Level 6",
        "Level 7",
        "Level 8",
        "Level 9",
        "Level 10",
        "Level 11",
        "Level 12",
        "Level 13",
        "Level 14",
        "Level 15",
        "Level 16",
        "Level 17",
        "Level 18",
        "Level 19",
        "Level 20",
        "Level 21",
        "Level 22",
        "Level 23",
        "Level 24",
        "Level 25",
    }
    local TEXT_FORMAT   = "^h<LEVEL>^i|^d<LEVEL_TITLE>^i|^7<NAME>^i>>^g <MESSAGE>"
    local new_message = string.gsub(TEXT_FORMAT,"<LEVEL>",tostring(Client:getLevel()))
    new_message = string.gsub(new_message,"<LEVEL_TITLE>",tostring(TITLES[Client:getLevel()]))
    new_message = string.gsub(new_message,"<NAME>",Client:getName())
    new_message = string.gsub(new_message,"<MESSAGE>",message)
    Team color change
    Special title
        option for only authorize to set title
        option to decide length
        /edit title My Title
    Sound chat sound
    Event play so can have background opacity
        For flags maybe as well
    Check ignored
    Check muted
    for k=1,tlen(Clients) do
        local f = false
        if where == "say" then f = true end
        
        if Client:getTeam() == Clients[k]:getTeam() and ( where == "say_team" or where == "say_buddy" ) then
            f = true
        end
        if f then
            Clients[k]:ChatClean(new_message)
        end
    end
end]]--