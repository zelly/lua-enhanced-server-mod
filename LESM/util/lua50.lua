local _tlen = table.getn

function tlen( t )
    return _tlen( t )
end

function va(...)
    return string.format(unpack(arg))
end