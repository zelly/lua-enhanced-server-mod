RoutineHandler                = { }
RoutineHandler.Routines       = { }
RoutineHandler.running        = false -- If a routine is currently running
RoutineHandler.banner_current = 1     -- Current banner message we are printing
RoutineHandler.banners        = { }   -- The banner table we are printing
RoutineHandler.banner_id      = nil   -- Banner routine id

-- Not 100% sure if this is the right word as I am not actually using the co routines lua functions
-- Like I originally was, it is more of just a scheduler now

-- TODO idea Add an option to routine table:
-- client = true
-- Will automatically iterate over clients and send func(Client)
-- So we can maybe eliminate iterating over clients multiple times at same time frame?

--- Get routine with [id]
-- returns routine table
function RoutineHandler:getWithId(id)
	for k=1,tlen(self.Routines) do
		local routine = self.Routines[k]
		if id == routine.id then
			return routine,k
		end
	end
	return nil
end

--- Gets a routine id that does not exist yet
function RoutineHandler:getNextId()
	local id = 0
	while true do
		id = id + 1
		if not self:getWithId(id) then return id end
	end
end

--- Adds all of the routines needed by LESM
function RoutineHandler:RegisterCoreRoutines()
	self:add(function() Logger:SaveDebugStream() end,15,15,"Saves debug and custom log stream")
	--[[
	self:add(function() 
		for k=1,tlen(Clients) do
			Clients[k]:runTimedPrint()
		end
	end, 0.5 , 5 , "Run timed print for large prints")--]]
	
	self:add(function() 
		for k=1,tlen(Clients) do
			Clients[k]:updateTimeVars(0.25)
			Clients[k]:runTimedPrint()
		end
	end, 0.25 , 0 , "Updates time variables & run timed print for large prints")

	if Game:Gamestate() ~= 0 then return end -- The rest should only be ran during game
	
	if Config.PauseStart then -- make shift pause start to bypass yellow bar at begining
		et.trap_SendConsoleCommand( et.EXEC_APPEND , "pause\n" )
		et.trap_SendConsoleCommand( et.EXEC_APPEND , "unpause\n" )
	elseif Config.FreezeStart > 0.0 then
		Console:Chat("Freeze for " .. Color.Secondary .. Config.FreezeStart .. Color.Primary .. " seconds")
		Game.Freeze = true
		local timelimit = tonumber(et.trap_Cvar_Get("timelimit"))
		local addtime   = Config.FreezeStart / 60
		et.trap_Cvar_Set("timelimit",addtime+timelimit)
		self:add( function()
			Game.Freeze = false
			for k=1,tlen(Clients) do
				Clients[k].freeze = false
				Clients[k].freezeLocation = { }
			end
			Console:Chat("START!")
		end,0,Config.FreezeStart,"Unfreezes players" )
	end
	
	if Game:Mod() == "silent" and Config.Profile.AutoWidth > 1 then
		self:add(function()
			for k=1,tlen(Clients) do
				local Client = Clients[k]
				if Client:isOnline() and Client.User:getKey("maxchars") <= 10 then
					Client:checkCvar("r_mode" , Core.rmode_function)
				end
			end
		end , Config.Profile.AutoWidth , 10 , "Gets client's r_mode cvar" )
	end
	
	self:add(function()
		for k=1,tlen(Clients) do
			Clients[k]:pingCheck()
		end
	end, 30 , 5 , "Checks whether or not to take action on clients ping")
	
	self:add(function() 
		for k=1,tlen(Clients) do
			Clients[k]:pulseLowHealth()
		end
	end, 1 , 30 , "Pulse health check")
	
	self:add(function() 
		for k=1,tlen(Clients) do
			Clients[k]:pingChange()
		end
	end, 10 , 5 , "Add new client ping to table")
	
	self:add(function() 
		for k=1,tlen(Clients) do
			Clients[k]:addTime(10.0)
		end
	end, 10 , 10 , "Add time to user profile")
	
	if Config.XpSave.Enabled then
		self:add(function() 
			for k=1,tlen(Clients) do
				Clients[k]:saveXp()
			end
		end, 10 , 10 , "Save clients xp")
	end
	
	self:add(function() 
		for k=1,tlen(Clients) do
			Clients[k]:ammoCheck()
		end
	end, 0.5 , 5 , "Ammo check")

	if Config.ClassHPRegen then
		self:add(function() 
			for k=1,tlen(Clients) do
				if Clients[k]:isAlive() == 1 and Clients[k]:getHealth() > Clients[k]:getClassTable().maxhp then
					Clients[k]:setHealth(Clients[k]:getHealth() + Clients[k]:getClassTable().regen)
				end
			end
		end, 0.25 , 0 , "Class health regen")
	end
	
	if Config.MapEnts then
		self:add(function() 
			EntityHandler:Map()
			for k=1,tlen(Clients) do
				if Clients[k].entityMessage <= 0.0 then
					EntityHandler:MapMessage(Clients[k])
				end
			end
		end, 1 , 0 , "Map entity loop")
	end
	
	if Config.Spec999 > 0.0 then
		self:add(function()
			Game:Spec999()
		end, 0 , Config.Spec999 , "Spec 999 after warmup")
	end
end

--- Remove a routine from the table
function RoutineHandler:remove( id )
	id = tonumber(id)
	if not id then
		Console:Warning("Tried to remove invalid rout id")
		return
	end
	local rout,index = self:getWithId( id )
	rout.remove = true
end

--- Add a new routine to the table
function RoutineHandler:add( func , loop , run , description )
	if type(func) ~= "function" then return end
	loop = tonumber(loop) or 0
	run  = tonumber(run) or 0
	if loop == 0 and run == 0 then return end -- Will never run
	description = Misc:trim(description)
	loop = math.floor(loop*1000) -- Loop time converted to milliseconds
	if run ~= 0 then run = Game:getLevelTime() + math.floor(run*1000) end -- Run converted to game level time if not 0
	local id = self:getNextId()
	self.Routines[tlen(self.Routines)+1] = {
		id   = id,
		func = func,
		loop = loop,
		run  = run,
		last = 0,
		desc = description,
	}
	Console:Info( "Routine add: " .. description )
	return id -- Return id just incase we need to do more with routine after that, also able to check if it succeeds if not nil
end

--- Routine Table:
-- id  : unique id of routine
-- func: Function to run
-- loop: Milliseconds between loops if 0 then is a "run once" routine and will be deleted on run
-- last: LevelTime that the routine last ran ( 0 to start aka not ran yet )
-- run : LevelTime to start the routine run. (Usually 0 for looping routines)
-- desc: description for routine command
function RoutineHandler:run()
	if self.running then return end
	local currenttime = Game:getLevelTime()
	if currenttime == 0 then self.running = false ; return end -- If game not running then do not do routines
	
	if not currenttime then
		Console:Warning("LevelTime is nil")
		return
	end
	
	local remove = { }
	
	for k=1,tlen(self.Routines) do
		local routine = self.Routines[k]
		if not routine then break end -- Likely removed while running loop from outside this function
		if routine.remove then
			remove[tlen(remove)+1] = routine.id
		else
			if routine.run == 0 or routine.run <= currenttime then
				if routine.loop == 0 or routine.last == 0 or ( routine.last + routine.loop ) <= currenttime then
					if not routine.run or not routine.loop then
						Console:Warning( tostring(routine.desc) .. " has nil value loop:"..tostring(routine.loop).." run: "..tostring(routine.run) )
						remove[tlen(remove)+1] = routine.id
						break--break loop due to error (will clean up loop next go
					end
					routine.last = currenttime
					routine.func()
					if routine.loop == 0 then
						remove[tlen(remove)+1] = routine.id -- Add to list of routines to be deleted
						Console:Info("Remove Routine " .. routine.desc )
					end
				end
			end
		end
	end

	-- Remove dead routines if they exist
	if next(remove) then
		for k=1,tlen(remove) do
			local _,index = self:getWithId(remove[k])
			if index then
				table.remove(self.Routines,index)
			end
		end
	end
	self.running = false
end

--- Refresh the banner relay system
function RoutineHandler:refreshBanners()
	local bannerid = self.banner_id -- quick copy 
	self.banner_current = 1
	self.banners        = { }
	self.banner_id      = nil
	if not Config.Banner.Enabled then return end
	
	Console:Info("Refreshing banners")
	
	for k=1,tlen(Banners) do
		self.banners[tlen(self.banners)+1] = Banners[k]
	end
	
	if Config.Banner.AddRules then
		for k=1,tlen(Rules) do
			self.banners[tlen(self.banners)+1] = Rules[k]
		end
	end
	
	if Config.Banner.Random then
		self.banners = Misc:table_shuffle(self.banners)
	end
	
	if bannerid and self:getWithId(bannerid) then
		self:remove(bannerid)
	end
	
	self.banner_id = self:add( function()
		self:bannerRelay()
	end , Config.Banner.Delay,5,"Banner messages" )
end

--- Relay the banner messages out
function RoutineHandler:bannerRelay()
	if self.banner_current >= tlen(self.banners) or self.banner_current <= 0 then
		self.banner_current = 1
	end
	
	if not self.banners[self.banner_current] then
		Console:Warning(va("Banner table empty at %d" , self.banner_current ))
		self.banner_current = 1 -- attempt to fix by going back to begining
		return
	end
	
	self:bannerDisplay( self.banners[self.banner_current] )
	self.banner_current = self.banner_current + 1
end

--- Display the banner message
function RoutineHandler:bannerDisplay( message )
	message = Misc:trim(message)
	if message == "" then return end
	Console:IteratePrint( message , Misc:trim(Config.Banner.Location,true,true) , "bannerlocation" , nil , true )
end
