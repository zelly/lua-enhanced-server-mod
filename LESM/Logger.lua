Logger             = { }
Logger.path = "LESM/logs/" -- FileHandler:Save already puts Core.cwd in front
Logger.filedata    = { }
Logger.debugstream = { }
Logger.file        = os.date("%B %d %A")..".log"
-- Init(GameState 2 == warmup)
-- ShutDown(Gamestate 0 == warmup)
-- Init(GameState 0 == game)
-- ShutDown(Gamestate -1 == game)

-- GAME
-- { "warmup"      , "MapName" }
-- { "warmup_end"  , "MapName" }
-- { "game"        , "MapName" }
-- { "intermission", "MapName" }
-- { "game_end"    , "MapName" }

-- Clients
-- { "connect"        , "Id ClientGuid ClientIp Country ClientName", }
-- { "switchteam"     , "Id FromTeam ToTeam" }
-- { "begin"          , "Id Username" }
-- { "disconnect"     , "Id" }
-- { "command"        , "Id CommandType CommandName Arguments")
-- { "chat"           , "Id where message")
-- { "voicechat"      , "Id where message")
-- { "privatemessage" , "FromId ToId Message" }
-- { "timedout"       , "Id" }
-- { "blacklist"      , "Id" }

-- Bots
-- { "bot_connect" , "Id BotName", }
-- { "bot_switchteam"  , "Id FromTeam ToTeam" }
-- { "bot_begin"   , "Id" }
-- { "bot_disconnect"  , "Id" }
-- { "bot_chat"     , "Id where message")
-- { "bot_voicechat"     , "Id where message")

-- Events
-- { "obituary"    , "KillerId VictimId MethodOfDeath" }
-- { "bot_obituary"    , "KillerId VictimId MethodOfDeath" }
-- { "vote" , "id vote etc" }
-- { "console"     , "message" }

local pcall = pcall

function Logger:Init()
    Console:Info("Loading Logger")
    if Config.Log.OneFile then
        self.file = "lesm_log.log"
    else
        self.file = os.date("%B %d %A")..".log"
    end
end

--- Saves all files
function Logger:DeInit()
    Console:Info("Saving Logs")
    self:Save()
    self:SaveDebugStream()
end

--- Adds a log message with a timestamp and type of log
function Logger:Log(LogType,LogMessage)
    local datetime = os.date("%X")
    
    if Config.Log.OneFile then datetime = os.date("%x_%X") end
    local NewLog = {
        datetime,
        LogType,
        LogMessage,
    }
    self.filedata[tlen(self.filedata)+1] = NewLog
end

--- Adds a Debug message to the log
-- If streaming is enabled will append to debug.log
function Logger:Debug(message,type)
    if not type then type = "other" end
    type = string.lower(Misc:trim(type))
    if type == "" then type = "other" end
    if Config.Debug[type] then
        et.G_LogPrint("[DEBUG] ".. message  .."\n")
        if Config.Debug.ToChat then
            Console:ChatClean(message)
        elseif Config.Debug.ToPrint then
            Console:PrintClean(message)
        end
        if ( Config.Debug.Stream ) then
            self.debugstream[tlen(self.debugstream)+1] = os.date("%X") .. " " .. message
        end
    end
end

--- Saves all logs that currently are not saved
function Logger:Save()
    if not next(self.filedata) then return end
    local FileData = { }
    if Config.Log.Full then
        FileData = self:FullMessage()
    else
        for k=1,tlen(self.filedata) do FileData[tlen(FileData)+1] = self.filedata[k][1] .. " " .. self.filedata[k][2] .. " " .. self.filedata[k][3] end
    end
    if not next(FileData) then return end
    if not Config.Log.Colors then
        for k = 1 , tlen(FileData) do FileData[k] = et.Q_CleanStr(FileData[k]) end
    end
    FileHandler:Save{ filePath = self.path .. self.file , fileData = FileData, fileType = "text",allowEmpty = false, saveType="a"}
end

--- Saves the debug.log stream
function Logger:SaveDebugStream()
    if not Config.Debug.Stream then return end
    if not next(self.debugstream) then return end
    FileHandler:Save{ filePath = self.path .. "debug.log" , fileData = self.debugstream, fileType = "text",allowEmpty = false, saveType="a"}
    self.debugstream = { }
end

local _team = function(team)
    team = tonumber(team) or 0
    if team == 1 then
        return "Axis"
    elseif team == 2 then
        return "Allies"
    elseif team == 3 then
        return "Spectator"
    end
    return "None"
end
local _errorFileName = et.trap_Cvar_Get( 'fs_homepath' ) .. '/' .. et.trap_Cvar_Get( 'fs_game' ) .. '/LESM_Errors.log'
local _loggerError = function( message )
    if not message then return end
    message = string.format( message )
    LOG_ERROR( "LESM:Logger" , message )
end
--- Converts Log messages to full explained messages
function Logger:FullMessage()
    local _clientNames = { }
    local _pattern     = { }
    _pattern["warmup"]       = "(.*)"
    _pattern["warmup_end"]   = "(.*)"
    _pattern["game"]         = "(.*)"
    _pattern["game_end"]     = "(.*)"
    _pattern["intermission"] = "(.*)"
    _pattern["connect"]      = "(%d+) (%S+) (%S+) (%S+) (.*)"
    _pattern["disconnect"]   = "(%d+)"
    _pattern["switchteam"]   = "(%d+) (%d+) (%d+)"
    _pattern["begin"]        = "(%d+) (%S*)"
    _pattern["timedout"]     = "(%d+)"
    _pattern["blacklist"]    = "(%d+)"
    _pattern["command"]      = "(%d+) (%S+) (%S+) (.*)"
    _pattern["chat"]         = "(%d+) (%S+) (.*)"
    _pattern["voicechat"]    = "(%d+) (%S+) (.*)"
    _pattern["privatemessage"]="(%d+) (%S+) (.*)"
    _pattern["obituary"]     = "(%d+) (%d+) (%d+)"
    _pattern["vote"]         = "(%d+) (%S+) (.*)"
    _pattern["console"]      = "(.*)"
    
    _pattern["bot_connect"]    = "(%d+) (.*)"
    _pattern["bot_begin"]      = "(%d+)"
    _pattern["bot_disconnect"] = "(%d+)"
    _pattern["bot_chat"]       = "(%d+) (%S+) (.*)"
    _pattern["bot_voicechat"]  = "(%d+) (%S+) (.*)"
    _pattern["bot_obituary"]   = "(%d+) (%d+) (%d+)"
    
    local _getClientName = function( clientNum )
        clientNum = tonumber(clientNum)
        if not clientNum then
            _loggerError("Could not find clientNum in Logger:FullMessage _getClientName")
            return ""
        end
        if not _clientNames[clientNum] then
            _loggerError(va("Could not find clientName for %d in Logger:FullMessage _getClientName",clientNum))
            return ""
        end
        return _clientNames[clientNum]
    end
    
    local _setClientName = function( clientNum , clientName )
        clientNum = tonumber(clientNum)
        if not clientNum then
            _loggerError("Could not find clientNum in Logger:FullMessage _setClientName")
            return
        end
        clientName = Misc:trim(clientName)
        if clientName == "" then
            _loggerError(va("Could not find clientName for %d in Logger:FullMessage _setClientName",clientNum))
            clientName = "emptyname"
        end
        _clientNames[clientNum] = clientName
    end
    local _eraseClientName = function(clientNum)
        clientNum = tonumber(clientNum)
        if not clientNum then
            _loggerError("Could not find clientNum in Logger:FullMessage _eraseClientName")
            return
        end
        if not _clientNames[clientNum] then
            _loggerError(va("Could not find clientName for %d in Logger:FullMessage _eraseClientName",clientNum))
            return
        end
        _clientNames[clientNum] = nil
    end
    
    local _findLog = function(logData )
        if not logData then
            _loggerError("Could not find logData in Logger:FullMessage _findLog")
            return ""
        end
        if not _pattern[logData[2]] then
            _loggerError("Could not find _pattern[logData[2]] in Logger:FullMessage _findLog")
            return ""
        end
        logData[3] = tostring(logData[3])
        return Misc:match(logData[3],_pattern[logData[2]])
    end
    local _getLogMessage = function (logData)
        if not _findLog(logData) then return end
        if logData[2] == "warmup" then
            return "Warmup started on " .. Color.Secondary .. _findLog(logData)
        elseif logData[2] == "warmup_end" then
            return "Warmup ended on " .. Color.Secondary   .. _findLog(logData)
        elseif logData[2] == "game" then
            return "Game started on " .. Color.Secondary .. _findLog(logData)
        elseif logData[2] == "game_end" then
            return "Game ended on " .. Color.Secondary .. _findLog(logData)
        elseif logData[2] == "intermission" then
            return "Intermission started on " .. Color.Secondary .. _findLog(logData)
        elseif logData[2] == "connect" then
            local clientNum , clientGuid, clientIp, clientCountry, clientName = _findLog(logData)
            _setClientName(clientNum,clientName)
            return "("..Color.Secondary..clientNum..Color.Primary..") " .. clientName .. Color.Primary .. " Connected with guid("..Color.Secondary..clientGuid..Color.Primary..") ip("..Color.Secondary..clientIp..Color.Primary..") country("..Color.Secondary..clientCountry..Color.Primary..")"
        elseif logData[2] == "disconnect" then
            local clientNum  = _findLog(logData)
            local clientName = _getClientName(clientNum)
            _eraseClientName(clientNum)
            return "("..Color.Secondary..clientNum..Color.Primary..") " .. clientName .. Color.Primary .. " Disconnected"
        elseif logData[2] == "switchteam" then
            local clientNum,fromTeam,toTeam = _findLog(logData)
            return _getClientName(clientNum) .. Color.Primary .. " switched to " .. Color.Secondary .. _team(toTeam) .. Color.Primary .. " from " .. Color.Secondary .. _team(fromTeam)
        elseif logData[2] == "begin" then
            local clientNum,userName = _findLog(logData)
            return _getClientName(clientNum) .. Color.Primary .. " client begin on username(" .. Color.Secondary .. userName .. Color.Primary .. ")"
        elseif logData[2] == "timedout" then
            local clientNum = _findLog(logData)
            return _getClientName(clientNum) .. Color.Primary .. " client timed out and was moved to spectator"
        elseif logData[2] == "blacklist" then
            local clientNum = _findLog(logData)
            return _getClientName(clientNum) .. Color.Primary .. " blacklisted user tried connecting"
        elseif logData[2] == "command" then
            local clientNum , commandType, commandName, commandArgs = _findLog(logData)
            return _getClientName(clientNum) .. " " .. Color.Secondary .. string.upper(commandType) .. Color.Tertiary .. "(" .. Color.Secondary .. string.lower(commandName) .. Color.Tertiary ..") " .. Color.Primary .. commandArgs
        elseif ( logData[2] == "chat" or logData[2] == "voicechat" ) then
            local clientNum , chatWhere, chatMessage = _findLog(logData)
            return _getClientName(clientNum) .. " " .. Color.Secondary .. string.upper(chatWhere) .. Color.Primary .. " " .. chatMessage
        elseif ( logData[2] == "privatemessage" ) then
            local fromId , targetClients, pmMessage = _findLog(logData)
            return "(PM)" .. _clientNames[fromId] .. Color.Tertiary .. "->" .. Color.Primary .. targetClients .. " " .. Color.Primary .. pmMessage
        elseif ( logData[2] == "obituary" or logData[2] == "bot_obituary" ) then
            local killerId,victimId,modId = _findLog(logData)
            local weaponName = tonumber(modId) or 0
            local killerName = _clientNames[killerId] or "*unknown*"
            local victimName = _clientNames[victimId] or "*unknown*"
            for k = 1 , tlen(Weapons) do 
                if Weapons[k].id == modId and Weapons[k].nickname then weaponName = Weapons[k].nickname end
            end
            if not weaponName then weaponName = "*unknown*" end
            return killerName .. Color.Primary .. " killed " .. victimName .. Color.Primary .. " with mod " .. weaponName
        elseif ( logData[2] == "vote" ) then
            local clientNum,voteCalled,voteArgs= _findLog(logData)
            return _getClientName(clientNum) .. Color.Primary .. " called vote " .. voteCalled .. " " .. voteArgs
        elseif ( logData[2] == "console" ) then
            return "CONSOLE: " .. _findLog(logData)
        elseif logData[2] == "bot_begin" then
            local clientNum = _findLog(logData)
            return Color.Secondary .. "[BOT] " .. Color.Primary .. _getClientName(clientNum) .. Color.Primary .. " Client begin"
        elseif ( logData[2] == "bot_chat" or logData[2] == "bot_voicechat" ) then
            local clientNum , chatWhere, chatMessage = _findLog(logData)
            return Color.Secondary .. "[BOT] " .. Color.Primary .. _getClientName(clientNum) .. " " .. Color.Secondary .. string.upper(chatWhere) .. Color.Primary .. " " .. chatMessage
        elseif logData[2] == "bot_disconnect" then
            local clientNum = _findLog(logData)
            local clientName = _getClientName(clientNum)
            _eraseClientName(clientNum)
            return "("..Color.Secondary..clientNum..Color.Primary..") " .. Color.Secondary .. "[BOT] " .. Color.Primary .. clientName .. Color.Primary .. " Disconnected"
        elseif logData[2] == "bot_connect" then
            local clientNum , clientName = _findLog(logData)
            _setClientName(clientNum,clientName)
            return "("..Color.Secondary..clientNum..Color.Primary..") " .. Color.Secondary .. "[BOT] " .. Color.Primary .. clientName
        end
    end
    local new_fd      = { }
    for k=1,tlen(self.filedata) do
        local status,msgReturn = pcall( _getLogMessage,self.filedata[k] )
        if status and msgReturn then
            new_fd[tlen(new_fd)+1] = Color.Secondary .. self.filedata[k][1] .. Color.Primary .. " " .. msgReturn .. Color.Primary
        else
            if msgReturn then LOG_ERROR("LESM:Logger",msgReturn ) end
        end
    end
    return new_fd
end
