UserHandler = { }

------------------
-- LOCAL FUNCTIONS
------------------

--- sort function
-- Sorts by key "id"
local _sortById   = function(a,b) return a.id < b.id end

--- Registers a key for UserProfile
-- Booleans(key , "boolean", default , editable , auth , title )
-- Tables  (key , "table"  , default , editable , auth , title )
-- Strings (key , "string" , default , editable , auth , title , min , max , pattern ) -1 for 
-- Numbers (key , "number" , default , editable , auth , title , min , max ) -1 max = infinite
function UserHandler:RegisterKey(keyName,type,default,editable,auth,title,min,max,pattern)
    if keyName  == nil then return end
    if type     == nil then return end
    if default  == nil then return end
    if editable == nil then return end
    if title    == nil then return end
    if auth     == nil then return end
    local Key    = { }
    Key.name     = string.lower(Misc:trim(keyName))
    Key.type     = string.lower(Misc:trim(type))
    Key.default  = Misc:ConvertType(type,default)
    Key.editable = editable
    Key.auth     = auth
    Key.title    = Misc:trim(title)
    if Key.type == "number" then
        Key.min  = tonumber(min) or 0
        Key.max  = tonumber(max) or -1
    elseif Key.type == "string" then
        Key.pattern  = pattern
        Key.min      = tonumber(min) or 0
        Key.max      = tonumber(max) or -1
    end
    Keys[tlen(Keys)+1] = Key
end

function UserHandler:RegisterKeyDescription(keyname,description)
    keyname     = Misc:triml(keyname)
    description = Misc:trim(description)
    if keyname == "" or description == "" then return end
    local Key;
    for k=1,tlen(Keys) do
        if Keys[k].name == keyname then
            Key = Keys[k]
            break
        end
    end
    if not Key then return end
    Key.desc = description
end

--- Add your register keys here
function UserHandler:RegisterKeys()
    --           NAME                TYPE      DEFAULT  EDITABLE AUTH  TITLE
    self:RegisterKey("authorize"       , "boolean" , false , false , true  , "Authorize")
    self:RegisterKey("adminwatch"      , "boolean" , false , true  , true  , "AdminWatch")
    self:RegisterKey("vsaybypass"      , "boolean" , true  , true  , true  , "VsayBypass")
    self:RegisterKey("adminwatchsound" , "boolean" , false , true  , true  , "AdminWatch Sound")
    self:RegisterKey("fakeping"        , "boolean" , false , true  , true  , "Fake Ping")
    self:RegisterKey("afk"             , "boolean" , false , false , false , "Away")
    self:RegisterKey("ammocheck"       , "boolean" , false , true  , false , "Low Ammo Notifier")
    --self:RegisterKey("autolevel"       , "boolean" , true  , false , false , "AutoLevel")
    self:RegisterKey("autologin"       , "boolean" , true  , false , false , "AutoLogin")
    self:RegisterKey("grammarcheck"    , "boolean" , false , true , false , "Grammar Check")
    self:RegisterKey("nocolors"        , "boolean" , false , true , false , "NoColors")
    self:RegisterKey("nocaps"          , "boolean" , false , true , false , "No Caps")
    self:RegisterKey("swearfilter"     , "boolean" , false , true , false , "Swear Filter")
    self:RegisterKey("sounds"          , "boolean" , true  , true  , false , "Sounds")
    self:RegisterKey("singlepistol"    , "boolean" , false , true  , false , "Secondary Weapon")
    self:RegisterKey("showdamage"      , "boolean" , false , true  , false , "Show Damage")
    self:RegisterKey("showdamagedealt" , "boolean" , false , true  , false , "Show Damage Dealt")
    self:RegisterKey("showweapon"      , "boolean" , false , true  , false , "Show Weapon")
    self:RegisterKey("showname"        , "boolean" , false , true  , false , "Show Name")
    self:RegisterKey("motd"            , "boolean" , true  , true  , false , "Message of the Day")
    
    --           NAME              TYPE     DEFAULT  EDIT    AUTH    TITLE             MIN    MAX
    self:RegisterKey("id"            , "number" , 0    , false , false , "User Id"       , 0 , -1   )
    self:RegisterKey("level"         , "number" , 0    , false , false , "Level"         , 0 , -1   )
    self:RegisterKey("age"           , "number" , 0    , true  , false , "Age"           , 6 , 100  )
    self:RegisterKey("kills"         , "number" , 0    , false , false , "Kills"         , 0 , -1   )
    self:RegisterKey("deaths"        , "number" , 0    , false , false , "Deaths"        , 0 , -1   )
    self:RegisterKey("selfkills"     , "number" , 0    , false , false , "Self Kills"    , 0 , -1   )
    self:RegisterKey("teamkills"     , "number" , 0    , false , false , "Team Kills"    , 0 , -1   )
    self:RegisterKey("teamdeaths"    , "number" , 0    , false , false , "Team Deaths"   , 0 , -1   )
    self:RegisterKey("worlddeaths"   , "number" , 0    , false , false , "World Deaths"  , 0 , -1   )
    self:RegisterKey("botteamdeaths" , "number" , 0    , false , false , "Bot Team Deaths", 0 , -1  )
    self:RegisterKey("botteamkills"  , "number" , 0    , false , false , "Bot Team Kills", 0 , -1   )
    self:RegisterKey("botkills"      , "number" , 0    , false , false , "Bot Kills"     , 0 , -1   )
    self:RegisterKey("botdeaths"     , "number" , 0    , false , false , "Bot Deaths"    , 0 , -1   )
    self:RegisterKey("timeplayed"    , "number" , 0    , false , false , "Time Played"   , 0 , -1   )
    self:RegisterKey("karmagoodin"   , "number" , 0    , false , false , "Good Karma In" , 0 , -1   )
    self:RegisterKey("karmagoodout"  , "number" , 0    , false , false , "Good Karma Out", 0 , -1   )
    self:RegisterKey("karmabadin"    , "number" , 0    , false , false , "Bad Karma in"  , 0 , -1   )
    self:RegisterKey("karmabadout"   , "number" , 0    , false , false , "Bad Karma Out" , 0 , -1   )
    --self:RegisterKey("karmalast"     , "number" , -1   , false , false , "Karma Last"    , -1, -1   )
    self:RegisterKey("karmatime"     , "number" , 0    , false , false , "Karma Time"    , 0 , -1   )
    self:RegisterKey("afktime"       , "number" , 0    , false , false , "Afk Time"      , 0 , -1   )
    self:RegisterKey("spam"          , "number" , 0    , false , false , "Spam"          , 0 , -1   )
    self:RegisterKey("pingmin"       , "number" , 0    , true  , true  , "Min Fake Ping" , 0 , 999  )
    self:RegisterKey("pingmax"       , "number" , 0    , true  , true  , "Max Fake Ping" , 0 , 999  )
    self:RegisterKey("pulsetime"     , "number" , 20   , true  , false , "Pulse Delay"   , 1 , 3600 )
    self:RegisterKey("pulsedistance" , "number" , 0    , true  , false , "Pulse Distance", 0 , 5000 ) -- Dunno if these values are good or not
    self:RegisterKey("pulsehealth"   , "number" , 35   , true  , false , "Pulse Health"  , 0 , -1   )
    self:RegisterKey("timeout"       , "number" , 0    , false , false , "Timeout"       , 0 , -1   ) -- Date
    self:RegisterKey("lastseen"      , "number" , 0    , false , false , "Last Seen"     , 0 , -1   ) -- Date
    self:RegisterKey("joindate"      , "number" , 0    , false , false , "Join Date"     , 0 , -1   ) -- Date
    self:RegisterKey("maxchars"      , "number" , -1   , true  , false , "Max Characters", -1 , 500 )
	self:RegisterKey("maxrows"       , "number" , 25   , true  , false , "Max Rows"      , 10 , 500 )
    self:RegisterKey("unitmeasurement" , "number"  , 0 , true  , false , "Unit Measurement" , 0 , 3 )
    
    --           NAME              TYPE       DEFAULT  EDIT    AUTH    TITLE MIN MAX  PATTERN
    self:RegisterKey("afkreason"     , "string" , ""     , false , false , "Afk Reason"          , 0 , 150  , "")
    self:RegisterKey("autologinguid" , "string" , ""     , false , false , "AutoLogin Guid"      , 0 , 32  , "[^A-Za-z0-9]")
    self:RegisterKey("autologinip"   , "string" , ""     , false , false , "AutoLogin Ip"        , 7 , 15  , "[^0-9.]")
    if Game:Mod() == "silent" then
        self:RegisterKey("autologinsid"  , "string" , ""     , false , false , "AutoLogin Silent Id" , 0 , 32  , "[^A-Za-z0-9]")
        self:RegisterKeyDescription( "autologinsid" , "Silent guid in which you are being auto logged into" )
    elseif Game:Mod() == "etpro" then
        self:RegisterKey("autologinetpro"  , "string" , ""     , false , false , "AutoLogin ETPro Guid" , 0 , 32  , "[^A-Za-z0-9]")
        self:RegisterKeyDescription( "autologinetpro" , "ETPro guid youa re being auto logged into" )
    end
    self:RegisterKey("forumsname"    , "string" , ""     , true  , false , "Forums Name" , 0 , -1  , "[\\\n\"'%%]")
    self:RegisterKey("password"      , "string" , ""     , true  , false , "Password" , 4 , 32  , "[\\\n\"'%% ]")
    self:RegisterKey("realname"      , "string" , ""     , true  , false , "Real Name" , 0 , 64  , "[^A-Za-z ]")
    self:RegisterKey("username"      , "string" , ""     , true  , false , "UserName" , 4 , 32  , "[^A-Za-z0-9_%-]")
    self:RegisterKey("xfire"         , "string" , ""     , true  , false , "Xfire" , 4 , 32  , "[^A-Za-z0-9_]")
    self:RegisterKey("greeting"      , "string" , ""     , true  , false , "Greeting" , 0 , 150 , "")
    self:RegisterKey("greetingsound" , "string" , ""     , true  , false , "Greeting Sound" , 0 , -1  , "[^A-Za-z0-9_%-%.]")
    self:RegisterKey("farewell"      , "string" , ""     , true  , false , "FareWell" , 0 , 150 , "")
    self:RegisterKey("farewellsound" , "string" , ""     , true  , false , "FareWellSound" , 0 , -1  , "[^A-Za-z0-9_%-%.]")
    
    --self:RegisterKey("location" , "string" , "" , false , false , ""      , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("greetinglocation"  , "string" , "" , true  , false , "Greeting Location"       , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("farewelllocation"  , "string" , "" , true  , false , "Farewell Location"       , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("playsoundlocation" , "string" , "" , true  , false , "Playsound Location"      , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("deathprintlocation", "string" , "" , true  , false , "DeathPrint Location"     , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("connectlocation"   , "string" , "" , true  , false , "Connect Location"        , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("bannerlocation"    , "string" , "" , true  , false , "Banner Location"         , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("dynolocation"      , "string" , "" , true  , false , "Dyno Counter Location"   , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("damagelocation"    , "string" , "" , true  , false , "Damage Print Location"   , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("ammochecklocation" , "string" , "" , true  , false , "Ammo Check Location"     , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("pulselocation"     , "string" , "" , true  , false , "Health Pulse Location"   , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("afklocation"       , "string" , "" , true  , false , "Health Pulse Location"   , 0 , 32  , "[^A-Za-z]")
    self:RegisterKey("autolevellocation" , "string" , "" , true  , false , "Auto Level Location"     , 0 , 32  , "[^A-Za-z]")
    
    self:RegisterKey("silent"    , "table",{},false,true ,"Silent Ids")
    self:RegisterKey("etpro"     , "table",{},false,true ,"ETPro guids")
    self:RegisterKey("mac"       , "table",{},false,true ,"Mac Addresses")
    self:RegisterKey("guid"      , "table",{},false,true ,"Guids")
    self:RegisterKey("ip"        , "table",{},false,true ,"Ip Addresses")
    self:RegisterKey("alias"     , "table",{},false,true ,"Aliases")
    self:RegisterKey("savepoint" , "table",{},false,true ,"SavePoints")
    self:RegisterKey("skills"    , "table",{},false,false,"Skills")
    self:RegisterKey("pings"     , "table",{},false,true ,"Pings")
    self:RegisterKey("karmalast" , "table",{},false,true ,"Last id's karma'd")
    
    self:RegisterKeyDescription( "unitmeasurement" , "Changes distance output to from 0(quake units) 1(Imperial units) or 2(Metric units)" )
    self:RegisterKeyDescription( "vsaybypass" , "Bypasses nocaps,grammarcheck, and swear filter for user" )
    self:RegisterKeyDescription( "authorize" , "If an authorized user" )
    self:RegisterKeyDescription( "adminwatch" , "If using adminwatch" )
    self:RegisterKeyDescription( "adminwatchsound" , "If playing adminwatch sounds" )
    self:RegisterKeyDescription( "fakeping" , "If fake ping is active" )
    self:RegisterKeyDescription( "afk" , "If currently away" )
    self:RegisterKeyDescription( "ammocheck" , "If using ammo check notifier" )
    self:RegisterKeyDescription( "autologin" , "If profile will be auto logged in" )
    self:RegisterKeyDescription( "grammarcheck" , "If message grammar/ spellcheck is on" )
    self:RegisterKeyDescription( "nocolors" , "If message colors are stripped" )
    self:RegisterKeyDescription( "nocaps" , "If message is forced to lower case" )
    self:RegisterKeyDescription( "swearfilter" , "If swear words in message are replaced" )
    self:RegisterKeyDescription( "sounds" , "If lua sounds are played" )
    self:RegisterKeyDescription( "singlepistol" , "If spawn with singlepistol only" )
    self:RegisterKeyDescription( "showdamage" , "If prints damage received" )
    self:RegisterKeyDescription( "showdamagedealt" , "If prints damage dealt" )
    self:RegisterKeyDescription( "showweapon" , "If prints weapon in damage print" )
    self:RegisterKeyDescription( "showname" , "If prints name in damage print" )
    self:RegisterKeyDescription( "id" , "Id of user profile" )
    self:RegisterKeyDescription( "level" , "Last known shrubbot level or current level for non-shrubbot" )
    self:RegisterKeyDescription( "age" , "Real life age" )
    self:RegisterKeyDescription( "kills" , "Amount of kills on other players" )
    self:RegisterKeyDescription( "deaths" , "Amount of deaths from other players" )
    self:RegisterKeyDescription( "selfkills" , "Amount of suicides" )
    self:RegisterKeyDescription( "teamkills" , "Amount of teamkills on other players" )
    self:RegisterKeyDescription( "teamdeaths" , "Amount of teamdeaths from other players" )
    self:RegisterKeyDescription( "worlddeaths" , "Amount of deaths from the map or other unknown causes" )
    self:RegisterKeyDescription( "botkills" , "Amount of kills on bots" )
    self:RegisterKeyDescription( "botdeath" , "Amount of deaths from bots" )
    self:RegisterKeyDescription( "botteamkills" , "Amount of teamkills on bots" )
    self:RegisterKeyDescription( "boteamdeaths" , "Amount of teamdeaths from bots" )
    self:RegisterKeyDescription( "timeplayed" , "Seconds played on axis or allies" )
    self:RegisterKeyDescription( "karma" , "Amount of karma received" )
    self:RegisterKeyDescription( "karmalast" , "Last user karma command was used on" )
    self:RegisterKeyDescription( "karmatime" , "Date last used karma" )
    self:RegisterKeyDescription( "karmaout" , "Amount of karma given" )
    self:RegisterKeyDescription( "afktime" , "Seconds away" )
    self:RegisterKeyDescription( "spam" , "Spam protection multiplier" ) -- i think
    self:RegisterKeyDescription( "pingmin" , "Fake ping minimum" )
    self:RegisterKeyDescription( "pingmax" , "Fake ping maximum" )
    self:RegisterKeyDescription( "pulsetime" , "Delay between health pulse check in seconds" )
    self:RegisterKeyDescription( "pulsedistance" , "Distance health pulse checks in quake units" )
    self:RegisterKeyDescription( "pulsehealth" , "Maximum health for health pulse to show" )
    self:RegisterKeyDescription( "timeout" , "Date in which the login will be terminated" )
    self:RegisterKeyDescription( "lastseen" , "Date in which user was last seen" )
    self:RegisterKeyDescription( "joindate" , "Date in which user was registered" )
    self:RegisterKeyDescription( "afkreason" , "Reason for going away" )
    self:RegisterKeyDescription( "autologinguid" , "Guid in which you are being auto logged into" )
    self:RegisterKeyDescription( "autologinip" , "Ip address in which you are being auto logged into" )
    self:RegisterKeyDescription( "forumsname" , "Forums username" )
    self:RegisterKeyDescription( "password" , "Password for user profile" )
    self:RegisterKeyDescription( "realname" , "First and last name in real life" )
    self:RegisterKeyDescription( "username" , "User profile username" )
    self:RegisterKeyDescription( "xfire" , "Xfire chat client username" )
    self:RegisterKeyDescription( "greeting" , "Connect greeting message" )
    self:RegisterKeyDescription( "greetingsound" , "Connect greeting sound title" )
    self:RegisterKeyDescription( "farewell" , "Disconnect farewell message" )
    self:RegisterKeyDescription( "farewellsound" , "Disconnect farewell sound title" )
    self:RegisterKeyDescription("silent" , "Stores all silent guids")
    self:RegisterKeyDescription("etpro" , "Stores all etpro anticheat guids")
    self:RegisterKeyDescription("mac"    , "Stores all mac addresses")
    self:RegisterKeyDescription("guid","Stores all guids")
    self:RegisterKeyDescription("ip","Stores all ip addresses")
    self:RegisterKeyDescription("alias","Stores all aliases")
    self:RegisterKeyDescription("savepoint","Stores origins from save command")
    self:RegisterKeyDescription("skills","Stores non-shrubbot xp values")
    self:RegisterKeyDescription("pings","Stores average ping values")
    self:RegisterKeyDescription( "greetinglocation" , "Location where to print connect greeting" )
    self:RegisterKeyDescription( "farewelllocation" , "Location where to print disconnect farewell" )
    self:RegisterKeyDescription( "playsoundlocation" , "Location where to print playsound messages" )
    self:RegisterKeyDescription( "deathprintlocation" , "Location where to print death message" )
    self:RegisterKeyDescription( "connectlocation" , "Location where to print connect message" )
    self:RegisterKeyDescription( "bannerlocation" , "Location where to print banners" )
    self:RegisterKeyDescription( "dynolocation" , "Location where to print dyno counter" )
    self:RegisterKeyDescription( "damagelocation" , "Location where to print damage info" )
    self:RegisterKeyDescription( "ammochecklocation" , "Location where to print ammo notifier" )
    self:RegisterKeyDescription( "pulselocation" , "Location where to print healthpulse" )
    self:RegisterKeyDescription( "afklocation" , "Location where to print afk updates" )
end

--- Loads all UserProfiles from file
-- May need to make this public for reload command
-- initusers
function UserHandler:LoadUsers()
    self:RegisterKeys()
    local UserData = FileHandler:Load{ filePath = 'LESM/save/' .. Config.JSON.Users , allowEmpty = false , }
    if not UserData or not next(UserData) then Console:Warning("No users found in file");return end
    Users = { }
    for k=1,tlen(UserData) do Users[tlen(Users)+1] = UserObject(UserData[k]) end
    table.sort(Users,_sortById)
    Console:Info("Loaded " .. tlen(Users) .. " Users from database")
end

--- Saves all UserProfiles to file
-- Does not save keys not registered
-- _deinitusers
function UserHandler:SaveUsers()
    local UserData = { }
    for j=1,tlen(Users) do
        UserData[j] = { }
        for k=1,tlen(Keys) do
            UserData[j][Keys[k].name] = Misc:ConvertType(Keys[k].type,Users[j][Keys[k].name])
        end
    end
    table.sort(UserData,_sortById)
    Console:Info("Saving " .. tlen(UserData) .. " Users")
    FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Users,fileData = UserData }
end

--- Gets User Id that is not being used
function UserHandler:getNextId()
    local id = 0
    while true do
        id = id + 1
        if not self:getWithId(id) then return id end
    end
end

--- Checks Users registered under the same ip,guid,silentid,etpro
function UserHandler:checkAutoLoginDuplicates(Client)
    local ip                    = Misc:trim(Client:getIp(),true)
    local eGuid,sGuid,etproGuid = Client:getGuids()
    local ipCount,eGuidCount,sGuidCount,etproGuidCount = 0,0,0,0
    
    for k=1,tlen(Users) do
		local User = Users[k]
        if ip ~= ""    and User.autologinip   == ip    then ipCount    = ipCount    + 1 end
        if eGuid ~= "" and User.autologinguid == eGuid then eGuidCount = eGuidCount + 1 end
        if sGuid ~= "" and User.autologinsid  == sGuid then sGuidCount = sGuidCount + 1 end
        if etproGuid ~= "" and User.autologinetpro== etproGuid then etproGuidCount = etproGuidCount + 1 end
    end
	
    return ipCount,eGuidCount,sGuidCount,etproGuidCount
end

--- Check if a username exists
function UserHandler:exists(username)
    if self:getWithUsername(username,true) then
        return true
    else
        return false
    end
end

--- Gets UserObject where [keyname] equals [keyValue]
function UserHandler:getWithKey(key,value)
	key   = Misc:trim(key,true)
	value = Misc:trim(value,true,true)
	
	if key == "" or value == "" then return nil end
    for k=1,tlen(Users) do
		local User = Users[k]
        if not User[key] then return nil end
		if Misc:trim(User[key],true,true) == value then
			return User
		end
    end
    return nil
end

--- Returns UserObject that matches [userName]
-- If it can't find a match it will search again for a match within all the usernames
function UserHandler:getWithUsername(username,exact)
	username = Misc:trim(username,true,true)
	if string.len(username) < 3 then return nil end
	
	local founduser;
    for x=1 , tlen(Users) do
		local uname = Misc:trim(Users[x].username, true)
        if uname == username then
			return Users[x]
		elseif not founduser and string.find(uname,username,1,true) then -- First partial match, if exact match isn't found
			founduser = Users[x]
		end
    end
	if exact then
		return nil
	else
		return founduser
	end
end

--- Returns UserObject that matches [userId]
function UserHandler:getWithId(userId)
    userId = tonumber(userId)
    if not userId then return nil end
    for x=1 , tlen(Users) do
        if Users[x].id == userId then
            return Users[x]
        end
    end
    return nil
end

--- Gets UserObject of userString
function UserHandler:getWith(data)
    if not data then return nil end
    if not tonumber(data) then
        return self:getWithUsername(data)
    else
        return self:getWithId(data)
    end
end

--- Gets UserObject of TargetUser
-- If fails then trys to get it from an online ClientObject
-- External call usually
function UserHandler:getUser(TargetUser)
    local User = self:getWith(TargetUser)
    if not User then
        local TargetClient = ClientHandler:getWith(TargetUser)
        if TargetClient and TargetClient:isOnline() then
			return TargetClient.User
		end
    end
    return User
end

--- Deletes a user with userId
function UserHandler:delete(userId) -- Remove
    userId = tonumber(userId)
    if not userId then return end
    for k=1,tlen(Users) do
		local User = User[k]
        if User.id == userId then
            Console:Warning(va("Deleting %s (%d) from user database" , User.username,User.id))
            table.remove(Users,k)
            break
        end
    end
end

function UserHandler:getKey( key )
	key = Misc:trim(key,true,true)
	if key == "" then return nil end
	for k=1,tlen(Keys) do
		if Keys[k].name == key then
			return Keys[k]
		end
	end
end

--- Automatically register a new user for the client
function UserHandler:autoregister( Client )
	if not Client or Client:isOnline() then return end
	local usernameKey = self:getKey( 'username' )
	local username    = Misc:trim( Client:getName() , true , true )
	username = string.gsub( username , "(" .. usernameKey.pattern .. ")" , "" ) -- Replace invalid characters
	while string.len(username) < usernameKey.min do
		username = username .. "0"
	end
	while string.len(username) > usernameKey.max do
		username = string.sub(username,1,-2)
	end
	local count = 0
	while self:exists(username) do
		count    = count + 1
		username = string.sub(username,1,-2) .. count
	end
	
	local user = { }
	for k=1,tlen(Keys) do
		user[Keys[k].name] = Keys[k].default
	end
	
	local eGuid,sGuid,etproGuid = Client:getGuids()
	local ip = Client:getIp()
	local id = self:getNextId()
	user.id = id
	user.autologinguid     = eGuid
    user.autologinip       = ip
    user.autologinsid      = sGuid
    user.autologinetpro    = etproGuid
    user.joindate          = os.time()
    user.lastseen          = os.time()
    user.level             = Client:getLevel()
    user.password          = Core:encrypt("") -- Auto creation password is empty string, since login does not accept empty string
    user.timeout           = os.time() + ( 3 * 60 * 60 ) -- 3 Hours
    user.username          = username
	
	Users[tlen(Users)+1] = UserObject(user)
	Client.User = self:getUser(id)
	
    Client.User:updateNotFound('ip' , ip )
    Client.User:updateNotFound('silent', sGuid)
    Client.User:updateNotFound('guid'  , eGuid)
    Client.User:updateNotFound('etpro' , etproGuid)
    Client.User:updateNotFound('alias' , Misc:trim(Client:getName(),true,true) )
	
    Client:Chat("A new profile has been automatically created for you")
    Client:Chat("See ".. Color.Command .."/"..Config.Prefix.Lua .." help" .. Color.Primary.." for all your new commands")
	Client:Print("You can change your username and password with the /edit command")
    
	Client:Play(Config.Sound.Register)
	
    Console:Info(self:getName() .. " auto registered profile to " .. username)
end
