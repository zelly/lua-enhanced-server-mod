WeaponHandler = { }
WeaponHandler.Weapons = { }

function WeaponHandler:Init()
	-- Make sure each weapon has a table entry ( If the MAX_WEAPONS key is set )
	if et.MAX_WEAPONS then
        for k=0, et.MAX_WEAPONS - 1 do
            if not self:getWithId(k) then
                Console:Debug("Missing weapon # " .. k , "core")
                self.Weapons[tlen(self.Weapons)+1] = { id = k , name = 'WP_UNDEFINED' , nickname = 'UNDEFINED' , ac_ammo = -1 , ac_ammoclip = -1 }
            end
        end
    end
	
	-- Verify each key does not have a nil value
	for k=1,tlen(self.Weapons) do
		local weap = self.Weapons[k]
		weap.id           = tonumber(weap.id) or -1
		weap.name         = weap.name or "WP_UNDEFINED"
		weap.nickname     = weap.nickname or "UNDEFINED"
		weap.ac_ammo      = tonumber(weap.ac_ammo) or -1
		weap.ac_ammoclip  = tonumber(weap.ac_ammoclip) or -1
	end
end

function WeaponHandler:getWithId( id )
	id = tonumber(id)
	if not id then return nil end
	for k=1,tlen(self.Weapons) do
		local weap = self.Weapons[k]
		if weap.id == id then
			return weap
		end
	end
	return nil
end

function WeaponHandler:getWithName( name )
	name = Misc:trim( name , true , true )
	if name == '' then return nil end
	
	for k=1,tlen(self.Weapons) do
		local weap = self.Weapons[k]
		if Misc:trim(weap.name,true,true) == name then
			return weap
		end
	end
	return nil
end

function WeaponHandler:iterate( func )
	if type(func) ~= "function" then return end
	for k=1,tlen(self.Weapons) do
		func(self.Weapons[k])
	end
end