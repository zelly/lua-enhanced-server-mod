Game                  = { }
Game.CurrentState     = "none"
Game.Intermission     = false
Game.Freeze           = false
Game.StartTime        = 0
Game.LTDiff           = 0
Game.FirstBlood       = false
Game.LastConnect      = { }
Game.Dynamites        = { }
Game.Maps             = { }

-- SHUTDOWN
--  0 1 WARMUP
-- -1 0 TIMELIMIT

local _AxisPlayers      = 0
local _AlliedPlayers    = 0
local _BotAxisPlayers   = 0
local _BotAlliedPlayers = 0
local _SpecPlayers      = 0
local _BotSpecPlayers   = 0

local _getDyno = function(id)
    if not id then return nil,nil end
    id = tonumber(id)
    if not id then return nil,nil end
    for k=1,tlen(Game.Dynamites) do
        if id == Game.Dynamites[k].id then return Game.Dynamites[k],k end
    end
    return nil,nil
end

local _getDynoId = function()
    local id = 1
    while _getDyno(id) ~= nil do id = id + 1 end
    return id
end

local _dynoPrint = function(message,team)
    if not team then team = -1 end
    if Config.DynamiteCounter.PrintAll then team = -1 end
    for k = 1 , tlen(Clients) do 
        if team == -1 or Clients[k]:getTeam() == team then
            Clients[k]:PrintLocation(message,Config.DynamiteCounter.Location,"dynolocation")
        end
    end
end

local _findDyno = function(location)
    for k=1,tlen(Game.Dynamites) do
        if Misc:trimlname(location) == Misc:trimlname(Game.Dynamites[k].location) then
            return Game.Dynamites[k],k
        end
    end
    return nil,nil
end

function Game:DynamitePlanted(team,location)
    if not Config.DynamiteCounter.Enabled then return end
    local dynoid = _getDynoId()
    local DYNO = { }
    DYNO.id       = dynoid
    DYNO.routines = { }
    DYNO.team     = team
    DYNO.location = location
    if Config.DynamiteCounter.Plant then
        _dynoPrint("Dynamite planted at " .. Color.Secondary .. location,team)
    end
    for k=1,tlen(Config.DynamiteCounter.CountDown) do
        DYNO.routines[tlen(DYNO.routines)+1] = RoutineHandler:add( function () _dynoPrint(Color.Secondary .. Config.DynamiteCounter.CountDown[k] .. Color.Primary .. " seconds until detonation at " .. Color.Secondary .. location,team) end , 0 , 30 - Config.DynamiteCounter.CountDown[k], "Dyno counter " .. dynoid )
    end
    DYNO.routines[tlen(DYNO.routines)+1] = RoutineHandler:add( function ()
        if Config.DynamiteCounter.Explode then
            _dynoPrint("Dynamite exploded at " .. Color.Secondary .. location,team)
        end
        self:RemoveDynamite(location,2,dynoid)
    end , 0,30, "Dyno counter explode " .. dynoid )
    self.Dynamites[tlen(self.Dynamites)+1] = DYNO
end

function Game:RemoveDynamite(location,state,dynoid)
    state = tonumber(state) or 0
    -- 0 = remove no message (Intermission)
    -- 1 = defused
    -- 2 = exploded
    local dyno,k = _getDyno(dynoid)
    if not dyno then
        dyno = _findDyno(location)
        if not dyno then return end
    end
    for r=1,tlen(dyno.routines) do
        RoutineHandler:remove(dyno.routines[r])
    end
    if state == 1 then
        _dynoPrint("Dynamite defused at " .. Color.Secondary .. location,dyno.team)
        table.remove(self.Dynamites,k)
    elseif state == 2 then
        table.remove(self.Dynamites,k)
    end
     -- State 0 will just erase the table.
end

--- Changes the current game state
-- This does not actually "change" anything in the game, it is just informational
function Game:ChangeState(restart)
    local gs = self:Gamestate()
    restart = tonumber(restart)
    if not restart or not gs then
        self.CurrentState = "unknown"
    elseif gs == 2 and restart == 0 then
        self.CurrentState = "warmup"
    elseif gs == 2 and restart == 1 then
        self.CurrentState = "restart"
    elseif gs == 0 and restart == 1 then
        self.CurrentState = "game"
    else
        self.CurrentState = "unknown"
    end
end
function Game:IntermissionStart()
    local round = tonumber(et.trap_Cvar_Get("g_currentround"))
    Console:Debug("et_IntermissionStarts("..round..")","callback")
    Logger:Log("intermission",self:Map())
    self.Intermission = true
    
    local maps,current_map = Core:MapList()
    local nextmapname = ""
    if current_map and current_map.mapname then nextmapname = current_map.mapname end
    RoutineHandler:add( function ()
        if nextmapname ~= "" then
            Console:Chat("The next map in the rotation is: " .. Color.Secondary .. nextmapname)
        end
        if ( self:Gametype() == 6 ) then
            Console:Chat("Please vote for the next map!")
        end
    end,0,2, "Print's intermission vote reminder" )
    for k=1,tlen(self.Dynamites) do
        self:RemoveDynamite(nil,0,self.Dynamites[k].id)
    end
    self.Dynamites = { }
end
function Game:Gamestate()
    -- 1 & 2 == Warmup?
    -- 0     == Game?
	--[[
	Gamestate value
	0: in game
	1: countdown
	2: warmup
	3: end of the map
	--]]
    return tonumber( et.trap_Cvar_Get( "gamestate" ) )
end

function Game:Mod()
    return string.lower( et.trap_Cvar_Get( "gamename" ) )
end

function Game:Maxclients()
    return tonumber( et.trap_Cvar_Get( "sv_maxClients" ) )
end

function Game:AxisRespawn()
    return tonumber( et.trap_Cvar_Get( "g_userAxisRespawnTime" ) )
end

function Game:AlliedRespawn()
    return tonumber( et.trap_Cvar_Get( "g_userAlliedRespawnTime" ) )
end

function Game:Gametype()
    return tonumber( et.trap_Cvar_Get( "g_gameType" ) )
end

function Game:CurrentRound()
    return tonumber( et.trap_Cvar_Get( "g_currentRound" ) )
end

function Game:Map()
    return string.lower( et.trap_Cvar_Get( "mapname" ) )
end

function Game:getLevelTime()
    --return tonumber(et.trap_Milliseconds())
    return tonumber(et.trap_Milliseconds())
end
--[[
function Game:getLevelTime()
    return ( et.trap_Milliseconds() ) -- Removed LTDifference
end--]]

function Game:getTimelimit()
    return tonumber(et.trap_Cvar_Get( "timelimit" ))
end

--- Gets percentage of map complete based off of the timelimit
function Game:getMapPercentComplete()
    local timepassed = ( self:getLevelTime() - self.StartTime )
    local timelimit = ( self:getTimelimit() * 60 * 1000 )
    return Misc:roundPercent( timepassed / timelimit, 2)
end

--- Gets number of players/bots for specific teams 

function Game:getTotalPlayers( )
    return ( _AxisPlayers + _AlliedPlayers + _SpecPlayers )
end

function Game:getTotalBots( )
    return ( _BotAxisPlayers + _BotAlliedPlayers + _BotSpecPlayers )
end

function Game:getTotalClients( includeBots ) --Basically one function to do both of above functions
    if ( includeBots ) then
        return ( self:getTotalPlayers() + self:getTotalBots() )
    else
        return ( self:getTotalPlayers() )
    end
end

function Game:getTotalPlayingPlayers( )
    return ( _AxisPlayers + _AlliedPlayers )
end

function Game:getTotalPlayingBots( )
    return ( _BotAxisPlayers + _BotAlliedPlayers )
end

function Game:getTotalPlayingClients( includeBots )
    if ( includeBots ) then
        return ( self:getTotalPlayingPlayers() + self:getTotalPlayingBots() )
    else
        return ( self:getTotalPlayingPlayers() )
    end
end

function Game:getAxisPlayers( )
    return ( _AxisPlayers )
end

function Game:getAxisBots( )
    return ( _BotAxisPlayers )
end

function Game:getAxisClients( includeBots )
    if ( includeBots ) then
        return ( _AxisPlayers + _BotAxisPlayers )
    else
        return ( _AxisPlayers )
    end
end

function Game:getAlliedPlayers( )
    return ( _AlliedPlayers )
end

function Game:getAlliedBots( )
    return ( _BotAlliedPlayers )
end

function Game:getAlliedClients( includeBots )
    if ( includeBots ) then
        return ( _AlliedPlayers + _BotAlliedPlayers )
    else
        return ( _AlliedPlayers )
    end
end

--- Gets the difference between axis or allies
-- Negative = Allies Favor , Positive = Axis Favor
-- [includeBots] Also adds the bots into the algorithm
function Game:getDifference( includeBots ) -- Axis - Allies
    return ( self:getAxisClients( includeBots ) - self:getAlliedClients( includeBots ) )
end

--- Calculates the current players.
function Game:getTeams()
    _AxisPlayers      = 0
    _AlliedPlayers    = 0
    _SpecPlayers      = 0
    _BotAxisPlayers   = 0
    _BotAlliedPlayers = 0
    _BotSpecPlayers   = 0
    for k = 1 , tlen(Clients) do 
        if ( Clients[k]:isBot() ) then
            if ( Clients[k]:getTeam() == 1 ) then
                _BotAxisPlayers = _BotAxisPlayers + 1
            elseif ( Clients[k]:getTeam() == 2 ) then
                _BotAlliedPlayers = _BotAlliedPlayers + 1
            elseif ( Clients[k]:getTeam() == 3 ) then
                _BotSpecPlayers = _BotSpecPlayers + 1
            end
        else
            if ( Clients[k]:getTeam() == 1 ) then
                _AxisPlayers = _AxisPlayers + 1
            elseif ( Clients[k]:getTeam() == 2 ) then
                _AlliedPlayers = _AlliedPlayers + 1
            elseif ( Clients[k]:getTeam() == 3 ) then
                _SpecPlayers = _SpecPlayers + 1
            end
        end
    end
    Console:Debug("getTeams Axis(" .. _AxisPlayers ..",".._BotAxisPlayers..") Allies(" .. _AlliedPlayers ..",".._BotAlliedPlayers..") Spec(" .. _SpecPlayers ..",".._BotSpecPlayers..")","getteams")
end

--- Moves players that are not connect, have no ping or have greater than 999 ping to spectator
-- FEATURE Spec999
function Game:Spec999()
    Console:Print("Checking for timed out players... ")
    for k = 1 , tlen(Clients) do 
        local Client = Clients[k]
        --Console:PrintClean(k.. " : " .. tostring(Client.id) .. " " .. tostring(Client))
        if not Client:isBot() and Client:getTeam() ~= 3 then
            if Client:isConnected() then
                if Client:getPing() >= 999 then -- Client ping 0  every mean zombie?
                    Client:setTeam("spec")
                    Logger:Log("timedout",Client.id)
                    Console:ChatClean(va("spec999" .. Color.Tertiary .. ": %s" .. Color.Primary .. " moved to spectator" , Client:getName() ))
                end
            else
                Client:setTeam("spec")
                Logger:Log("timedout",Client.id)
                Console:ChatClean(va("spec999" .. Color.Tertiary .. ": %s" .. Color.Primary .. " moved to spectator" , Client:getName() ))
            end
        end
    end
end



--- Balances a client to the correct team, if one team has more than 1 extra player
function Game:BalanceTeam(Client)
    if not Config.Balance.Enabled or Client:isBot() then return end
    if Config.Balance.ExcludeLevel > 0 and Client:getLevel() >= Config.Balance.ExcludeLevel then return end
    if Config.Balance.ExcludeOnline and Client:isOnline() then return end
    if Config.Balance.ExcludeAuth and Client:isAuth() then return end
    if Client:getTeam() == et.TEAM_SPEC then return end -- TODO Check for shuffle here and onDisconnect
    local Difference = self:getDifference( Config.Balance.CountBots )
    Console:Debug("BalanceTeam(" .. Client:getName() ..") getTeam(" .. Client:getTeamName() .. ") .team(" .. Client:getTeamName(Client.team) .. ") .teamswitches(" .. Client.teamSwitches .. ") || Difference(" .. Difference .. ") Bots(" .. tostring(Config.Balance.CountBots) .. ")","balance")
    if ( Difference == 0 or Difference == 1 or Difference == -1 ) then -- FAIR ENOUGH
        return
    elseif Difference > 1 then -- Axis has more players
        if ( Client:getTeam() == et.TEAM_AXIS ) then
            Client.teamSwitches = Client.teamSwitches + 1
            Client:setTeam( et.TEAM_ALLIES)
            Console:Debug("BalanceTeam->Allies Axis(" .. self:getAxisClients(Config.Balance.CountBots) .. ") Allies(" .. self:getAlliedClients(Config.Balance.CountBots) .. ") Switches(" .. Client.teamSwitches .. ")","balance")
        end
    elseif Difference < -1 then -- Allies has more players
        if ( Client:getTeam() == et.TEAM_ALLIES ) then
            Client.teamSwitches = ( Client.teamSwitches + 1 )
            Client:setTeam(et.TEAM_AXIS)
            Console:Debug("BalanceTeam->Axis Axis(" .. self:getAxisClients(Config.Balance.CountBots) .. ") Allies(" .. self:getAlliedClients(Config.Balance.CountBots) .. ") Switches(" .. Client.teamSwitches .. ")","balance")
        end
    end
    Client:Chat("You have been moved to balance teams")
    local switches = Config.Balance.MaxSwitches - Client.teamSwitches
    if switches <= 0 then -- No switches left
        local cfgBalanceBLTime = Misc:getTimeFormat(Config.Balance.BlacklistTime)
        if Config.Balance.BlockTeam then
            Client.disableTeam = true
            Client:Chat("Team switching is now blocked for this map")
        end
        if Config.Balance.PutSpec then
            Client:setTeam("spectator")
            Client:Chat("You have been moved to spectator")
        end
        if cfgBalanceBLTime ~= 0 then
            if cfgBalanceBLTime < 0 then cfgBalanceBLTime = -1 end
            if cfgBalanceBLTime > 0 then cfgBalanceBLTime = os.time() + cfgBalanceBLTime end
            Client:blacklist{ title     = "(Auto) BalanceTeam",
                              desc      = { "Automatic balance blacklist", },
                              expires   = cfgBalanceBLTime,
                              banner    = "LUA Automatic",
                              reason    = "Automatic balance blacklist", }
        end
    elseif switches == 1 then
        Client:Chat("^1This is your final move, next move and you will be temporarily disconnected")
    elseif switches == 2 then
        Client:Chat("^8Please stop attempting to join the unbalanced team")
    elseif switches == 3 then
        Client:Chat("Please keep teams balanced")
    end
end

--- Callback when client becomes gibbed
-- Isn't instant, and may not be 100% accurate due to I don't know exact gib hp
-- Also this doesn't get gibbed, though it would probably be able to get info from entity like last_hurt and last_mod etc.
-- Probably more bugs as well...
-- Appears it does not include if I press space to tapout, but its at least progress...
function Game:ClientGibbed(Client)
    if Client.authFollowing ~= -1 then
        local Target = ClientHandler:getClient(Client.authFollowing)
        if not Target then
            Client.authFollowing = -1
        else
            Client:entSet("sess.spectatorState", 2)
            Client:entSet("sess.spectatorClient", Target.id)
        end
    end
end

--- Called whenever a client switches to a new team
-- Custom Callback made from ClientUserInfoChanged
function Game:ClientSwitchTeam(Client,team)
    Console:Debug("ClientSwitchTeam("..Client:getName()..") team("..Client:getTeamName(team)..") getTeam("..Client:getTeamName()..") .team("..Client:getTeamName(Client.team)..") .skipNext("..tostring(Client.skipNext)..")","switchteam")
    local from = Client.team
    
    -- ### FUNCTIONS ### --
    
    Client.team = team
    self:getTeams()
    if ( Client.skipNext ) then
        Client.skipNext = false
    else
        self:BalanceTeam(Client)
        if Client.isAfk and team ~= et.TEAM_SPEC then
            local backCommand = CommandHandler:getCommand("back")
            backCommand:Call(Client,{})
        end
        if Client.following ~= -1 and team ~= 3 then
            local Target = ClientHandler:getClient(Client.following)
            if Target then
                Client:Chat("No longer following " .. Target:getName())
            end
            Client.following = -1
        end
    end

    -- ### FUNCTIONS ### --
    Console:Debug("ClientSwitchTeam("..Client:getName()..") team("..Client:getTeamName(team)..") getTeam("..Client:getTeamName()..") .team("..Client:getTeamName(Client.team)..") .skipNext("..tostring(Client.skipNext)..") finished","switchteam")
end

--- Called whenever a client changes his name
-- Custom Callback made from ClientUserInfoChanged
function Game:ClientChangeName(Client)
    if Client:isStrictMuted() then
        Client:setName(Client.name)
        return
    end
    
    Console:Debug( string.format( "Game:ClientChangeName: clientNum(%d) oldName(%s) newName(%s)" , Client.id , Client.name, Client:getName() ) , "callback" )

    Client:validateName()
    Client.name = Client:getName()
    if Client and Client.rudclient then
        RegularUser:update(Client.rudclient,Client)
    end
end

--- Adds Client to one LastConnect table
function Game:setLastConnect(Client,connect)
    if connect == nil then connect = true end
    if not Client:isBot() then
        if connect then
--            Console:Debug("Adding " .. Client:getName() .. " to last connect","callback")
            self.LastConnect[tlen(self.LastConnect)+1] = Client.id
        else
--            Console:Debug("Removing " .. Client:getName() .. " from last connect","callback")
            for k=1, tlen(self.LastConnect) do
                if ( self.LastConnect[k] == Client.id ) then
                    self.LastConnect[k] = nil
                    table.remove(self.LastConnect,k)
                    break
                end
            end
            table.sort(self.LastConnect)
        end
    end
end

--- Gets the last connected Client
function Game:getLastConnect()
    local Client = ClientHandler:getClient(self.LastConnect[tlen(self.LastConnect)])
    return Client
end

--- Checks to make sure there is at least one active client on the server
function Game:isPlayerOnServer()
    for k = 1 , tlen(Clients) do 
        local Client = Clients[k]
        if not ( Client:isBot() ) then
            if ( Client:isConnected() ) then
                return true
            end
        end
    end
    return false
end

--- Plays a global sound to all clients
function Game:Play(sound) Sound:playAll(sound,Console) end

--- Toggles a bit cvar values
function Game:bitCvarToggle(cvarName,cvarValue) -- TODO rename to cvarToggle
    local currentValue = tonumber(et.trap_Cvar_Get(cvarName))
    local currentBits  = bit.tobits(currentValue)
    
    if currentBits[Core:getBitNum(cvarValue)] == 1 then
        et.trap_Cvar_Set(cvarName,currentValue-cvarValue)
        return false -- Return False = Now Disabled
    else
        et.trap_Cvar_Set(cvarName,currentValue+cvarValue)
        return true -- Return True = Now Enabled
    end
end

--- Checks if shrubbot lua is supported
function Game:Shrubbot()
    if et.G_shrubbot_level == nil then 
        return false
	else
		return true
    end
end

local _DeathPrint = function( Killer , Victim , mod , teamkill) -- TODO Support mod values
	if Killer:isBot() and Victim:isBot() then return end
	local deathMessage = Misc:trim( Config.DeathPrint.Message )
	if deathMessage == "" then return end
	-- TODO do a replacement loop for replace <killer with <client to get access to all the replace texts
	-- Same for victim
	-- First need to update userinfo client gets with clientip
	--[[deathMessage = string.gsub( deathMessage , "<killername>" , Killer:getName() )
	deathMessage = string.gsub( deathMessage , "<victimname>" , Victim:getName() )
	deathMessage = string.gsub( deathMessage , "<killerhp>"   , Killer:getHealth() )
	deathMessage = string.gsub( deathMessage , "<victimhp>"   , Victim:getHealth() )
	]]--
	deathMessage = string.gsub( deathMessage , "<distancemessage>" , Victim:getDistanceMessage( EntityHandler:getDistance(Killer.id,Victim.id) ) )
	if teamkill then
		deathMessage = string.gsub( deathMessage , "<teamkilled>"   , "teamkilled" )
	else
		deathMessage = string.gsub( deathMessage , "<teamkilled>"   , "killed" )
	end
	deathMessage = string.gsub( deathMessage , "<victim"   , "<client" )
	deathMessage = MessageHandler:replace( deathMessage , true , Victim )
	deathMessage = string.gsub( deathMessage , "<killer"   , "<client" )
	deathMessage = MessageHandler:replace( deathMessage , true , Killer)
	Victim:PrintLocation(deathMessage,Config.DeathPrint.Location,"deathprintlocation")
end

--- Kill callback
function Game:Kill(Killer,Victim,mod)
    Victim:died()
    if Killer:isBot() and Victim:isBot() then
        Logger:Log("bot_obituary",Killer.id .. " " .. Victim.id .. " " .. mod)
        return
    end
    Logger:Log("obituary",Killer.id .. " " .. Victim.id .. " " .. mod)
    Killer:Play(Config.Sound.KillConfirm)
    _DeathPrint(Killer,Victim,mod)
    
    if Killer:isBot() and not Victim:isBot() then
        if Victim:isOnline() then Victim.User:update("botdeaths",Victim.User:getKey("botdeaths") + 1) end
    elseif not Killer:isBot() and Victim:isBot() then
        if Killer:isOnline() then Killer.User:update("botkills",Killer.User:getKey("botkills") + 1) end
    else
        if Victim:isOnline() then Victim.User:update("deaths",Victim.User:getKey("deaths") + 1) end
        if Killer:isOnline() then Killer.User:update("kills",Killer.User:getKey("kills") + 1) end
    end
	
	-- TODO More advanced killspree system
	--[[
	local cur_lt = self:getLevelTime()
	local MULTIKILL_LENGTH = 3000
	
	-- Killer
	if Killer.killchain ~= 0 and Killer.lastkill > 0 and cur_lt - Killer.lastkill <= MULTIKILL_LENGTH then
		Killer.killchain = Killer.killchain + 1
		if Killer.killchain == 2 then
			Sound:playAll(Config.Sound.kill2,Killer)
			Console:Chat("DOUBLE KILL " .. Killer:getName())
		elseif Killer.killchain == 3 then
			Sound:playAll(Config.Sound.kill3,Killer)
			Console:Chat("TRIPLE KILL " .. Killer:getName())
		elseif Killer.killchain == 4 then
			Sound:playAll(Config.Sound.kill4,Killer)
			Console:Chat("QUAD KILL " .. Killer:getName())
		elseif Killer.killchain == 5 then
			Sound:playAll(Config.Sound.kill5,Killer)
			Console:Chat("MULTI KILL " .. Killer:getName())
		elseif Killer.killchain == 6 then
			Sound:playAll(Config.Sound.kill6,Killer)
			Console:Chat("ULTRA KILL " .. Killer:getName())
		elseif Killer.killchain == 7 then
			Sound:playAll(Config.Sound.kill7,Killer)
			Console:Chat("MONSTER KILL " .. Killer:getName())
		elseif Killer.killchain == 8 then
			Sound:playAll(Config.Sound.kill8,Killer)
			Console:Chat("SUPER DUPER OCTA KILL " .. Killer:getName())
		elseif Killer.killchain == 9 then
			Sound:playAll(Config.Sound.kill9,Killer)
			Console:Chat("OMFG SO BEASTLY KILL " .. Killer:getName())
		else -- 10
			Sound:playAll(Config.Sound.kill10,Killer)
			Console:Chat("OOBVIOUSLY GOD " .. Killer:getName())
		end
	else
		Killer.killchain = 1
	end
	
	Killer.killspree = Killer.killspree + 1
	-- can't be 0
	if string.find(_VERSION,"5.0") then
		if math.mod(Killer.killspree,5) == 0 then
			Console:Chat(va("%s" .. Color.Primary .. " is on a " .. Color.Tertiary .. "%d" .. Color.Primary .." killing spree",Killer:getName(),Killer.killspree) )
		end
	else
		local x,y = math.modf(Killer.killspree/5)
		if y == 0.0 then
			Console:Chat(va("%s" .. Color.Primary .. " is on a " .. Color.Tertiary .. "%d" .. Color.Primary .." killing spree",Killer:getName(),Killer.killspree ))
		end
	end
	
	if Killer.killspree == 5 then
		Sound:playAll(Config.Sound.spree5,Killer)
	elseif Killer.killspree == 10 then
		Sound:playAll(Config.Sound.spree10,Killer)
	elseif Killer.killspree == 15 then
		Sound:playAll(Config.Sound.spree15,Killer)
	elseif Killer.killspree == 20 then
		Sound:playAll(Config.Sound.spree20,Killer)
	end
		
	Killer.lastkill = cur_lt
	
	if not self.FirstBlood then
		Sound:playAll(Config.Sound.killfirst,Killer)
		Console:Chat(va("%s"..Color.Primary.." got first blood!" ,Killer:getName()))
		self.FirstBlood = true
	end
	]]--
end

--- TeamKill callback
function Game:TeamKill(Killer,Victim,mod)
    Victim:died()
    if Killer:isBot() and Victim:isBot() then
        Logger:Log("bot_obituary",Killer.id .. " " .. Victim.id .. " " .. mod)
        return
    end
    Logger:Log("obituary",Killer.id .. " " .. Victim.id .. " " .. mod)
    Killer:Play(Config.Sound.TeamKillConfirm)
    _DeathPrint(Killer,Victim,mod)
	
    if Killer:isBot() and not Victim:isBot() then
        if Victim:isOnline() then Victim.User:update("botteamdeaths",Victim.User:getKey("botteamdeaths") + 1) end
    elseif not Killer:isBot() and Victim:isBot() then
        if Killer:isOnline() then Killer.User:update("botteamkills",Killer.User:getKey("botteamkills") + 1) end
    else
        if Victim:isOnline() then Victim.User:update("teamdeaths",Victim.User:getKey("teamdeaths") + 1) end
        if Killer:isOnline() then Killer.User:update("teamkills",Killer.User:getKey("teamkills") + 1) end
    end
end

--- SelfKill callback
function Game:SelfKill(Victim,mod)
    Victim:died()
    if Victim:isBot() then
        Logger:Log("bot_obituary",Victim.id .. " " .. Victim.id .. " " .. mod)
    else
        Logger:Log("obituary",Victim.id .. " " .. Victim.id .. " " .. mod)
        if Victim:isOnline() then Victim.User:update("selfkills",Victim.User:getKey("selfkills") + 1) end
    end
end

--- WorldDeath callback
function Game:WorldDeath(Victim,mod)
    Victim:died()
    if Victim:isBot() then
        Logger:Log("bot_obituary",Victim.id .. " " .. Victim.id .. " " .. mod)
    else
        Logger:Log("obituary",Victim.id .. " " .. Victim.id .. " " .. mod)
        if Victim:isOnline() then Victim.User:update("worlddeaths",Victim.User:getKey("worlddeaths") + 1) end
    end
end
