CommandHandler = { }
CommandHandler.Commands = { }



--- Loads all available commands
-- If Commands.json does not exist then will attempt to scan directory
function CommandHandler:Init()
    Console:Info("Loading Commands")
    local CommandList = { }
    if FileHandler:exists("LESM/save/Commands.json") then
        CommandList = FileHandler:Load{ filePath = "LESM/save/Commands.json" }
    else
        Console:Warning("LESM/save/Commands.json found. Using default.")
        local tmp_cmdlist = dofile(Core.basepath .. "LESM/util/defaults.lua").commands
        for k=1,tlen(tmp_cmdlist) do
            CommandList[tlen(CommandList)+1] =  { path = tmp_cmdlist[k], }
        end
    end
    
    if not CommandList or next(CommandList) == nil then
        Console:Warning("No commands loaded")
        return
    end
    
    local change = false
    for c=1, tlen(CommandList) do
        local status,LuaCommand = pcall(dofile,Core.basepath .. "/" .. CommandList[c].path)
        if status and LuaCommand ~= nil then
            local req_level = tonumber(CommandList[c].level)
            if req_level then
                LuaCommand.REQUIRE_LEVEL = CommandList[c].level
            else
                CommandList[c].level      = LuaCommand.REQUIRE_LEVEL
                change = true
            end
            if CommandList[c].auth then
                LuaCommand.REQUIRE_AUTH  = CommandList[c].auth
            else
                CommandList[c].auth       = LuaCommand.REQUIRE_AUTH
                change = true
            end
            if CommandList[c].login then
                LuaCommand.REQUIRE_LOGIN = CommandList[c].login
            else
                CommandList[c].login      = LuaCommand.REQUIRE_LOGIN
                change = true
            end
            LuaCommand.PATH = CommandList[c].path
            if CommandList[c].name then
                CommandList[c].name = nil
                change = true
            end
            if CommandList[c].load == nil then
                CommandList[c].load = true
                change = true
            end
            if CommandList[c].force == nil then
                CommandList[c].force = false
                change = true
            end
            if not CommandList[c].load then
                Console:Debug("Not loading " .. LuaCommand.TITLE)
            else
                if Game:Shrubbot() and ( LuaCommand.NOSHRUBBOT and not CommandList[c].force ) then
                    Console:Debug("No shrubbot command not loading " .. tostring(CommandList[c].path),"command")
                elseif Config.Profile.AutoCreate and ( LuaCommand.NAME == "register" or LuaCommand.NAME == "login" or LuaCommand.NAME == "logout" or LuaCommand.NAME == "logout" ) then
                    Console:Debug("Not loading " .. LuaCommand.NAME .. " due to autocreate is on","command")
                elseif next(LuaCommand.REQUIRE_MOD) then
                    local mod   = Game:Mod()
                    local found = false
                    for k=1,tlen(LuaCommand.REQUIRE_MOD) do
                        if mod == LuaCommand.REQUIRE_MOD[k] then
                            found = true
                            break
                        end
                    end
                    if found then self.Commands[tlen(self.Commands)+1] = LuaCommand end
                else
                    self.Commands[tlen(self.Commands)+1] = LuaCommand
                end
            end
        else
            -- Sometimes could be an actual error
            -- But Usually it is just because it is not loaded during shrubbot
            if LuaCommand then -- If it was actually an error
                LOG_ERROR("LESM:CommandHandler(" .. CommandList[c].path .. ")" , tostring(LuaCommand) )
                --Console:Error("Failed to load command " .. tostring(CommandList[c].path),"command")
                --Console:Error(tostring(LuaCommand),"command")
            end
        end
    end
    Console:Info("Loaded " .. tlen(self.Commands) .. " commands")
    if change then
        table.sort(CommandList,function(a,b) return a.path<b.path end)
        FileHandler:Save{ filePath = "LESM/save/Commands.json", fileData = CommandList }
    end
end

--- Gets command with [CommandName]
function CommandHandler:getCommand(CommandName)
    if ( CommandName == nil or CommandName == "" ) then
        return self:getCommand("help")
    end
    for k=1,tlen(self.Commands) do
        if ( string.lower(self.Commands[k].NAME) == string.lower(CommandName) ) then
            return self.Commands[k]
        end
    end
    
    for k=1,tlen(self.Commands) do
        for j=1, tlen(self.Commands[k].ALIASES) do -- Checks aliases 
            if ( string.lower(self.Commands[k].ALIASES[j]) == string.lower(CommandName)) then
                return self.Commands[k]
            end
        end
    end
    return nil
end

--- Runs the console command
function CommandHandler:ConsoleCommand(CommandName,Args)
     MessageHandler:adminwatch(Color.Secondary .. "CONSOLE" .. Color.Tertiary .. "#" .. Color.Primary .. CommandName .. Color.Tertiary .. "#" .. table.concat(Args," "),0,-1,false)
    local COMMAND = self:getCommand(CommandName,"Command")
    if COMMAND == nil then
        Console:Info("No such command")
        return 1
    end
    if not COMMAND.CONSOLE then
        Console:Info("Can not use this command here")
        return 1
    end
    return COMMAND:Call(Console,Args,"console")
end

--- Runs the Command
-- Expects to return true or false on all everything except failed chat,admin, and core commands
function CommandHandler:Command(Client,CommandType,CommandName,Args)
    local COMMAND = self:getCommand(CommandName)
    if not COMMAND and not Game:Shrubbot() and CommandType ~= "mail" and CommandType ~= "core" and ( Misc:triml(CommandName) == "reset" or Misc:triml(CommandName) == "restart" ) then
        COMMAND = self:getCommand("maplist")
        Args = { "reset" }
    end
    if not COMMAND and CommandType == "mail" and ( Misc:triml(CommandName) == "delete" or Misc:triml(CommandName) == "empty" ) then
        local newargs = { }
        newargs[1] = CommandName
        for k=1,tlen(Args) do newargs[tlen(newargs)+1] = Args[k] end
        Args = newargs
        COMMAND = self:getCommand("inbox")
    end
    if not COMMAND and ( CommandType == "lua" or CommandType == "auth" or CommandType == "console" or CommandType == "mail" or CommandType == "clan" ) then
        -- Repeat this stuff from below to prevent spam
        if Client:checkSpam(1.25) then return 1 end
        
        if ( Client:isStrictMuted() and not Client:isAuth() ) then
            Client:Error("Can not do that while muted")
            return 1
        end
         MessageHandler:adminwatch(string.upper(CommandType) .. Color.Tertiary .. "#" .. Color.Primary .. Client:getName()..Color.Tertiary .. "#" .. Color.Primary .. CommandName.. Color.Tertiary .. "#" ..  table.concat(Args," "),0,Client.id,false)
        Client:Error("No such command")
        return 1
    elseif not COMMAND and ( CommandType == "core" or CommandType == "chat" or CommandType == "admin" ) then
        return 0
    end
    
    if Client.invalid then
        Client:Error("Please wait until your client is validated")
        return 1
    end
    
    if Client:checkSpam(1.25) then return 1 end
    
    if ( Client:isStrictMuted() and not Client:isAuth() ) then
        Client:Error("Can not do that while muted")
        return 1
    end
--TODO Support for non shrubbot
    if ( COMMAND.GLOBALOUTPUT and Game:Shrubbot() and et.G_shrubbot_permission( Client.id , "3") == 0 ) and ( CommandType == "lua" or CommandType == "admin" or CommandType == "core" ) then
        if Config.Shrubbot.BlockNoSilentFlag then
            Client:Error("You do not have permission to use this command silently")
            return 1
        end
        if Config.Shrubbot.ConvertNoSilentFlag then
            CommandType = "chat"
        end
    end
    if ( CommandType ~= "chat" ) then
        local newargs = { }
        for k=1,tlen(Args) do newargs[k] = Args[k] end
        
        if newargs[2] and ( COMMAND.NAME == "register" or COMMAND.NAME == "login" or ( COMMAND.NAME == "edit" and Misc:triml(newargs[1]) == "password" ) )  then
            newargs[2] = Core:encrypt(newargs[2])
        end
         MessageHandler:adminwatch(string.upper(CommandType) .. Color.Tertiary .. "#" .. Color.Primary .. Client:getName()..Color.Tertiary .. "#" .. Color.Primary .. CommandName.. Color.Tertiary .. "#" ..  table.concat(newargs," "),0,Client.id,false)
    end
    
    if ( CommandType == "chat") then
        if not ( COMMAND.CHAT ) then return 0 end -- Proceed to chat filter
        
        if ( COMMAND.OVERWRITE and Game:Shrubbot() ) then
            return COMMAND:Call(Client , Args, CommandType)
        else
                Console:Debug("Test " .. table.concat(Config.Shrubbot.CommandFix, " ") .. " CMDNAME: " .. COMMAND.NAME,"command")
            if Misc:table_find(Config.Shrubbot.CommandFix,string.lower(COMMAND.NAME)) then
                Console:Debug("Test 1","command")
                Console:ChatClean(Client:getName() .. "^7: ^2!" .. CommandName .. " " .. table.concat(Args," "))
            else
                Console:Debug("Test 2","command")
                et.G_Say( Client.id, et.SAY_ALL,"!" .. CommandName .. " " .. table.concat(Args," "))
            end
        end
    elseif ( CommandType == "admin" ) then
        if ( Game:Shrubbot() ) and ( et.G_shrubbot_permission( Client.id , "3") == 0 ) then return 0 end -- IF does not have silent command flag
        if not ( COMMAND.ADMIN ) then
            return 0
        end
        
        if ( COMMAND.OVERWRITE and Game:Shrubbot() ) then -- Do I need this here?
            return COMMAND:Call(Client,Args,CommandType)
        end
    elseif ( CommandType == "lua" ) then
        if not ( COMMAND.LUA ) then
            Client:Error("Can not use this command here")
            return 1
        end 
    elseif ( CommandType == "auth" ) then
        if not ( COMMAND.AUTH ) then
            Client:Error("Can not use this command here")
            return 1
        end 
    elseif ( CommandType == "clan" ) then
        if not ( COMMAND.CLAN ) then
            Client:Error("Can not use this command here")
            return 1
        end 
    elseif ( CommandType == "mail" ) then
        if not ( COMMAND.MAIL ) then
            Client:Error("Can not use this command here")
            return 1
        end
    elseif ( CommandType == "console") then
        if not ( COMMAND.CONSOLE ) then
            Client:Error("Can not use this command here")
            return 1
        end
    elseif ( CommandType == "core" ) then
        if not ( COMMAND.CORE ) then
            return 0
        end
    end
    local run = COMMAND:Call(Client,Args,CommandType)
--    Console:Debug("CommandHandler " .. tostring(run) .. " " .. tostring(CommandType),"command")
    if Args[2] and ( COMMAND.NAME == "register" or COMMAND.NAME == "login" or ( COMMAND.NAME == "edit" and Misc:triml(Args[1]) == "password" ) )  then
        Args[2] = Core:encrypt(Args[2]) -- Should be safe to set args here because its after command call
    end
    Logger:Log( "command" , Client.id .. " " .. CommandType .. " " .. CommandName .. " " .. table.concat(Args," "))
    return run -- Return the command run
end

--- Converts args string to a list of args
--  -- ALWAYS send et.ConcatArgs(0)
function CommandHandler:getArgs(ArgString,Start)
    if ( ArgString == nil ) then return nil end
    ArgString = string.gsub(ArgString,"%%",".")
    ArgString = Misc:trim(ArgString)
    if ( ArgString == "" ) then
        return "",{ }
    end
    -- /say
    Start = ( Start + 1 ) -- Concat starts at 0 , While Lua starts at 1
    local Args = Misc:split(ArgString," ")
    local NewArgs = { }
    local keep = true
    for k=1,tlen(Args) do
        keep = true
        if ( k < Start ) then
            keep = false
        end
        Args[k] = Misc:trim(Args[k])
        if ( Args[k] == "" ) then
            keep = false
        end
        if ( keep ) then
            NewArgs[tlen(NewArgs)+1] = Args[k]
        end
    end
    local CommandName = ""
    if ( NewArgs[1] ~= nil ) then
        CommandName = NewArgs[1]
        table.remove(NewArgs,1)
    end
    return CommandName,NewArgs
end

--- Filters chat messages
function CommandHandler:ChatFilter(Client,Where,Message,run)
    if Client:isMuted() then return 1 end
    if not run then run = 0 end
    Where = string.lower(Where)
    local OriginalMessage = Message
    local endc = "^2"
    if ( Where == "say_team" ) then
        endc = "^5"
    elseif ( Where == "say_teamnl" ) then
        endc = "^5"
    elseif ( Where == "say_buddy" ) then
        endc = "^3"
    end
    Message = MessageHandler:filter(Message,Client,nil,endc)
    
    if Client:isBot() then
        Logger:Log("bot_chat",Client.id .. " " .. Where .. " " .. Message)
    else
        Logger:Log("chat"    ,Client.id .. " " .. Where .. " " .. Message)
    end
    
    if ( Where ~= "say" ) then
         MessageHandler:adminwatch(string.upper(Where) .. Color.Tertiary .. "#"..Color.Primary .. Client:getName() .. Color.Tertiary .. "#" .. Message,Client:getTeam(),Client.id,false)
    end
    local newchat = false
    if newchat then
        Core:NewChat(Client,Message,Where)
        return 1
    end
    if ( Message == OriginalMessage ) then return run end
    if ( Where == "say" ) then
        et.G_Say( Client.id, et.SAY_ALL, Message)
    elseif ( Where == "say_team" ) then
        et.G_Say( Client.id, et.SAY_TEAM, Message)
    elseif ( Where == "say_teamnl" ) then
        et.G_Say( Client.id, et.SAY_TEAMNL, Message)
    elseif ( Where == "say_buddy" ) then
        et.G_Say( Client.id, et.SAY_BUDDY, Message)
    end
    
    return 1
end

--- Filters voice chat
function CommandHandler:VoiceChatFilter(Client,where,message,run)
    if Client:isMuted() then return 1 end
    if not run then run = 0 end
    
    where = Misc:triml(where)
    local vsay = Misc:match(message,"^(%S+)")
    message = Misc:trim(string.gsub(message,vsay,""))
    
    local OriginalMessage = message
    
    local endc = "^2"
    if where == "vsay_team" or where == "vsay_teamnl" then
        endc = "^5"
    elseif where == "vsay_buddy" then
        endc = "^3"
    end
    message = MessageHandler:filter(message,Client,nil,endc,nil,true)
    
    if Client:isBot() then
        Logger:Log("bot_voicechat",Client.id .. " " .. where .. " " .. vsay .. " " .. message)
    else
        Logger:Log("voicechat"    ,Client.id .. " " .. where .. " " .. vsay .. " " .. message)
    end
    if where ~= "vsay" then
         MessageHandler:adminwatch(string.upper(where) .. Color.Tertiary .. "#".. Color.Primary  .. Client:getName() .. Color.Tertiary .. "#" .. Color.Secondary .. vsay .. Color.Tertiary .. "#" .. message,Client:getTeam(),Client.id,true)
    end
    
    Console:Debug("Original:[" .. tostring(OriginalMessage) .. "] Message:[" .. tostring(message) .. "]")
    if message == OriginalMessage then return run end
    if Misc:trim(message) == "" then return 1 end
    
    if where == "vsay" then
        et.G_Say( Client.id, et.SAY_ALL, message)
    elseif where == "vsay_team" then
        et.G_Say( Client.id, et.SAY_TEAM, message)
    elseif where == "vsay_teamnl" then
        et.G_Say( Client.id, et.SAY_TEAMNL, message)
    elseif where == "vsay_buddy" then
        et.G_Say( Client.id, et.SAY_BUDDY, message)
    end
    return 1
end

function CommandHandler:getTypeColor(cmdtype)
    if cmdtype == "mail" then
        return Color.mail
    elseif cmdtype == "auth" then
        return Color.auth
    elseif cmdtype == "login" then
        return Color.lua
    elseif cmdtype == "admin" then -- Don't want self.ADMIN check here, because that doesn't really matter
        return Color.admin
    elseif cmdtype == "console" then
        return Color.console
    else
        return Color.default
    end
end

function CommandHandler:getAvailableCommands(Client)
    local cmdlist = { }
    for k=1,tlen(self.Commands) do
        local cmd = self.Commands[k]
        if cmd:canUseCommand(Client) then
            cmdlist[tlen(cmdlist)+1] = cmd
        end
    end
    -- sort table?
    table.sort(cmdlist, function(a,b) return a.NAME < b.NAME end)
    table.sort(cmdlist, function(a,b) return a:getCommandType() < b:getCommandType() end)
    return cmdlist
end
--local _sortAvailableCommands = function( a,b ) return string.lower(a.name) < string.lower(b.name) end
--local _sortAvailableCommands2 = function( a,b ) return string.lower(a.cmdtype) < string.lower(b.cmdtype) end

 --- Get the help command
function CommandHandler:getHelp(Client,CommandName)
    local HelpCommand = self:getCommand("help")
    if not HelpCommand then Console:Error("HELP COMMAND NOT LOADED") end
    return HelpCommand:Call(Client,{ CommandName, },"lua")
end