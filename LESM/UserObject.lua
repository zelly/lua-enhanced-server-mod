UserObject = class("UserObject")

local _sortUsers = function(a,b) return tonumber(a.id) < tonumber(b.id) end

local _getTimeout = function(autoLogin)
    local currentTime = os.time() 
    if autoLogin then
        return currentTime + ( 24 * 3 * 60 * 60 ) -- 3 days
    else
        return currentTime + ( 1 * 60 * 60 ) -- 1 hour
    end
end
    
local _getTableKeys = function (self,key)
    local returnString = { }
    for k=1,tlen(self[key]) do
        returnString[tlen(returnString)+1] = " " .. tostring(self[key][k])
    end
    return table.concat(returnString)
end
   
local _getAllUsernames = function ()
    local UserNames = { }
    for k=1 , tlen(Users) do 
        UserNames[tlen(UserNames)+1] = Users[k].username
    end
    return UserNames
end

function UserObject:initialize(userData)
    for k = 1 , tlen(Keys) do 
        self[Keys[k].name] = Misc:ConvertType(Keys[k].type,userData[Keys[k].name])
    end
end
            

--- Gets value of [key] in User
-- Also converts to correct key type
function UserObject:getKey(key)
    key = Misc:triml(key)
    if key == '' or self[key] == nil then return nil end
    
    if type(self[key]) == "table" then return _getTableKeys(self,key) end
    
    self[key] = Misc:ConvertType(key,self[key])
    
    return self[key]
end

--- Sets [key] to [value] of User
-- Converts to correct type on update
function UserObject:update(key,value)
    key = Misc:triml(key)
    if key == '' or self[key] == nil or value == nil then
        Console:Error( va("User:update( key=%s , value=%s ) for %s" , tostring(key) , tostring(value) , tostring(self.username) ))
        return false -- Failure
    end
    
    self[key] = Misc:ConvertType(key,value)
    return true -- Success
end

--- Updates [key] with [value] if not exist
function UserObject:updateNotFound(key,value)
    key = Misc:triml(key)
    if key == '' or self[key] == nil or value == nil then
        Console:Error( va("User:updateNotFound( key=%s , value=%s ) for %s" , tostring(key) , tostring(value) , tostring(self.username) ))
        return false -- Failure
    end
    if type(self[key]) ~= "table" then
        Console:Error( va("User:updateNotFound( key=%s , value=%s ) tried to update non-table for %s" , tostring(key) , tostring(value) , tostring(self.username) ))
        return false
    end
    if value == '' then return false end
    
    local foundMatch = false
    local value_lower = Misc:triml(value)
    for k=1, tlen(self[key]) do
        if Misc:triml(self[key][k]) == value_lower then
            foundMatch = true
            break
        end
    end
    if not foundMatch then
        self.change = true
        self[key][tlen(self[key]) + 1] = value
    end
end

--- Gets the ClientObject that is currently logged into this UserObject
function UserObject:getOnlineClient() -- getOnline
    for k = 1 , tlen(Clients) do
        local Client = Clients[k]
        if Client:isOnline() and self.id == Client.User.id then
            return Client
        end
    end
    return nil
end

--- Updates all fields using [Client](ClientObject)
function UserObject:updateFields(Client)
    if Client:isConnected() then
        Client.spamMultiplier = self.spam
        
        Client.isAfk     = self.afk
        Client.afkReason = self.afkreason
        self:update("level",tonumber(Client:getLevel()))
        self:update("lastseen",tonumber(os.time()))
        self:updateNotFound("ip",Client:getIp())
        self:updateNotFound("alias",et.Q_CleanStr(Client:getName()))
        
        if self.fakeping then
            Client.pingMin = self.pingmin
            Client.pingMax = self.pingmax
        end
        
        if self.autologin then
            self:update("timeout",_getTimeout(true))
        else
            self:update("timeout",_getTimeout(false))
        end
        
        local eGuid,sGuid,etproGuid = Client:getGuids()
        self:updateNotFound("guid",eGuid)
        self:updateNotFound("silent",sGuid)
        self:updateNotFound("etpro",etproGuid)
        if Game:Mod()== "etpub" then self:updateNotFound("mac",Client:getMac()) end
    end
end

--- Checks Users timeout to see if they are expired or not
function UserObject:checkTimeout()
   local currentTime = os.time()
    if currentTime >= self.timeout then
        return false -- EXPIRED
    else
        return true  -- NOT EXPIRED
    end
end

