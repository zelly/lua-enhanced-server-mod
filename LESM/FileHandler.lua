FileHandler              = { }
FileHandler.SUCCESS      = 0
FileHandler.NOT_EXIST    = 1
FileHandler.ERROR        = 2
FileHandler.ERROR_DECODE = 3

--[[function JSON:onDecodeError(message, text, location, etc)
    Console:InfoClean("[JSONError] " .. message .. " " .. text .. " " .. location ) -- testing needs done
end--]]

--- Reads file to lua
-- f is a table of options:
-- filePath   : Path relative to fs_homepath/fs_game
-- fileType   : json , text
-- allowEmpty : not sure yet
-- default    : default table to return if nothing found
-- ignoreComments
-- makeIfNotExists
-- COPYPASTE: { filePath = FILEPATH , fileType = FILETYPE , allowEmpty = ALLOWEMPTY , default = DEFAULT , ignoreComments = IGNORECOMMENTS , makeIfNotExists = NOTEXISTS, }
-- COPYPASTE: { filePath = FILEPATH , }
-- Assumes filetype is json if not specified
function FileHandler:Load(f)
    if not f then Console:Warning("FileHandler:Load fileOptions table empty");return end -- fileOptions table is empty
    f.filePath = Misc:trim(f.filePath)
    if f.filePath == "" then Console:Warning("FileHandler:Load file path empty");return end -- Filepath is empty
    if not f.fileType then f.fileType = "json" end
    if not f.default then
        if f.fileType == "json" then
            f.default = { }
        else
            f.default = ""
        end
    end
    if f.allowEmpty == nil then f.allowEmpty = true end
    if f.makeIfNotExists == nil then f.makeIfNotExists = true end
    local FILE,err = self:LoadFile(f.filePath,f.ignoreComments)
    if tonumber(FILE) then
        if FILE == self.NOT_EXIST then
            Console:Warning("FileHandler:Load Could not find " .. f.filePath,true)
            if f.makeIfNotExists then
                f.fileData = f.default -- Set fileData to defuault so it knows what to save
                self:Save(f) -- Use all the same stuff options to save.
            end
        elseif FILE == self.ERROR then
            Console:Error("FileHandler:Load Error loading file " .. f.filePath .. " " .. tostring(err))
        elseif FILE == self.ERROR_DECODE then
            Console:Error("FileHandler:Load Error decoding json file " .. f.filePath)
        end
        return f.default
    end
    if f.fileType == "json" then FILE = self:LoadJSON(FILE) end
    if tonumber(FILE) == self.ERROR_DECODE then
        Console:Error("FileHandler:Load Error decoding json file " .. f.filePath)
        return f.default
    end
    return FILE
end

function FileHandler:LoadJSON(fileStr)
    local status,JSONTable;
    if string.find( _VERSION , "5.0" ) then
        status,JSONTable = pcall(decode,fileStr)
    else
        status,JSONTable = pcall(JSON.decode,JSON,fileStr)
    end
    
    if not status or not JSONTable or type(JSONTable) ~= "table" then
        Console:InfoClean( tostring(JSONTable) )
        return self.ERROR_DECODE
    end
    return Misc:table_convertvalues(JSONTable)
end

--- LoadFile loads file to its basic form
-- [fileName] is filePath relative to fs_homepath/fs_game
-- [ignoreComments] (true/false) ignores // and /* */ comments
-- returns string of file
-- returns self.ERROR if could not open
-- returns self.NOT_EXIST if could not locate file
function FileHandler:LoadFile(fileName,ignoreComments)
    if not fileName then return self.NOT_EXIST end
    if not self:exists(fileName) then return self.NOT_EXIST end
    if ignoreComments ~= true then ignoreComments = false end
    
    local status , FileObject , err = pcall(io.open,Core.homepath .. fileName, "r")
    if not FileObject then return self.ERROR,err end
    if not status then return self.ERROR,FileObject end
    
    local fileData = { }
    local inBlockComment = false
    for line in FileObject:lines() do
        if not line then break end
        line = string.gsub( line , "\r" , "" ) -- FUCK MOTHER FUCKING CARRIAGE RETURNS
        if ignoreComments then
            -- WIP still needs testing
            -- I believe string find will need to be subtracted by 2
            local commentStart = string.find(line,'/%*')
            local commentEnd = -1
            if commentStart then -- Checks if block comment ends same line
                if string.find(line,'%*/') then commentEnd = string.find(line,'%*/') else inBlockComment = true end
            elseif not commentStart and inBlockComment then
                if string.find(line,'%*/') then commentEnd = string.find(line,'%*/') end
            elseif not commentStart and not inBlockComment then
                commentStart = string.find(line,'//')
                commentEnd   = -1
            end
            if commentStart then
                local newLine = string.sub(line,1,commentStart)
                if Misc:trim(newLine) ~= "" then fileData[tlen(fileData)+1] = newLine end
            end
        else
            if line ~= "" then fileData[tlen(fileData)+1] = line end
        end
    end
    FileObject:close()
    Console:Debug("Read " .. tlen(fileData) .. " lines from " .. fileName,"file")
    return table.concat(fileData,'\n')
end

function FileHandler:SaveFile(fileName,fileData,saveType)
    if not fileName then return self.NO_PATH end
    if not fileData then return self.NO_DATA end
    if not saveType then saveType = "w"      end
    local status,FileObject,err = pcall(io.open,Core.homepath .. fileName,saveType)
    if not FileObject then return self.ERROR,err end
    if not status then return self.ERROR,FileObject end
    if type(fileData) == "table" then
        for k = 1 , tlen(fileData) do FileObject:write(fileData[k] .. "\n") end
    else
        FileObject:write(fileData)
    end
    FileObject:close()
    Console:Debug("Wrote to " .. tostring(fileName),"file")
    return self.SUCCESS
end

function FileHandler:SaveJSON(fileData)
    if string.find( _VERSION , "5.0" ) then
        return encode(fileData)
    else
        return JSON:encode_pretty(fileData, nil, { pretty = true, indent = "    ", align_keys = true })
    end
end

--- Saves file with options
-- available options:
-- filePath
-- fileData
-- fileType
-- saveType
-- allowEmpty
-- COPYPASTE: { filePath = FILEPATH , fileData = FILEDATA , fileType = FILETYPE , saveType = SAVETYPE , allowEmpty = ALLOWEMPTY }
-- COPYPASTE: { filePath = FILEPATH , fileData = FILEDATA  }
function FileHandler:Save(f)
    if not f then
        Console:Warning("FileHandler:Save fileOptions table is empty")
        return self.NO_PATH
    end
    f.filePath = Misc:trim(f.filePath)
    if f.filePath == "" then
        Console:Warning("FileHandler:Save File path is empty")
        return self.NO_PATH
    end
    if not f.fileData then 
        Console:Warning("FileHandler:Save No file Data for " .. f.filePath)
        return self.NO_DATA
    end
    if not f.saveType then f.saveType = 'w' end
    if not f.fileType then f.fileType = "json" end
    if f.allowEmpty == nil then f.allowEmpty = true end
    
    if not f.allowEmpty then
        if type(f.fileData) == "table" and not next(f.fileData) then
            Console:Debug("Not saving empty table to " .. f.filePath,"file")
            return self.NO_DATA
        elseif type(f.fileData) == "string" and Misc:trim(f.fileData) == "" then
            Console:Debug("Not saving empty string to " .. f.filePath,"file")
            return self.NO_DATA
        end
    end
    if f.fileType == "json" then
        f.fileData = self:SaveJSON(f.fileData)
        if not f.fileData then
            Console:Warning("FileHandler:Save Error encoding json for " .. f.filePath)
            return self.ERROR_ENCODE
        end
    end
    
    local status,err = self:SaveFile(f.filePath,f.fileData,f.saveType)
    if status == self.NO_PATH then
        Console:Warning("FileHandler:Save NO_PATH")
        return status
    elseif status == self.NO_DATA then
        Console:Warning("FileHandler:Save NO_DATA")
        return status
    elseif status == self.ERROR_ENCODE then
        Console:Error("FileHandler:Save ERROR_ENCODE")
        return status
    elseif status == self.ERROR then
        Console:Error("FileHandler:Save ERROR " .. tostring(err))
        return status
    end
    return self.SUCCESS
end

--- Checks if a file exists
function FileHandler:exists(fileName)
    local file_exists =io.open(Core.homepath .. fileName,"r")
    if ( file_exists ~= nil ) then
        io.close(file_exists)
        Console:Debug(fileName .. " - Exists","file")
        return true
    else
        Console:Debug(fileName .." - Does not Exist","file")
        return false
    end
end

function FileHandler:touchFile(fileName)
    local status,theFile = pcall( io.open, Core.homepath .. fileName , "w" )
    if status then
        theFile:write("")
        theFile:close()
        Console:Debug("Created new empty file")
    end
end