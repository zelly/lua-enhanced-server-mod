---Lua Enhanced Server Module
-- LuaESMod.lua Contains et callbacks
-- Intitilize Global Variables
-- Make sure to check wiki for setup

--- Middle Class Lua Implementation
-- @type class
-- https://github.com/kikito/middleclass
if string.find( _VERSION , "5.0" ) then
	local bp = string.gsub( et.trap_Cvar_Get("fs_basepath"),"\\","/") .. "/" .. et.trap_Cvar_Get("fs_game") .. "/?.lua"
	LUA_PATH = LUA_PATH .. ';' .. bp
	require( "LESM/util/lua50" )
	class = require( "/LESM/util/middleclass_compat" )
else
	require( "LESM/util/lua51" )
	class = require( "/LESM/util/middleclass")
end

local _errorFileName = et.trap_Cvar_Get( 'fs_homepath' ) .. '/' .. et.trap_Cvar_Get( 'fs_game' ) .. '/LESM_Errors.log'
function LOG_ERROR( errorLocation , errorTraceback )
	if not errorLocation or not errorTraceback then return end
    local errorFile = io.open(_errorFileName,'ab')
    errorFile:write( '-----------------\n' .. os.date('%c') .. ' ' .. errorLocation .. '\n-----------------\n' .. errorTraceback .. '\n' )
    errorFile:close()
    et.G_LogPrint( '[Error](' .. errorLocation .. ') ' .. errorTraceback .. '\n' )
end

local pcall = pcall
Core = { }
require("LESM/Core")

bit             = { }
sha             = { }
JSON            = { }

UserHandler     = { }
ClientHandler   = { }
RoutineHandler  = { }
MessageHandler  = { }
WeaponHandler   = { }
ClientObject    = { } -- class Handles Client Functions
Console         = { } -- class Handles Console Functions
RegularUser     = { } -- class Handles the regularuser data
UserObject      = { } -- class Handles User Functions
CommandHandler  = { } -- class Create Run Edit Command Functions
Command         = { } -- class Core of command
EntityHandler   = { } -- class Handles Entitys Functions (Such as teleportation)
FileHandler     = { } -- class Handles loading and saving of files
Logger          = { } -- class Handles logging to files and console
ConfigHandler   = { } -- class Handles the configuration of the application
Game            = { } -- class Adds functions and callbacks dealing with gametype,mod, and team switches etc.
Misc            = { } -- class Miscellaneous functions handling strings and such
Mail            = { } -- class Handles mail functions, composing,reading etc. Mail.Inbox holds messages
Sound           = { }

Levels          = { } -- table Holds autolevel levels and their respective xp
Config          = { } -- table Holds all the config values

Clients         = { } -- #table Containing each currently connected Client as a ClientObject
Users           = { } -- #table Containing each User as a UserObject
RegularUserData = { } -- #table Containing non-UserObject info

---------------------
Keys            = { } -- #table Key names,type,default values
DefaultOptions  = { } -- #table Default Config Options ( Probably can remove this from global )
Weapons         = { } -- #table Mod weapon info
Classes         = { } -- #table Class info such as max health and regen rate
Countries       = { } -- #table Country list
ClientFieldNames= { } -- #table Client Fieldnames ( Different mods support Different ones )
FieldNames      = { } -- #table Entity Fieldnames ( Different mods support Different ones )
Cvars           = { }
---------------------
Entities        = { } -- #table Map entities
Color           = { } -- #table Color codes with full names
SwearReplace    = { } -- #table Configurable Swear Replace
Rules           = { } -- #table Configurable Rules
Banners         = { } -- #table Configurable Banners
Blacklist       = { } -- #table Configurable Blacklist
Grammar         = { } -- #table Configurable Grammar / Spellcheck
---------------------

function _printt(t,tc)
	tc = tonumber(tc) or 0
	local tabs = ""
	for k=0,tc do tabs = tabs .. ' ' end
	for i , v in pairs(t) do
		if i ~= "_G" and v ~= "_G" then
			et.G_Print( tabs .. Misc:GetStaticString(tostring(i) , 50-tc , ' ', false) .. ' | ' .. 
				Misc:GetStaticString(tostring(v) , 50 , ' ', false) .. '\n' )
			
			--if type(v) == "table" and tc <= 1 then _printt(v,tc+1) end
		end
	end
end

--- InitGame et callback
-- [levelTime] is the current level time in milliseconds
-- [randomSeed] is a number that can be used to seed random number generators
-- [restart] if map restart then 1 else 0
function et_InitGame(levelTime, randomSeed, restart)
    local status,callbackReturn = pcall( function()
        math.randomseed(randomSeed)
        Core:Init()
        Console:Debug("et_InitGame("..levelTime..", "..randomSeed..", "..restart..")","callback")
        Game.StartTime = Game:getLevelTime()
        Game.LTDiff    = levelTime - Game.StartTime
    end )
    if not status then
        LOG_ERROR( 'et_InitGame' , callbackReturn )
    end
	--_printt(_G)
end

function et_ShutdownGame( restart )
    local status,callbackReturn = pcall( function()
        Console:Debug("et_ShutdownGame("..restart..")","callback")
        Core:DeInit()
    end )
    if not status then
        LOG_ERROR( 'et_ShutdownGame' , callbackReturn )
    end
end


function et_IntermissionStarts( round )
    local status,callbackReturn = pcall( function()
        Game:IntermissionStart()
    end )
    if not status then
        LOG_ERROR( 'et_IntermissionStarts' , callbackReturn )
    end
end

function et_Print(text)
    if not Core or not Core.LOADED then return end
    local status,callbackReturn = pcall( function()
        --- Check for ETPro Guid
        Core:getETProIACGuid( text )
        --- Print damage for silent mod
        if Config.DamagePrint and Game:Mod() == "silent" then
            if string.find(text,"client:(%d+) health:(%d+) damage:(%d+) mod:(%S+)") then
                local clientNum,_,clientDamage,_ = Misc:match(text,"client:(%d+) health:(%d+) damage:(%d+) mod:(%S+)")
                local Client = ClientHandler:getClient(clientNum)
                if not Client                           then return end
                if not Client:isOnline()                then return end
                if not Client.User:getKey("showdamage") then return end
                Client:EchoClean("You took " .. Color.Secondary .. clientDamage .. Color.Primary .. " Damage!")
            end
        end
        
        --- Print dynamite plants
        if string.find(text,"^" .. et.trap_Cvar_Get("gamename") .. "%s+popup:%s+(%w+)%s+(%w+)%s+\"(.+)\"") then
            local _,_,team,action,location = string.find(text,"^" .. et.trap_Cvar_Get("gamename") .. "%s+popup:%s+(%w+)%s+(%w+)%s+\"(.+)\"")
            if team and action and location then
                if Misc:triml(team) == "allies" then
                    team = et.TEAM_ALLIES
                elseif Misc:triml(team) == "axis" then
                    team = et.TEAM_AXIS
                end
                location = string.gsub(location,"(%a)(%w*)", function(a,b) return string.upper(a)..b end)
                action   = Misc:triml(action)
                if action == "planted" then
                    Game:DynamitePlanted(team,location)
                elseif action == "defused" then
                    Game:RemoveDynamite(location,1)
                end
            end
        elseif string.find(text,"^" .. et.trap_Cvar_Get("gamename") .. "%s+popup:%s+(%w+)%s+\"(.+)\"") then
            local _,_,action,location = string.find(text,"^" .. et.trap_Cvar_Get("gamename") .. "%s+popup:%s+(%w+)%s+\"(.+)\"")
            if action and location then
                location = string.gsub(location,"(%a)(%w*)", function(a,b) return string.upper(a)..b end)
                action   = Misc:triml(action)
                if action == "defused" then Game:RemoveDynamite(location,1) end
            end
        end
    end )
    if not status then
        LOG_ERROR( 'et_Print' , callbackReturn )
    end
end

-- Not every mod has this
-- Soon hopefully silent mod will have it
-- TODO Play deeper hitsounds for hitting dead body like jaymod
function et_Damage( target, attacker, damage, dflags, mod)
    local status,callbackReturn = pcall( function()
        if not Config.DamagePrint then return end
        local Target;
        local Attacker;
        if EntityHandler:isClient(target) then   Target   = ClientHandler:add(target) end
        if EntityHandler:isClient(attacker) then Attacker = ClientHandler:add(attacker) end
        
        local modName = ""
        local attackerName = ""
        damage = Misc:GetStaticString(tostring(damage),4)
        
        if Target and Target:isOnline() and Target.User:getKey("showdamage") then
            if modName ~= "" and Target.User:getKey("showweapon") then modName = " with " .. Color.Secondary .. WeaponHandler:getWithId(mod).nickname end
            if Attacker and Target.User:getKey("showname") then attackerName = " from " .. Attacker:getName() end
            -- ? Don't show damage from unknown source
            -- ? I think I will leave it so it shows fall damage and map damage
            Target:PrintLocation("^1" .. damage .. Color.Primary .. "damage" .. attackerName .. Color.Primary .. modName,"echoclean","damagelocation")
        end
        if Attacker and Attacker:isOnline() and Attacker.User:getKey("showdamagedealt") then
            if Target then -- Don't show damage to bodies/world/glass/etc.
                Attacker:PrintLocation("^2" .. damage .. Color.Primary .. "damage","echoclean","damagelocation")
            end
        end
    end )
    if not status then
        LOG_ERROR( 'et_Damage' , callbackReturn )
    end
end

function et_RunFrame( levelTime )
    if not math.mod(levelTime,50) == 0 then return end
	if not Core or not Core.LOADED then return end
    if not Game:isPlayerOnServer() then return end
    
    local status,callbackReturn = pcall( function()
        if Game:Gamestate() == 3 and not Game.Intermission then Game:IntermissionStart() end
        
        RoutineHandler:run()
        
        for k=1,tlen(Clients) do
            local Client = Clients[k]
            if Client:isConnected() then
                local aliveState = Clients[k]:isAlive()
                if aliveState ~= Client.aliveState then
                    Client.aliveState = aliveState
                    if aliveState == 3 then
                        --Game:ClientGibbed(Client) -- Useless call atm 
                    end
                end
                if ( aliveState == 2 or aliveState == 3 ) and Client.authFollowing ~= -1 then
                    -- I would like this to be in Game:ClientGibbed but there, ClientGibbed doesn't register tapouts
                    local Target = ClientHandler:getClient(Client.authFollowing)
                    if not Target then
                        Client.authFollowing = -1
                    else
                        Client:entSet("sess.spectatorState", 2)
                        Client:entSet("sess.spectatorClient", Target.id)
                    end
                end
                
                -- tlen(#) Freeze Command tlen(#) --
                if ( Client.freeze or Game.Freeze ) and Client:isAlive() == 1 then
                    if not next(Client.freezeLocation) then Client.freezeLocation = Client:getOrigin() end
                    Client:setOrigin(Client.freezeLocation)
                end
                -- ## Player Fake Ping Command ##
                if ( Client.pingMin >= 0 and Client.pingMax >= 0 ) then
                    Client:setPing(math.random(Client.pingMin,Client.pingMax))
                end
                
                -- ## Spawn Walkthrough ##
                if Config.SpawnWalkthrough  then
                    if ( Client.spawnWalkthrough and Client:getTeam() ~= 3 and Client:getHealth() > 0 ) then
                        local spawnshield = et.gentity_get(Client.id,"ps.powerups",1)
                        if ( spawnshield > levelTime ) then
                            et.gentity_set(Client.id,"r.contents",32) -- Set to water
                        else
                            Client.spawnWalkthrough = false
                            et.gentity_set(Client.id,"r.contents",33554432) -- Body
                        end
                    end
                end
            end
        end
    end )
    if not status then
        LOG_ERROR( 'et_RunFrame' , callbackReturn )
    end
end


function et_ClientUserinfoChanged(clientNum)
    local status,callbackReturn = pcall( function()
        local Client = ClientHandler:add(clientNum)

        local newTeam = Client:getTeam()
        if Client.team == 0 then
            Console:Debug("et_ClientUserinfoChanged("..Client:getName()..") .team("..Client.team..") == 0 || newTeam = ("..newTeam..")","switchteam")
            Client.team = newTeam
        elseif Client.team ~= newTeam then
            Console:Debug("et_ClientUserinfoChanged("..Client:getName()..") .team("..Client.team..") ~= newTeam("..newTeam..")","switchteam")
            if Client:isBot() then
                Logger:Log( "bot_switchteam" , Client.id .. " " .. Client.team .. " " .. newTeam)
            else
                Logger:Log( "switchteam" , Client.id .. " " .. Client.team .. " " .. newTeam)
            end

            Game:ClientSwitchTeam(Client,newTeam)
        end
        
        if Misc:trimlname( Client:getName() ) ~= Misc:trimlname( Client.name ) then
            Game:ClientChangeName(Client)
        end
    end )
    if not status then
        LOG_ERROR( 'et_ClientUserinfoChanged' , callbackReturn )
    end
end

function et_ClientBegin( clientNum )
    local status,callbackReturn = pcall( function()
        Console:Debug("et_ClientBegin( "..clientNum.." )","callback")
        local Client = ClientHandler:add(clientNum)
        Console:Debug("et_ClientBegin firstTime(".. tostring(Client.firstTime) .. ") firstConnect(" .. tostring(Client.firstConnect) .. ")","callback")
        if not Client.firstTime then return end -- Fix for older mods lua api calls et_ClientBegin a lot
        Client.firstTime = false
        Client.begin     = true
        if Client.team == 0 then Client.team = Client:getTeam() end
        Client:validateGuid()
    end )
    if not status then
        LOG_ERROR( 'et_ClientBegin' , callbackReturn )
    end
end


function et_ClientConnect(clientNum,firstTime,isBot)
    local status,callbackReturn = pcall( function()
        Console:Debug("et_ClientConnect( "..clientNum..","..firstTime..","..isBot.." )","callback")
        local Client = ClientHandler:add(clientNum)
        if not Client then Console:Error( va("ClientConnect Failed to get client from %d" , clientNum ));return end
        if firstTime == 1 then Client.firstConnect = true else Client.firstConnect = false end
        Client:logConnect()
        local blacklist = Core:BlacklistCheck(Client)
        if blacklist then return Core:BlacklistReason(blacklist) end
    end )
    if not status then
        LOG_ERROR( 'et_ClientConnect' , callbackReturn )
    else
        return callbackReturn
    end
end


function et_ClientDisconnect( clientNum )
    local status,callbackReturn = pcall( function()
        Console:Debug("et_ClientDisconnect( "..clientNum..")","callback")
        local Client = ClientHandler:add(clientNum)
        Client:saveXp()
        
        if Client:isBot() then
            Logger:Log( "bot_disconnect" ,Client.id)
        else
            Logger:Log( "disconnect" , Client.id )
        end
        
        
        if Client:isOnline() then Client:Logout() end
        for k=1,tlen(Clients) do
            if Clients[k].following == Client.id then     Clients[k].following = -1 end
            if Clients[k].authFollowing == Client.id then Clients[k].authFollowing = -1 end
        end
        Game:setLastConnect(Client,false)
        
        ClientHandler:delete(Client.id)
    end )
    if not status then
        LOG_ERROR( 'et_ClientDisconnect' , callbackReturn )
    end
end

function et_ClientSpawn( clientNum, revived, teamChange, restoreHealth )
    local status,callbackReturn = pcall( function()
        Console:Debug("et_ClientSpawn( "..clientNum..","..revived..","..tostring(teamChange)..","..tostring(restoreHealth).." )","callback")
        local Client = ClientHandler:add(clientNum)
        
        if ( Client.freeze or Game.Freeze ) and Client:isAlive() == 1 then Client.freezeLocation = Client:getOrigin() end
        if ( Config.SpawnWalkthrough and revived == 0 ) then Client.spawnWalkthrough = true end
        
        Client:xpCheckLevel()
        Client:checkFollow()
		Client:checkSecondary()
        
        -- FEATURE ClassMaxHealth
        if Config.ClassHPMax and revived == 0 then Client:setMaxHealth(Client:getClassTable().maxhp) end
    end )
    if not status then
        LOG_ERROR( 'et_ClientSpawn' , callbackReturn )
    end
end

function et_Obituary( victimNum, killerNum, meansOfDeath )
    local status,callbackReturn = pcall( function()
        if Game:Gamestate() ~= 0 then return end -- Only in game.
        Console:Debug("et_Obituary( "..victimNum..","..killerNum..","..meansOfDeath.." )","callback")
        local Killer = ClientHandler:getWithId(killerNum)
        local Victim = ClientHandler:getWithId(victimNum)
        if not Killer and not Victim then
            Console:Warning("Killer and Victim are both nil")
            return
        end -- I don't think this should  happen often?
        if Killer and not Victim then
            Console:Warning("Killer killed unknown victim")
            return
        end
        if not Killer and Victim then
            -- WORLD DEATH
            Game:WorldDeath(Victim,meansOfDeath)
            return
        end
        if Killer.id == Victim.id then
            Game:SelfKill(Victim,meansOfDeath)
            return
        end
        
        if Killer:getTeam() == Victim:getTeam() then
            Game:TeamKill(Killer,Victim,meansOfDeath)
            return
        else
            Game:Kill(Killer,Victim,meansOfDeath)
            return
        end
    end )
    if not status then
        LOG_ERROR( 'et_Obituary' , callbackReturn )
    end
end

local _createuser = function()
    local u = {}
    for k=1,tlen(Keys) do
        u[Keys[k].name] = Keys[k].default
        u.username = Misc:randomWord()
        u.password = Core:encrypt(Misc:randomWord())
        u.joindate = tonumber(os.time())
        u.lastseen = tonumber(os.time())
    end
    Users[tlen(Users)+1] = UserObject(u)
end

function et_ConsoleCommand( command )
    local status,callbackReturn = pcall( function()
        local Arg0 = Misc:triml(et.trap_Argv(0))
        if Arg0 == "forcecvar" or Arg0 == "sv_cvar" or Arg0 == "bot" or Arg0 == "vid_restart" then return 0 end
        Console:Debug("et_ConsoleCommand("..et.ConcatArgs(0)..")","callback")
        Logger:Log( "console" , et.ConcatArgs(0) )
        if Arg0 == "user" then
            Console:Info("TEST")
            local usercount = tonumber(et.trap_Argv(1)) or 0
            Console:Info("Creating " .. usercount .. " users")
            for _=1,usercount do _createuser() end
            Console:Info("Done")
            return 1
        end
        if ( Arg0 == Config.Prefix.Lua or Arg0 == "lua" ) then
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),1)
            return CommandHandler:ConsoleCommand(CommandName,Args)
        end
    end )
    if not status then
        LOG_ERROR( 'et_ConsoleCommand' , callbackReturn )
        return 0
    else
        return callbackReturn
    end
end

function et_ClientCommand( clientNum, cmd )
    local status,callbackReturn = pcall( function()
        local _cmdList = { class = true, statsall = true, obj = true, skrwrd = true, scores = true, score = true, forcetapout = true, ws = true,
                            immaplist = true, impt = true, impkd = true, imwa = true,  imws = true, }
        local Arg0 = Misc:triml(et.trap_Argv(0))
        if _cmdList[Arg0] then return 0 end
        _cmdList=nil
        local Client = ClientHandler:add(clientNum)
        if not Client then
            Console:Error("et_ClientCommand Could not get client from " .. tostring(clientNum))
            return 0
        end
        if Client:isBot() then return 0 end
        
        local _chatPattern  = "^" .. Config.Prefix.Chat .. "%w+$"
        local _adminPattern = "^" .. Config.Prefix.Admin .. "%w+$"
        local _corePattern  = "^/%w+$"
        
        Console:Debug("et_ClientCommand("..clientNum..","..et.ConcatArgs(0)..")","callback")
        if ( Arg0 == "say" or Arg0 == "say_team" or Arg0 == "say_teamnl" or Arg0 == "say_buddy" ) then -- HANDLES CHAT
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),1)
            if ( CommandName ~= "" and string.find(CommandName,_chatPattern) ) then -- CHAT COMMAND FORMAT
                if CommandHandler:Command(Client,"chat",string.gsub(CommandName,Config.Prefix.Chat,"",1),Args) == 1 then
                    Console:Debug("et_ClientCommand chat command return 1" ,"command")
                    return 1
                else
                    local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
                    return CommandHandler:ChatFilter(Client,CommandName,table.concat(Args," "))
                end
            elseif ( CommandName ~= "" and string.find(CommandName,_corePattern) ) then
                local run = tonumber(CommandHandler:Command(Client,"core", string.gsub(CommandName,"/","",1),Args)) or 0
                Console:Debug("et_ClientCommand chat core command return " .. tostring(run),"command")
                if run == 1 then return run end
                local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
                return CommandHandler:ChatFilter(Client,CommandName,table.concat(Args," "))
            else
                local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
                return CommandHandler:ChatFilter(Client,CommandName,table.concat(Args," "))
            end
        elseif ( Arg0 == "vsay" or Arg0 == "vsay_team" or Arg0 == "vsay_teamnl" or Arg0 == "vsay_buddy" ) then
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
            return CommandHandler:VoiceChatFilter(Client,CommandName,table.concat(Args," "))
        elseif string.find(Arg0,_adminPattern) then
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
            if ( CommandName ~= "" and string.find(CommandName,_adminPattern ) ) then
                local run = tonumber(CommandHandler:Command(Client,"admin",string.gsub(CommandName,Config.Prefix.Admin,"",1),Args)) or 0
                Console:Debug("et_ClientCommand admin command return " .. tostring(run),"command")
                return run
            else
                return 0
            end
        elseif ( Arg0 == Config.Prefix.Lua or Arg0 == "lua" ) then
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),1)
            if CommandName == "" then CommandName = "help" end
            CommandHandler:Command(Client,"lua",CommandName,Args)
            return 1
        elseif ( Arg0 == Config.Prefix.Mail or Arg0 == "mail" ) then
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),1)
            if CommandName == "" then CommandName = "inbox" end
            CommandHandler:Command(Client,"mail",CommandName,Args)
            return 1
        elseif ( Arg0 == Config.Prefix.Auth or Arg0 == "sudo" ) and Client:isAuth() then
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),1)
            if CommandName == "" then CommandName = "help" end
            CommandHandler:Command(Client,"auth",CommandName,Args)
            return 1
        elseif Arg0 == "callvote" then
            if Client:checkSpam() then return 1 end
            if Config.Vote.Mute and Client:isStrictMuted() then
                Client:Error("You can not call votes while muted.")
                return 1
            end
            if Config.Vote.RequireLevel >= Client:getLevel() then
                Client:Error("You do not have enough level to call votes")
                return 1
            end
            if Config.Vote.RequireLogin and not Client:isOnline() then
                Client:Error("You must be logged in to call votes")
                return 1
            end
            local _,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
            Logger:Log( "vote" , Client.id .. " " .. table.concat(Args, " ") )
             MessageHandler:adminwatch(Color.Secondary .. "VOTE" .. Color.Tertiary .. "#" .. Color.Primary .. Client:getName() .. Color.Tertiary .. "#" ..  Color.Primary .. table.concat(Args," "),0,Client.id,false)
        elseif Arg0 == "ref" then
            local _,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
             MessageHandler:adminwatch(Color.Secondary .. "REF" .. Color.Tertiary .. "#" .. Color.Primary .. Client:getName() .. Color.Tertiary .. "#" ..  Color.Primary .. table.concat(Args," "),0,Client.id,false)
        elseif Arg0 == "imready" then
            if Game.Intermission then
                if not Client.isReady then
                    Client.isReady = true
                    if Config.Intermission.ReadyMessage then
                        if Game:Gametype() == 6 and not Config.Intermission.PrintBoth then
                            if not Client.hasVoted then -- Ready, not voted
                                Console:Print(Client:getName() .. Color.Primary .. " is ready!")
                            else -- Ready , has voted
                                Console:Chat(Client:getName()  .. Color.Primary .. " has voted for a map and is ready!")
                            end
                        else
                            Console:Chat(Client:getName() .. Color.Primary ..  " is ready!")
                        end
                    end
                end
            end
        elseif Arg0 == "mapvote" then
            if Game:Gametype() ~= 6 then return 0 end
            if Game.Intermission then
                if Config.Intermission.BlockSpec and not Client:isActive() then
                    Client:ErrorChat("Must be on a team to vote for a map")
                    return 1
                end
                if Config.Intermission.Mute and Client:isMuted() then
                    Client:ErrorChat("You are muted!")
                    return 1
                end
                if Config.Intermission.RequireLogin and not Client:isOnline() then
                    Client:ErrorChat("You must register to vote for a map")
                    return 1
                end
                if Config.Intermission.RequireLevel ~= 0 and Config.Intermission.RequireLevel >= Client:getLevel() then
                    Client:ErrorChat("Not high enough level to vote for a map")
                    return 1
                end
                if not Client.hasVoted then
                    Client.hasVoted = true
                    if Config.Intermission.VoteMessage then
                        if Game:Gametype() == 6 and not Config.Intermission.PrintBoth then
                            if not Client.isReady and Game:Mod() ~= "legacy" then -- has Voted, ready
                                Console:Print(Client:getName() .. Color.Primary .. " has voted for a map!")
                            else -- Ready , has voted
                                Console:Chat(Client:getName()  .. Color.Primary .. " has voted for a map and is ready!")
                            end
                        else
                            Console:Chat(Client:getName() .. Color.Primary ..  " has voted for a map!")
                        end
                    end
                end
            end
        elseif Arg0 == "team" then
            local _,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
            local team = Args[1]
            --if tonumber(team) then team = tonumber(team) end -- I don't think the command actually excepts number values for the team
            if not team then return 0 end
            if team == "b" or team == "blue" or team == "allies" then team = et.TEAM_ALLIES end
            if team == "r" or team == "red" or team == "axis" then team = et.TEAM_AXIS   end
            if team == "s" or team == "spectator" then team = et.TEAM_SPEC end
            if team ~= et.TEAM_ALLIES and team ~= et.TEAM_AXIS and team ~= et.TEAM_SPEC then
                Console:Warning( Client:getName() .. "Invalid team command format: " .. table.concat(Args) )
                return 1 -- Changed to 1 from 0
            end -- Invalid team format
            if team == Client:getTeam() then return 0 end -- Already on this team
            
            Client.skipNext = false -- Set this to false, so it doesn't skip the clientswitchteam
            
            if Client.invalid then
                Client:Error("Please wait until your client is validated")
                return 1
            end
            if Client:checkSpam() then return 1 end
            
            if Config.Profile.ForceRegister and not Client:isBot() and not Client:isOnline() then -- Check force registration
                Client:Chat("You must first register a profile and log in before you can play.")
                CommandHandler:getHelp(Client,"register")
                if Client:getTeam() ~= et.TEAM_SPEC then Client:setTeam(et.TEAM_SPEC) end
                return 1
            end
            
            if Config.TeamBlock > 0 then
                if Client.teamBlock > 0 then -- Team block protection, prevents 
                    Client:Chat("Team command blocked for " .. Color.Secondary .. Client.teamBlock .. Color.Primary .. " seconds" )
                    return 1
                else
                    if Client.lastTeamBlock == 0 then
                        Client.lastTeamBlock = Config.TeamBlock
                        Client.teamBlock     = Config.TeamBlock
                    else
                        Client.teamBlock     = Client.lastTeamBlock * Config.TeamBlockMultiplier
                        Client.lastTeamBlock = Client.teamBlock
                    end
                end
            end
            if Client.disableTeam then
                if team == et.TEAM_SPEC and not Client.confirm.teamSwitch then
                    Client:ErrorChat("If you go spec now, you will not be able to rejoin the game")
                    Client:ErrorChat("Go spec again if you agree to this.")
                    Client.confirm.teamSwitch = true
                    return 1
                elseif team == et.TEAM_SPEC and Client.confirm.teamSwitch then
                    Client:ErrorChat("Your team command has been disable for this map.")
                    return 1
                end
                if team ~= Client:getTeam() then
                    Client:ErrorChat("Your team command has been disable for this map.")
                    return 1
                end
            end
            
            -- TEAM COMMAND SUCCESS
            
            if team ~= et.TEAM_SPEC then
                if Client:entGet("sess.shoutcaster") ~= 0 then Client:entSet("sess.shoutcaster",0) end
            end
        else
            local CommandName,Args = CommandHandler:getArgs(et.ConcatArgs(0),0)
            return CommandHandler:Command(Client,"core",CommandName,Args)
        end
    end )
    if not status then
        LOG_ERROR( 'et_ClientCommand' , callbackReturn )
        return 0
    else
        return callbackReturn
    end
end

function et_CvarValue( clientNum, cvar, value )
    local status,callbackReturn = pcall( function()
        local Client = ClientHandler:add(clientNum)
        if not Client then return end
        Client.cvars[cvar] = Misc:trim(value)
    end )
    if not status then
        LOG_ERROR( 'et_CvarValue' , callbackReturn )
    end
end
